const mysql = require('mysql');

function sqlConnect() {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        // password : '0Gravity99',
        password : '',
        database : 'pyg'
    });

    connection.connect(function(err) {
        if(err) { 
            return "Error in Connecting Database";
        }
    });
    // console.log(connection);
     return connection;
}

module.exports = sqlConnect;