const admin = require('firebase-admin');
const extend = require('xtend');
const userServiceAccountAndroid = require('./fcmCredentials.json');
const userServiceAccountiOS = require('./fcmCredentialsIOS.json');
const merchantServiceAccountAndroid = require('./fcmCredentialsMerchant.json');
const merchantServiceAccountiOS = require('./fcmCredentialsMerchantIOS.json');

// Initialize Android Notifications for User

const Android = admin.initializeApp({
    credential : admin.credential.cert(userServiceAccountAndroid)
}, "Android");




// Initialize iOS Notifications for User

const iOS = admin.initializeApp({
    credential : admin.credential.cert(userServiceAccountiOS)
}, "iOS");




// Initialize Android Notifications for Merchant

const merchantAndroid = admin.initializeApp({
    credential : admin.credential.cert(merchantServiceAccountAndroid)
}, "merchantAndroid");




// Initialize iOS Notifications for Merchant

const merchantiOS = admin.initializeApp({
    credential : admin.credential.cert(merchantServiceAccountiOS)
}, "merchantiOS");





module.exports = {

    userNotificationAndroid : function(deviceToken, title, notificationContent, additionalInfo) {
            var info = JSON.stringify(additionalInfo.order_id)
            var options = {
                priority : "high",
                timeToLive : 60 * 60 *24
            };

            var payload = {

                "data" : {
                        "title" : title,
                        "body" : notificationContent,
                        "shop_name" : additionalInfo.shop_name,
                        "order_id"  : info,
                        "tracking_id": additionalInfo.tracking_id
                    }
            };

        Android.messaging().sendToDevice(deviceToken,payload,options)
            .then(function(response) {
                console.log(response.results[0]);
            })
            .catch(function(error) {
                console.log(error);
            });

    },




    userNotificationIOS : function(deviceToken, title, notificationContent, additionalInfo) {

        var info = JSON.stringify(additionalInfo.order_id);

        var options = {
            priority : "high",
            timeToLive : 60 * 60 *24
        };

        var payload = {
            "notification" : {
                "title" : title,
                "body" : notificationContent
            },
            "data" : {
                "shop_name" : additionalInfo.shop_name,
                "order_id"  : info,
                "additionalInfo" : additionalInfo.tracking_id
            }
        };
    
        iOS.messaging().sendToDevice(deviceToken,payload,options)
            .then(function(response) {
                console.log(response.results[0]);
            })
            .catch(function(error) {
                console.log(error);
            });

    },




    merchantNotificationAndroid : function(deviceToken, title, notificationContent, additionalInfo) {   
        var info = JSON.stringify(additionalInfo.order_id);

            var options = {
                priority : "high",
                timeToLive : 60 * 60 *24
            };

            var payload = {

                "data" : {
                        "title" : title,
                        "body" : notificationContent,
                        "user_name" : additionalInfo.user_name,
                        "order_id"  : info,
                        "tracking_id": additionalInfo.tracking_id
                    }
            };
        
        merchantAndroid.messaging().sendToDevice(deviceToken,payload,options)
            .then(function(response) {
                console.log(response.results[0]);
            })
            .catch(function(error) {
                console.log(error);
            });

    },




    merchantNotificationIOS : function(deviceToken, title, notificationContent, additionalInfo) {

        var info = JSON.stringify(additionalInfo.order_id);

        var options = {
            priority : "high",
            timeToLive : 60 * 60 *24
        };

        var payload = {
            "notification" : {
                "title" : title,
                "body" : notificationContent
            },
            "data" : {
                "user_name" : additionalInfo.user_name,
                "order_id"  : info,
                "additionalInfo" : additionalInfo.tracking_id
            }
        };

        merchantiOS.messaging().sendToDevice(deviceToken,payload,options)
            .then(function(response) {
                console.log(response.results[0]);
            })
            .catch(function(error) {
                console.log(error);
            });

    },
    
}
