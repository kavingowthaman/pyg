// PYG v1.0

// Required Modules and Files
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('secretkey');
const Geo = require('geo-nearby');
const randomstring = require('randomstring');
const Excel = require('xlsx');
const request = require('request');
const aws4 = require('aws4');
const asn1 = require('asn1');
const cors = require('cors');
const objectAssign = require('object-assign');
const Mail = require('./SendMail.js');
const Notify = require('./Notifications.js');
const User = require('../index.js');
const Dashboard = require('./dashboard.js');
const DbConnect = require('./DbConnect.js');
const connection = new DbConnect();

module.exports = {

merchantApp : function() {

//URL Encoding
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

//Access Control Mechanisms (Dashboard Access)
app.use(cors());

//Routing API's
var port = process.env.PORT || 8000;
var router = express.Router();




//API's in PYG MERCHANT as follows

// Registration and Verification API's for Consumers

//Registration

router.post('/register', function(req, res) {

    var email = req.body.email;
    var user_type = req.body.user_type;
    var otp = module.exports.otpGenerator();

    var query = "SELECT email,user_type FROM email_verify";
    connection.query(query, function(err, result, existing) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else {
            for(var i = 0; i < result.length; i++) {
                if(result[i].email == email && result[i].user_type == user_type) {
                    existing = 1;
                    break;
                }
            }
            if(existing == 1) {
                res.status(200).send({
                    error : true,
                    message : 'User already exists',
                });
            }
            else {
                var html = "<p>�Your OTP for PYG Registration is : '"+otp+"'</p><br><br><strong>Thanks for registering with PYG</strong>";
                Mail.sendMail(email, 'PYG Verification', html);
                var query = "INSERT INTO email_verify(email,user_type,otp,status) VALUES('"+email+"' , '"+user_type+"' , '"+otp+"' , 0)";
                connection.query(query, function(err, result) {
                    if(err) {
                        res.status(200).send({
                            error : true,
                            message : err
                        });
                    }
                    else if(result.affectedRows != 0) {                  
                        res.status(201).send({
                            error : false,
                            email : email,
                            user_type : user_type,
                            status : 'OTP has been sent to your mail'
                        });
                    }
                });
            }
        }
    });

});




// Email Verification

router.post('/verify', function(req, res) {

    var entered_otp = req.body.entered_otp;
    var email = req.body.email;
    var quoted_email = "'"+email+"'";

    var query = "SELECT otp FROM email_verify WHERE email LIKE "+quoted_email+" AND user_type LIKE 'Merchant'";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(201).send({
                error : true,
                message : err
            });
        }
        else if(result[0].otp == entered_otp) {
            var update_query = "UPDATE email_verify SET status = 1 WHERE otp = "+entered_otp+"";
            connection.query(update_query, function(updateError, updateResult) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : updateError
                    });
                }
                else if(updateResult.affectedRows != 0) {
                    res.status(201).send({
                        error : false,
                        message : 'Verified'
                    });
                }
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Incorrect OTP',
            });
        }
    });

});




// Creating Merchant Entry after verification

router.post('/insertMerchant', function(req, res) {

    var merchant_name = req.body.merchant_name;
    var merchant_email = req.body.merchant_email;
    var password = req.body.password;
    var merchant_contact = req.body.merchant_contact;
    var shop_name = req.body.shop_name;
    var address = req.body.address;
    var zip_code = req.body.zip_code;
    var area = req.body.area;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var merchant_img = req.body.merchant_img;
    var os_type = req.body.os_type;
    var fcm_id = req.body.fcm_id;
    var encrypted_password = module.exports.encrypt(password);
    var quoted_email = "'"+merchant_email+"'";

    var query = "SELECT status FROM email_verify WHERE email = "+quoted_email+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length == 0) {
            res.status(200).send({
                error : true,
                message : 'Complete your email verification'
            });
        }
        else if(result[0].status == 1) {
            var checkMerchantEntry_query = "SELECT merchant_email FROM merchants";
            connection.query(checkMerchantEntry_query, function(checkMerchantEntryErr, checkMerchantEntryResult, existing) {
                if(checkMerchantEntryErr) {
                    res.status(200).send({
                        error : true,
                        message : checkMerchantEntryErr
                    });
                }
                else {
                    for(var i = 0; i < checkMerchantEntryResult.length; i++) {
                        if(checkMerchantEntryResult[i].merchant_email == merchant_email) {
                            existing = 1;
                            break;
                        }
                    }
                    if(existing == 1) {
                        res.status(200).send({
                            error : true,
                            message : 'Merchant already registered',
                        });
                    }
                    else {
                        var insert_query = "INSERT INTO merchants(merchant_name,merchant_email,password,merchant_contact,shop_name,address,zip_code,area,latitude,longitude,merchant_img,os_type,fcm_id) VALUES('"+merchant_name+"' , '"+merchant_email+"' , '"+encrypted_password+"' , '"+merchant_contact+"' , '"+shop_name+"' , '"+address+"' , '"+zip_code+"' , '"+area+"' , '"+latitude+"' , '"+longitude+"' , '"+merchant_img+"' , '"+os_type+"', '"+fcm_id+"')";
                        connection.query(insert_query, function(insertErr, insertResult) {
                            if(insertErr) {
                                res.status(200).send({
                                    error : true,
                                    message : insertErr
                                });
                            }
                            else if(insertResult.affectedRows != 0) {
                                var catalogue_insertQuery = "INSERT INTO merchant_catalogue(merchant_id) VALUES('"+insertResult.insertId+"')";
                                connection.query(catalogue_insertQuery, function(catalogueinsertErr, catalogueinsertResult) {
                                    if(catalogueinsertErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : catalogueinsertErr
                                        });
                                    }
                                    else {
                                        var inventory_insertQuery = "INSERT INTO merchant_inventory(merchant_id) VALUES('"+insertResult.insertId+"')";
                                        connection.query(inventory_insertQuery, function(inventoryinsertErr, inventoryinsertResult) {
                                            if(inventoryinsertErr) {
                                                res.status(200).send({
                                                    error : true,
                                                    message : inventoryinsertErr
                                                });
                                            }
                                            else {
                                                res.status(201).send({
                                                    error : false,
                                                    status : 'Inserted',
                                                    merchant_details : {
                                                        merchant_id : insertResult.insertId,
                                                        merchant_email : merchant_email,
                                                        merchant_contact : merchant_contact,
                                                        shop_name : shop_name,
                                                        merchant_name : merchant_name,
                                                        address : address,
                                                        zip_code : zip_code,
                                                        area : area,
                                                        latitude : latitude,
                                                        longitude : longitude,
                                                        merchant_img : merchant_img,
                                                        os_type : os_type,
                                                        fcm_id : fcm_id,
                                                        admin_approval : 0,
                                                        catalogue_id : catalogueinsertResult.insertId,
                                                        inventory_id : inventoryinsertResult.insertId
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });        
                            }
                        });
                    }
                }
            });
        }
        else if(result[0].status == 0) {
            res.status(200).send({
                error : true,
                message : 'Complete your email verification'
            });
        }
    });
 
});




// Merchant Login API for registered merchants

router.post('/loginMerchant', function(req, res) {

    var merchant_email = req.body.merchant_email;
    var password = req.body.password;

    var search_query = "SELECT merchant_email,password FROM merchants WHERE merchant_email LIKE '"+merchant_email+"'";
    connection.query(search_query, function(searchErr, searchResult) {
        setTimeout(function() {
            if(searchErr) {
                res.status(200).send({
                    error : true,
                    message : searchErr
                });
            }
            else if(searchResult.length != 0) {
                var decrypted_password = module.exports.decrypt(searchResult[0].password);
                if(merchant_email == searchResult[0].merchant_email && password == decrypted_password) {
                    var fetchDetails_query = "SELECT merchants.merchant_id,merchants.merchant_email,merchants.merchant_contact,merchants.shop_name,merchants.merchant_name,merchants.address,zip_code,merchants.area,merchants.latitude,merchants.longitude,merchants.merchant_img,merchants.os_type,merchants.fcm_id,merchants.admin_approval,merchant_catalogue.catalogue_id,merchant_inventory.inventory_id FROM merchants LEFT JOIN merchant_catalogue ON merchants.merchant_id = merchant_catalogue.merchant_id LEFT JOIN merchant_inventory ON merchants.merchant_id = merchant_inventory.merchant_id WHERE merchants.merchant_email LIKE '"+merchant_email+"'";
                    connection.query(fetchDetails_query, function(fetchDetailsErr, fetchDetailsResult) {
                        if(fetchDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : fetchDetailsErr
                            });
                        }
                        else {
                            if(fetchDetailsResult[0].admin_approval == 1) {
                                res.status(200).send({
                                    error : false,
                                    message : "Login Successful",
                                    merchant_details : fetchDetailsResult[0]
                                });
                            }
                            else if(fetchDetailsResult[0].admin_approval == 0) {
                                res.status(200).send({
                                    error : true,
                                    message : "Account is under verification",
                                    admin_approval : 0
                                });
                            }
                            else if(fetchDetailsResult[0].admin_approval == 2) {
                                res.status(200).send({
                                    error : true,
                                    message : "Rejected merchant",
                                    admin_approval : 2
                                });
                            }
                        }
                    });
                }
                else if(merchant_email == searchResult[0].merchant_email && password != decrypted_password) {
                    res.status(200).send({
                        error : true,
                        message : 'Incorrect password'
                    });
                }
            }
            else {
                res.status(200).send({
                    error : true,
                    message : "Merchant doesn't exists"
                });
            }
        }, 2000);
    });

});




// To get all Merchant's Details

router.get('/getAllMerchants', function(req, res) {

    var query = "SELECT * FROM merchants";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length != 0){
            res.status(200).send({
                error : false,
                merchants : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Merchants Found"
            });
        }
    });

});




// To get Merchant with their Id

router.get('/getMerchantById/', function(req, res) {

    var merchantId = req.query.merchantId;

    var query = "SELECT * FROM merchants WHERE merchant_id = "+merchantId+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                merchant : result[0]
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Merchant not found'
            });
        }
    });

});




// To get Merchant by their Names

router.get('/getMerchantByName/', function(req, res) {

    var merchantName = req.query.merchantName;

    var merchantQuery = "SELECT * FROM merchants WHERE merchant_name LIKE CONCAT('"+merchantName+"','%')";
    connection.query(merchantQuery, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                merchant : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Merchant not found'
            });
        }
    });

});




// To get Merchants with their Shop Name

router.get('/getMerchantByShop/', function(req, res) {

    var shopName = req.query.shopName;

    var merchantQuery = "SELECT * FROM merchants WHERE shop_name LIKE CONCAT('"+shopName+"','%')";
    connection.query(merchantQuery, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                merchant : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Merchant not found'
            });
        }
    });

});




// To get Merchant with their Email Id

router.get('/getMerchantByEmail/', function(req, res) {

    var merchantEmail = req.query.merchantEmail;

    var merchantQuery = "SELECT * FROM merchants WHERE merchant_email LIKE CONCAT('"+merchantEmail+"','%')";
    connection.query(merchantQuery, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                merchant : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Merchant not found'
            });
        }
    });

});




// To Update Merchant's Profile

router.post('/updateMerchantProfile', function(req, res) {

    var merchant_id = req.body.merchant_id;
    var merchant_name = req.body.merchant_name;
    var shop_name = req.body.shop_name;
    var merchant_email = req.body.merchant_email;
    var merchant_img = req.body.merchant_img;
    var merchant_contact = req.body.merchant_contact;
    var os_type = req.body.os_type;

    var query = "UPDATE merchants SET merchant_name = '"+merchant_name+"' , shop_name = '"+shop_name+"' , merchant_img = '"+merchant_img+"' , merchant_contact = '"+merchant_contact+"' , os_type = '"+os_type+"' WHERE merchant_id = "+merchant_id+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.affectedRows != 0) {
            var fetchDetailsQuery = "SELECT merchants.address,merchants.zip_code,merchants.os_type,merchants.fcm_id,merchants.admin_approval,merchant_catalogue.catalogue_id,merchant_inventory.inventory_id FROM merchants INNER JOIN merchant_catalogue ON merchant_catalogue.merchant_id = merchants.merchant_id INNER JOIN merchant_inventory ON merchant_inventory.merchant_id = merchants.merchant_id WHERE merchants.merchant_id = "+merchant_id+"";
            connection.query(fetchDetailsQuery, function(fetchDetailsErr, fetchDetailsResult) {
                if(fetchDetailsErr) {
                    res.status(200).send({
                        error : true,
                        message : fetchDetailsErr,
                    });
                }
                else if(fetchDetailsResult.length != 0) {
                    res.status(201).send({
                        error : false,
                        status : 'Updated',
                        merchant : {
                            merchant_id : merchant_id,
                            merchant_name : merchant_name,
                            shop_name : shop_name,
                            merchant_contact : merchant_contact,
                            merchant_email : merchant_email,
                            merchant_img : merchant_img,
                            address : fetchDetailsResult[0].address,
                            zip_code : fetchDetailsResult[0].zip_code,
                            os_type : os_type,
                            fcm_id : fetchDetailsResult[0].fcm_id,
                            admin_approval : fetchDetailsResult[0].admin_approval,
                            catalogue_id : fetchDetailsResult[0].catalogue_id,
                            inventory_id : fetchDetailsResult[0].inventory_id
                        }
                    });
                }
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Not updated',
            });
        }
    });

});




// Change Merchant's Password

router.post('/changeMerchantPassword', function(req, res) {

    var merchant_id = req.body.merchant_id;
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;

    var pwdQuery = "SELECT password FROM merchants WHERE merchant_id = "+merchant_id+"";
    connection.query(pwdQuery, function(pwdErr, pwdResult) {
        if(pwdErr) {
            res.status(200).send({
                error : true,
                message : pwdErr,
            });
        }
        else if(pwdResult != null && pwdResult[0] != null) {
            var decryptedOldPassword = module.exports.decrypt(pwdResult[0].password);
            if(decryptedOldPassword === old_password) {
                var encryptedNewPassword = module.exports.encrypt(new_password);
                var updateQuery = "UPDATE merchants SET password = '"+encryptedNewPassword+"' WHERE merchant_id = "+merchant_id+"";
                connection.query(updateQuery, function(updateErr, updateResult) {
                    if(updateErr) {
                        res.status(200).send({
                            error : true,
                            message : updateErr,
                        });
                    }
                    else if(updateResult.changedRows != 0) {
                        res.status(201).send({
                            error : false,
                            status : "Updated with new password",
                        });
                    }
                })
            }
            else {
                res.status(200).send({
                    error : true,
                    message : "Incorrect Old Password",
                });
            }
        }
        else {
            res.status(200).send({
                error : true,
                message : "Incorrect details",
            });
        }
    });

});




// Forget Merchant Password, for registered merchants

router.post('/forgotMerchantPassword', function(req, res) {

    var email = req.body.email;
    var user_type = req.body.user_type;
    var regeneratedOtp = module.exports.otpGenerator();

    var emailVerifyQuery = "SELECT email,user_type,otp,status FROM email_verify WHERE email LIKE '"+email+"' AND user_type LIKE '"+user_type+"'";
    connection.query(emailVerifyQuery, function(emailVerifyErr, emailVerifyResult) {
        if(emailVerifyErr) {
            res.status(200).send({
                error : true,
                message : emailVerifyErr
            });
        }
        else if(emailVerifyResult.length == 0) {
            res.status(200).send({
                error : true,
                message : "No User Found..."
            });
        }
        else if(emailVerifyResult.length != 0) {
            if(emailVerifyResult[0].email == email && emailVerifyResult[0].user_type == user_type) {
                var updateStatusQuery = "UPDATE email_verify SET otp = "+regeneratedOtp+" , status = 0 WHERE email LIKE '"+email+"' AND user_type LIKE '"+user_type+"'";
                connection.query(updateStatusQuery, function(updateStatusErr, updateStatusResult) {
                    if(updateStatusErr) {
                        res.status(200).send({
                            error : true,
                            message : updateStatusErr
                        });
                    }
                    else if(updateStatusResult.affectedRows != 0) {
                        var html = "<p>�Your OTP for PYG Re-Verification is : '"+regeneratedOtp+"'</p><br><br><strong>Thanks for registering with PYG</strong>";
                        Mail.sendMail(email, 'PYG Reset Password', html);
                        res.status(201).send({
                            error : false,
                            status : "OTP has been sent to registered email-id"
                        });
                    }
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    message : "User type mismatch",
                });
            }
        }
    });

});




// Reset Merchant Password

router.post('/resetMerchantPassword', function(req, res) {

    var email = req.body.email;
    var new_password = req.body.new_password;

    var passwordResetStatusCheckQuery = "SELECT status FROM email_verify WHERE email LIKE '"+email+"' AND user_type LIKE 'Merchant'";
    connection.query(passwordResetStatusCheckQuery, function(passwordResetStatusCheckErr, passwordResetStatusCheckResult) {
        if(passwordResetStatusCheckErr) {
            res.status(200).send({
                error : true,
                message : passwordResetStatusCheckErr
            });
        }
        else if(passwordResetStatusCheckResult.length != 0) {
            var encryptedNewPassword = module.exports.encrypt(new_password);
            if(passwordResetStatusCheckResult[0].status == 1) {
                var passwordResetQuery = "UPDATE merchants SET password = '"+encryptedNewPassword+"' WHERE merchant_email LIKE '"+email+"'";
                connection.query(passwordResetQuery, function(passwordResetErr, passwordResetResult) {
                    if(passwordResetErr) {
                        res.status(200).send({
                            error : true,
                            message : passwordResetErr
                        });
                    }
                    else if(passwordResetResult.affectedRows != 0) {
                        res.status(201).send({
                            error : false,
                            status : "Password altered"
                        });
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : "Problem in reset password"
                        });
                    }
                });
            }
            else if(passwordResetStatusCheckResult[0].status == 0) {
                res.status(200).send({
                    error : true,
                    message : "Complete your email verification"
                });
            }
        }
        else {
            res.status(200).send({
                error : true,
                message : "Error in fetching status"
            });
        }
    });

});




// Update Merchant's last login

router.post('/merchantLastLogin', function(req, res) {

    var merchant_id = req.body.merchant_id;
    var lastlogin = req.body.lastlogin;

    var updateLastLoginQuery = "UPDATE merchants SET lastlogin = '"+lastlogin+"' WHERE merchant_id = "+merchant_id+"";
    connection.query(updateLastLoginQuery, function(updateLastLoginErr, updateLastLoginResult) {
        if(updateLastLoginErr) {
            res.status(200).send({
                error : true,
                message : updateLastLoginErr
            });
        }
        else if(updateLastLoginResult.affectedRows != 0) {
            res.status(201).send({
                error : false,
                status : 'Last login updated',
                lastLogin : lastlogin
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Problem in updating last login'
            });
        }
    });

});




// Change FCM ID on dynamic generation

router.post('/updateMerchantFCM', function(req, res) {

    var merchant_id = req.body.merchant_id;
    var os_type = req.body.os_type;
    var fcm_id = req.body.fcm_id;

    var fcmUpdateQuery = "UPDATE merchants SET fcm_id = '"+fcm_id+"' WHERE merchant_id = "+merchant_id+" AND os_type LIKE '"+os_type+"'";
    connection.query(fcmUpdateQuery, function(fcmUpdateErr, fcmUpdateResult) {
        if(fcmUpdateErr) {
            res.status(200).send({
                error : true,
                message : fcmUpdateErr
            });
        }
        else if(fcmUpdateResult.affectedRows != 0) {
            res.status(201).send({
                error : false,
                status : "FCM ID Updated",
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "Problem in updating FCM ID"
            })
        }
    });

});




// TO Get Pending Orders for a particular merchant

router.get('/getPendingOrders/', function(req, res) {

    var merchantId = req.query.merchantId;
    var orders = new Array();

    var orderQuery = "SELECT order_id FROM orders WHERE merchant_id = "+merchantId+" AND order_status = 1 ORDER BY order_time DESC";
    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult.length != 0) {
            orderResult.forEach(function(order) {
               
                var gatherOrdersQuery = "SELECT orders.*, users.name as user_name, users.user_img, order_status.order_status_name FROM users, orders, order_status WHERE orders.order_status = order_status.order_status_id AND users.user_id = orders.user_id AND order_id = "+order.order_id+" ORDER BY orders.order_time DESC";
                connection.query(gatherOrdersQuery, function(gatherOrdersErr, gatherOrdersResult) {
                    if(gatherOrdersErr) {
                        res.status(200).send({
                            error : true,
                            message : gatherOrdersErr
                        });
                    }
                    else {
                        orders.push(gatherOrdersResult[0]);
                    }
                });
            });

            setTimeout(function() {
                res.status(200).send({
                    error : false,
                    orders : orders
                });
            }, 2000);
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });

});




// To get 'Approved and Processing' Orders for a particular merchant

router.get('/getApprovedOrders/', function(req, res) {

    var merchantId = req.query.merchantId;
    var orders = new Array();

    var orderQuery = "SELECT order_id FROM orders WHERE merchant_id = "+merchantId+" AND order_status = 2 ORDER BY order_time DESC";
    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult.length != 0) {
            orderResult.forEach(function(order) {

                var gatherOrdersQuery = "SELECT orders.*, users.name as user_name, users.user_img, order_status.order_status_name FROM users, orders, order_status WHERE orders.order_status = order_status.order_status_id AND users.user_id = orders.user_id AND order_id = "+order.order_id+" ORDER BY orders.order_time DESC";
                connection.query(gatherOrdersQuery, function(gatherOrdersErr, gatherOrdersResult) {
                    if(gatherOrdersErr) {
                        res.status(200).send({
                            error : true,
                            message : gatherOrdersErr
                        });
                    }
                    else {
                        orders.push(gatherOrdersResult[0]);
                    }
                });
            });

            setTimeout(function() {
                res.status(200).send({
                    error : false,
                    orders : orders
                });
            }, 2000);
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });
});




// To get 'Ready to Deliver' Orders for a particular merchant

router.get('/getReadyToDeliverOrders/', function(req, res) {

    var merchantId = req.query.merchantId;
    var orders = new Array();

    var orderQuery = "SELECT order_id FROM orders WHERE merchant_id = "+merchantId+" AND order_status = 3 ORDER BY order_time DESC";
    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult.length != 0) {
            orderResult.forEach(function(order) {

                var gatherOrdersQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON orders.order_status = order_status.order_status_id WHERE order_id = "+order.order_id+" ORDER BY orders.order_time DESC";
                connection.query(gatherOrdersQuery, function(gatherOrdersErr, gatherOrdersResult) {
                    if(gatherOrdersErr) {
                        res.status(200).send({
                            error : true,
                            message : gatherOrdersErr
                        });
                    }
                    else {
                        orders.push(gatherOrdersResult[0]);
                    }
                });
            });

            setTimeout(function() {
                res.status(200).send({
                    error : false,
                    orders : orders
                });
            }, 2000);
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });
});




// To get 'Delivered' Orders for a particular merchant
router.get('/getDeliveredOrders/', function(req, res) {

    var merchantId = req.query.merchantId;
    var orders = new Array();

    var orderQuery = "SELECT order_id FROM orders WHERE merchant_id = "+merchantId+" AND order_status IN(4,5,6) ORDER BY order_time DESC";
    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult.length != 0) {
            orderResult.forEach(function(order) {

                var gatherOrdersQuery = "SELECT orders.*, users.name as user_name, users.user_img, order_status.order_status_name FROM users, orders, order_status WHERE orders.order_status = order_status.order_status_id AND users.user_id = orders.user_id AND order_id = "+order.order_id+" ORDER BY orders.order_time DESC";
                connection.query(gatherOrdersQuery, function(gatherOrdersErr, gatherOrdersResult) {
                    if(gatherOrdersErr) {
                        res.status(200).send({
                            error : true,
                            message : gatherOrdersErr
                        });
                    }
                    else {
                        orders.push(gatherOrdersResult[0]);
                    }
                });
            });

            setTimeout(function() {
                res.status(200).send({
                    error : false,
                    orders : orders
                });
            }, 2000);
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });
});




// To get 'Cancelled' Orders for a particular merchant
router.get('/getCancelledOrders/', function(req, res) {

    var merchantId = req.query.merchantId;
    var orders = new Array();

    var orderQuery = "SELECT order_id FROM orders WHERE merchant_id = "+merchantId+" AND order_status = 5 ORDER BY order_time DESC";
    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult.length != 0) {
            orderResult.forEach(function(order) {

                var gatherOrdersQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON orders.order_status = order_status.order_status_id WHERE order_id = "+order.order_id+" ORDER BY orders.order_time DESC";
                connection.query(gatherOrdersQuery, function(gatherOrdersErr, gatherOrdersResult) {
                    if(gatherOrdersErr) {
                        res.status(200).send({
                            error : true,
                            message : gatherOrdersErr
                        });
                    }
                    else {
                        orders.push(gatherOrdersResult[0]);
                    }
                });
            });

            setTimeout(function() {
                res.status(200).send({
                    error : false,
                    orders : orders
                });
            }, 2000);
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });
});




// Pick Orders between specific time range

router.get('/getOrdersInTimePeriod/', function(req, res) {

    var merchantId = req.query.merchantId;
    var fromDate = req.query.fromDate;
    var toDate = req.query.toDate;

    var ordersQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status WHERE orders.order_status = 4 AND ((DATE(orders.order_time) BETWEEN DATE('"+fromDate+"') AND DATE('"+toDate+"')) AND orders.merchant_id = "+merchantId+") ORDER BY orders.order_time DESC";
    connection.query(ordersQuery, function(ordersErr, ordersResult) {
        if(ordersErr) {
            res.status(200).send({
                error : true,
                message : ordersErr
            });
        }
        else if(ordersResult.length != 0) {
            res.status(200).send({
                error : false,
                orders : ordersResult
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });

});

// Pick Orders between specific time range Revised

router.get('/getOrdersInTimePeriodRevised/', function(req, res) {

    var merchantId = req.query.merchantId;
    var fromDate = req.query.fromDate;
    var toDate = req.query.toDate;

    var ordersQuery = "SELECT users.name as user_name, users.user_img, orders.*, order_status.order_status_name FROM orders, users, order_status WHERE order_status.order_status_id = orders.order_status AND users.user_id=orders.user_id AND orders.order_status = 4 AND ((DATE(orders.order_time) BETWEEN DATE('"+fromDate+"') AND DATE('"+toDate+"')) AND orders.merchant_id = "+merchantId+") ORDER BY orders.order_time DESC";
    connection.query(ordersQuery, function(ordersErr, ordersResult) {
        if(ordersErr) {
            res.status(200).send({
                error : true,
                message : ordersErr
            });
        }
        else if(ordersResult.length != 0) {

            var overallResult = new Array();
            ordersResult.forEach(function(order, orderIndex) {

                // var feedbackQuery = "SELECT rating, review, users.name as user_name FROM users, feedback WHERE users.user_id = feedback.user_id AND merchant_id = "+merchantId+" AND order_id = "+order.order_id+" ORDER BY feedback_number DESC"
                var feedbackQuery = "SELECT rating, review FROM feedback WHERE merchant_id = "+merchantId+" AND order_id = "+order.order_id+" ORDER BY feedback_number DESC";
                connection.query(feedbackQuery, function(feedbackError, feedbackResult) {
                    if(feedbackError) {
                        res.status(200).send({
                            error : true,
                            message : feedbackError
                        });
                    }
                    else if(feedbackResult.length != 0){
                        setTimeout(function() {
                            temp_order = {
                                order_id: order.order_id,
                                user_id: order.user_id,
                                user_name: order.user_name,
                                user_image: order.user_img,
                                merchant_id: order.merchant_id,
                                products_id: order.products_id,
                                qty: order.qty,
                                price: order.price,
                                total_price: order.total_price,
                                taxes: order.taxes,
                                bill_amount: order.bill_amount,
                                tracking_id: order.tracking_id,
                                order_time: order.order_time,
                                collecting_time: order.collecting_time,
                                order_status: order.order_status,
                                payment_mode: order.payment_mode,
                                payment_status: order.payment_status,
                                transaction_id: order.transaction_id,
                                merchant_approved: order.merchant_approved,
                                order_status_name: order.order_status_name,
                                rating: feedbackResult[0].rating,
                                review: feedbackResult[0].review
                            }
                            overallResult[orderIndex] = temp_order;
                        }, 2000);
                    } 
                    else {
                        setTimeout(function() {
                            temp_order = {
                                order_id: order.order_id,
                                user_id: order.user_id,
                                user_name: order.user_name,
                                user_image: order.user_img,
                                merchant_id: order.merchant_id,
                                products_id: order.products_id,
                                qty: order.qty,
                                price: order.price,
                                total_price: order.total_price,
                                taxes: order.taxes,
                                bill_amount: order.bill_amount,
                                tracking_id: order.tracking_id,
                                order_time: order.order_time,
                                collecting_time: order.collecting_time,
                                order_status: order.order_status,
                                payment_mode: order.payment_mode,
                                payment_status: order.payment_status,
                                transaction_id: order.transaction_id,
                                merchant_approved: order.merchant_approved,
                                order_status_name: order.order_status_name,
                                rating: null,
                                review: null
                            }
                            overallResult[orderIndex] = temp_order;
                        }, 2000);
                    }
                });
            });
            setTimeout(function() {
                res.status(200).send({
                    error : false,
                    orders : overallResult
                });
            }, 3000);  
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });
});




// Display the 'Categories' in Catalogue

router.get('/getCatalogueCategories/', function(req, res) {

    //Required Params
    var merchantId = req.query.merchantId;
    var catalogueCategories = new Array();

    var searchQuery = "SELECT * FROM merchant_catalogue WHERE merchant_id = "+merchantId+"";
    connection.query(searchQuery, function(searchErr, searchResult) {
        if(searchErr) {
            res.status(200).send({
                error : true,
                message : searchErr
                //check : 1
            });        
        }
        else if(searchResult[0].categories_id != null) {
            
            // Split the products into array
            if(searchResult[0].categories_id.indexOf(',') != -1) {

                var categoriesArr = searchResult[0].categories_id.split(',');
                categoriesArr.forEach(function(item) {
                    var categoriesQuery = "SELECT category_id,category_name,category_image,products FROM categories WHERE category_id = "+item+"";
                    connection.query(categoriesQuery, function(categoriesErr, categoriesResult) {
                        if(categoriesErr) {
                            res.status(200).send({
                                error : true,
                                message : categoriesErr
                                //check : 2
                            });
                        }
                        else if(categoriesResult.length != 0) {
                            var temp_arr = {
                                category_id : categoriesResult[0].category_id,
                                category_name : categoriesResult[0].category_name,
                                category_image : categoriesResult[0].category_image
                            };
                            catalogueCategories.push(temp_arr);
                        }
                        else {
                            res.status(200).send({
                                error : true,
                                message : "Problem in fetching category details"
                                //check : 3
                            });
                        }
                    });
                });
            }
            // If catalogue contains only 1 item
            else if(searchResult[0].categories_id.indexOf(',') == -1) {
                
                var singleCategoryQuery = "SELECT category_id,category_name,category_image,products FROM categories WHERE category_id = "+searchResult[0].categories_id+"";
                connection.query(singleCategoryQuery, function(singleCategoryErr, singleCategoryResult) {
                    if(singleCategoryErr) {
                        res.status(200).send({
                            error : true,
                            message : singleCategoryErr
                            //check : 4
                        });
                    }
                    else if(singleCategoryResult.length != 0) {
                        var temp_arr = {
                            category_id : singleCategoryResult[0].category_id,
                            category_name : singleCategoryResult[0].category_name,
                            category_image : singleCategoryResult[0].category_image
                        };
                        catalogueCategories.push(temp_arr);
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : "Problem in fetching category details"
                            //check : 5
                        });
                    }
                });              
            }

            // Set Results
            setTimeout(function() {
                if(catalogueCategories.length != 0) {
                    res.status(200).send({
                        error : false,
                        merchant_id : searchResult[0].merchant_id,
                        catalogue_id : searchResult[0].catalogue_id,
                        catalogue : catalogueCategories
                        //check : 6
                    });
                }
                else {
                    res.status(200).send({
                        error : true,
                        caatalogue : "No Categories found"
                        //check : 7
                    });
                }
            }, 2000);
        }
        else {
            res.status(200).send({
                error : true,
                caatalogue : "No Categories found"
                //check : 8
            });
        }
    }); 

});




// Display the 'Products' in Catalogue

router.get('/getCatalogueProducts/', function(req, res) {

    //Required Params
    var merchantId = req.query.merchantId;
    var catalogueProducts = new Array();

    var searchQuery = "SELECT * FROM merchant_catalogue WHERE merchant_id = "+merchantId+"";
    connection.query(searchQuery, function(searchErr, searchResult) {
        if(searchErr) {
            res.status(200).send({
                error : true,
                message : searchErr
            });
        }
        else if(searchResult[0].products_id != null) {
            
            // If catalogue contains multiple products
            if(searchResult[0].products_id.indexOf(',') != -1) {

                //Split products to array
                var productsArr = searchResult[0].products_id.split(',');
                productsArr.forEach(function(item) {

                    var productsQuery = "SELECT products.*, categories.category_name FROM products LEFT JOIN categories ON categories.category_id = products.category_id WHERE products.product_id = "+item+"";
                    connection.query(productsQuery, function(productsErr, productsResult) {
                        if(productsErr) {
                            res.status(200).send({
                                error : true,
                                message : productsErr
                            });
                        }
                        else if(productsResult.length != 0) {
                            var temp_arr = {
                                product_id : productsResult[0].product_id,
                                product_name : productsResult[0].product_name,
                                product_image : productsResult[0].product_image,
                                category_id : productsResult[0].category_id,
                                category_name : productsResult[0].category_name,
                                product_description : productsResult[0].product_description,
                                sku : productsResult[0].sku,
                                quantification : productsResult[0].quantification
                            }
                            catalogueProducts.push(temp_arr);
                        }
                        else {
                            res.status(200).send({
                                error : true,
                                message : "No product details found"
                            });
                        }
                    });
                });
            }
            // If Catalogue contains only 1 product
            else if(searchResult[0].products_id.indexOf(',') == -1) {

                var singleProductQuery = "SELECT products.*, categories.category_name FROM products LEFT JOIN categories ON categories.category_id = products.category_id WHERE products.product_id = "+searchResult[0].products_id+"";
                connection.query(singleProductQuery, function(singleProductErr, singleProductResult) {
                    if(singleProductErr) {
                        res.status(200).send({
                            error : true,
                            message : singleProductErr
                        });
                    }
                    else if(singleProductResult.length != 0) {
                        var temp_arr = {
                            product_id : singleProductResult[0].product_id,
                            product_name : singleProductResult[0].product_name,
                            product_image : singleProductResult[0].product_image,
                            category_id : singleProductResult[0].category_id,
                            category_name : singleProductResult[0].category_name,
                            product_description : singleProductResult[0].product_description,
                            sku : singleProductResult[0].sku,
                            quantification : singleProductResult[0].quantification
                        }
                        catalogueProducts.push(temp_arr);
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : "No product details found"
                        });
                    }
                });
            }

            //Set results
            setTimeout(function() {
                if(catalogueProducts.length != 0) {
                    res.status(200).send({
                        error : false,
                        merchant_id : searchResult[0].merchant_id,
                        catalogue_id : searchResult[0].catalogue_id,
                        products : catalogueProducts
                    });
                }
                else {
                    res.status(200).send({
                        error : true,
                        products : "No products found"
                    });
                }
            }, 2000);
        }

        else {
            res.status(200).send({
                error : true,
                products : "No products found"
            });
        }
    });
    
});




// Get categories merchant is selling from selected merchant

router.get('/getMerchantCategories/', function(req, res) {

    var merchantId = req.query.merchantId;

    var inventoryQuery = "SELECT inventory_id,merchant_id,categories_id FROM merchant_inventory WHERE merchant_id = "+merchantId+"";
    connection.query(inventoryQuery, function(inventoryErr, inventoryResult) {
        if(inventoryErr) {
            res.status(200).send({
                error : true,
                message : inventoryErr
            });
        }
        else if(inventoryResult.length != 0 && inventoryResult[0].categories_id != null) {
            var categoriesRepititive = inventoryResult[0].categories_id;
            var categoriesRepititiveArr = categoriesRepititive.split(',');
            var categoriesArr = categoriesRepititiveArr.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            });
            for(var i = 0; i < categoriesArr.length; i++) {
                var categoriesQuery = "SELECT category_id,category_name,category_image FROM categories WHERE category_id = "+categoriesArr[i]+"";
                connection.query(categoriesQuery, function(categoriesErr, categoriesResult) {
                    if(categoriesErr) {
                        res.status(200).send({
                            error : true,
                            message : categoriesErr,
                        });
                    }
                    else {
                        module.exports.getcategoriesByMerchant(categoriesResult[0]);
                    }
                });                
            }
            var categoriesWithMerchant = module.exports.getcategoriesByMerchant(1);
            setTimeout(function() {
                if(categoriesWithMerchant.length != 0) {
                    module.exports.getcategoriesByMerchant(0);    
                    res.status(200).send({
                        error : false,
                        inventory_id : inventoryResult[0].inventory_id,
                        merchant_id : inventoryResult[0].merchant_id,
                        categories : categoriesWithMerchant
                    });
                }
                else {
                    res.status(200).send({
                        error : true,
                        message : 'No categories found',
                    });
                }
            }, 1000);
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No categories found',
            });            
        }
    });
    
});


// Function used in the API getMerchantProducts to get product details(removed duplication)
var productsWithMerchant = [];
function setProductDetails(productResult, viewProductDetails) {

    if(productResult == 1) {
        return productsWithMerchant;
    }
    else if(productResult == 0) {
        productsWithMerchant = [];
    }
    else {
        viewProductDetails.add(productResult);
        return viewProductDetails;
    }
}


// Get Products and their details on selected category available with selected merchant

router.get('/getMerchantProducts/', function(req, res) {
	
	var merchantId = req.query.merchantId;
    var categoryId = req.query.categoryId;
    
    var functionRes = new Set();
    var viewProduct = new Set();

	var query = "SELECT * FROM merchant_inventory WHERE merchant_id = "+merchantId+"";
	connection.query(query, function(err, result) {
		if(err) {
			res.status(200).send({
				error : true,
				message : err
			});
		}
		else if(result[0].products_id != null && result[0].products_id != '') {
			
			var productsArr = result[0].products_id.split(',');
			var categoriesArr = result[0].categories_id.split(',');
			var skuArr = result[0].sku.split(',');
			var availabilityArr = result[0].available_for_sale.split(',');
			var quantificationArr = result[0].quantification.split(',');
			var defaultPriceArr = result[0].default_price.split(',');
			var sellingPriceArr = result[0].selling_price.split(',');
			var pygPriceArr = result[0].pyg_price.split(',');
			var seasonalArr = result[0].seasonal.split(',');
			var availableQtyArr = result[0].available_qty.split(',');
			var lowStockArr = result[0].low_stock.split(',');
			var categoriesIndex = new Array();
            var productDetails = new Array();
			
			var strCategoryId = categoryId.toString();
			for(var i = 0; i < categoriesArr.length; i++) {
				if(strCategoryId == categoriesArr[i]) {
					categoriesIndex.push(i);
				}
            }
            
			categoriesIndex.forEach(function(k) {
                
                var productIdInteger = parseInt(productsArr[k],10);

				var productNameQuery = "SELECT products.product_name, products.product_image, products.category_id, products.product_description, categories.category_name FROM products LEFT JOIN categories ON products.category_id = categories.category_id WHERE products.product_id = "+productIdInteger+"";
				connection.query(productNameQuery, function(productNameErr, productNameResult) {
					if(productNameErr) {
						res.status(200).send({
							error : true,
							message : productNameErr
						});
					}
					else {
						temp_arr = {
                            category_id : productNameResult[0].category_id,
                            category_name : productNameResult[0].category_name,
							product_id : productsArr[k],
							product_name : productNameResult[0].product_name,
                            product_img : productNameResult[0].product_image,
                            product_description : productNameResult[0].product_description,
							sku : skuArr[k],
							availability : availabilityArr[k],
							quantification : quantificationArr[k],
							default_price : defaultPriceArr[k],
							selling_price : sellingPriceArr[k],
							pyg_price : pygPriceArr[k],
							seasonal : seasonalArr[k],
							available_qty : availableQtyArr[k],
							low_stock : lowStockArr[k]
                        };
                        functionRes = setProductDetails(temp_arr, viewProduct);
                    }
                });
            });
            productDetails = module.exports.setProductDetails(1);
            setTimeout(function() {
                if(functionRes.length != 0) {
                    module.exports.setProductDetails(0);
                    res.status(200).send({
                        error : false,
                        merchant_id : result[0].merchant_id,
                        inventory_id : result[0].inventory_id,
                        products : Array.from(functionRes)
                    });
                }
                else {
                    res.status(200).send({
                        error : true,
                        message : 'No Products Found',
                    });
                }
            }, 1000);
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Products Found',
            });
        }       
    });

});




// Get all categories

router.get('/getAllCategories', function(req, res) {

    var query = "SELECT * FROM categories";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                categories : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Categories found"
            });
        }
    });

});




// Get category by id

router.get('/getCategoryById/', function(req, res) {

    //Required Params
    var categoryId = req.query.categoryId;

    var categoryDetailQuery = "SELECT * FROM categories WHERE category_id = "+categoryId+"";
    connection.query(categoryDetailQuery, function(categoryDetailsErr, categoryDetailResult) {
        if(categoryDetailsErr) {
            res.status(200).send({
                error : true,
                message : categoryDetailsErr
            });
        }
        else if(categoryDetailResult.length != 0) {
            res.status(200).send({
                error : false,
                category : categoryDetailResult[0]
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "Problem in fetching Category details"
            });
        }
    });

});




// Get Category by Name

router.get('/getCategoryByName/', function(req, res) {

    var categoryName = req.query.categoryName;

    var query = "SELECT * FROM categories WHERE category_name LIKE CONCAT('"+categoryName+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                category : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Category found',
            });
        }
    });

});




// Search Categories available with merchants

router.get('/searchMerchantCategories/', function(req, res) {

    var merchantId = req.query.merchantId;
    var categoryName = req.query.categoryName;

    var urls = new Array("http://52.43.215.200:8000/pyg/merchant/getCategoryByName/?categoryName="+categoryName+"" , "http://52.43.215.200:8000/pyg/merchant/getMerchantCategories/?merchantId="+merchantId+"");
    var urlResult1;
    var urlResult2;
    var searchResult = new Array();

    urls.forEach(function(url, index) {
        if(index == 0) { 
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult1 = body.category;
                }
            });
        }
        else if(index == 1) {
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult2 = body.categories;
                }
            });
        }
    });
    setTimeout(function() {
        urlResult1.forEach(function(commonResult) {
            urlResult2.forEach(function(merchantResult) {
                if(commonResult.category_name == merchantResult.category_name) {
                    getResultFromOtherAPI(merchantResult);                                        
                }
            });
        });
        searchResult = getResultFromOtherAPI(1);
        setTimeout(function() {
            getResultFromOtherAPI(0);
            if(searchResult.length != 0) {
                res.status(200).send({
                    error : false,
                    searchResults : searchResult
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    searchResults : "No Categories Found"
                });
            }
        }, 1000);
    }, 2000);

});




// Get all Products from 'PYG'

router.get('/getAllProducts', function(req, res) {

    var query = "SELECT * FROM products";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                products : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Products found"
            });
        }
    });

});




// Get Category by Id

router.get('/getProductById/', function(req, res) {

    var productId = req.query.productId;

    var query = "SELECT * FROM products WHERE product_id = "+productId+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                product : result[0],
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Product found',
            })
        }
    });

});




// Get Category by Name

router.get('/getProductByName/', function(req, res) {

    var productName = req.query.productName;

    var query = "SELECT * FROM products WHERE product_name LIKE CONCAT('"+productName+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                product : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Product found',
            });
        }
    });
});




// Search Products available with merchants

router.get('/searchMerchantProducts/', function(req, res) {

    var merchantId = req.query.merchantId;
    var categoryId = req.query.categoryId;
    var productName = req.query.productName;

    var urls = new Array("http://52.43.215.200:8000/pyg/merchant/getProductByName/?productName="+productName+"" , "http://52.43.215.200:8000/pyg/merchant/getMerchantProducts/?merchantId="+merchantId+"&categoryId="+categoryId+"");
    var urlResult1;
    var urlResult2;
    var searchResult = new Array();

    urls.forEach(function(url, index) {
        if(index == 0) { 
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult1 = body.product;
                }
            });
        }
        else if(index == 1) {
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult2 = body.products;
                }
            });
        }
    });
    setTimeout(function() {
        urlResult1.forEach(function(commonResult) {
            urlResult2.forEach(function(merchantResult) {
                if(commonResult.product_name == merchantResult.product_name) {
                    getResultFromOtherAPI(merchantResult);
                }
            });
        });
        searchResult = getResultFromOtherAPI(1);
        setTimeout(function() {
            getResultFromOtherAPI(0);
            if(searchResult.length != 0) {
                res.status(200).send({
                    error : false,
                    searchResults : searchResult
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    searchResults : "No Categories Found"
                });
            }
        }, 1000);
    }, 2000);

});




// Get all products based on category id

router.get('/getProductsByCategoryId/', function(req, res) {

    // Required Params
    var categoryId = req.query.categoryId;

    var productQuery = "SELECT * FROM products WHERE category_id = "+categoryId+"";
    connection.query(productQuery, function(productErr, productResult) {
        if(productErr) {
            res.status(200).send({
                error : true,
                message : productErr
            });
        }
        else if(productResult.length != 0) {
            res.status(200).send({
                error : false,
                products : productResult
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Products"
            });
        }
    });

});




// Get all products based on category name

router.get('/getProductsByCategoryName/', function(req, res) {

    // Required Params
    var categoryName = req.query.categoryName;

    var categoryQuery = "SELECT DISTINCT category_id FROM categories WHERE category_name LIKE CONCAT('"+categoryName+"','%') LIMIT 1";
    connection.query(categoryQuery, function(categoryErr, categoryResult) {
        if(categoryErr) {
            res.status(200).send({
                error : true,
                message : categoryErr
            });
        }
        else if(categoryResult.length != 0) {
            
            //Pick products
            var productQuery = "SELECT * FROM products WHERE category_id = "+categoryResult[0].category_id+"";
            connection.query(productQuery, function(productErr, productResult) {
                setTimeout(function() {
                    if(productErr) {
                        res.status(200).send({
                            error : true,
                            message : productErr
                        });
                    }
                    else if(productResult.length != 0) {
                        res.status(200).send({
                            error : false,
                            products : productResult
                        });
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : "No Product found"
                        });
                    }
                }, 1000);
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Categories found"
            });
        }
    });

});


// Add inventory to Merchant

router.post('/addInventoryToMerchant', function(req, res) {

    //Required Params
    var merchant_id = req.body.merchant_id;
    var category_id = req.body.category_id;
    var product_id = req.body.product_id;
    var sku = req.body.sku;
    var available_for_sale = req.body.available_for_sale;
    var quantification = req.body.quantification;
    var default_price = req.body.default_price;
    var selling_price = req.body.selling_price;
    var pyg_price = req.body.pyg_price;
    var seasonal = req.body.seasonal;
    var available_qty = req.body.available_qty;
    var low_stock = req.body.low_stock

    var catalogueCategories = new Array();
    var catalogueProducts = new Array();
    var inventoryProducts = new Array();

     // Check Product is present in database
    var repititionQuery = "Select * FROM products WHERE product_id LIKE '"+product_id+"'";
    connection.query(repititionQuery, function(repititionErr, repititionResult) {
        if(repititionErr) {
            res.status(200).send({
                error : true,
                message : repititionErr
            });
        }
        else if(repititionResult.length != 0) {
            // Check merchant's catalogue whether category and product is available or not
            var catalogueCheckQuery = "SELECT * FROM merchant_catalogue WHERE merchant_id = "+merchant_id+"";
            connection.query(catalogueCheckQuery, function(catalogueCheckErr, catalogueCheckResult) {
                if(catalogueCheckErr) {
                    res.status(200).send({
                        error : true,
                        message : catalogueCheckErr
                    });
                }
                else if(catalogueCheckResult.length != 0) {
                    //Split Categories
                    if((catalogueCheckResult[0].categories_id != null) && (catalogueCheckResult[0].categories_id.indexOf(',') != -1)) {
                        catalogueCategories = catalogueCheckResult[0].categories_id.split(',');
                    }
                    else if((catalogueCheckResult[0].categories_id != null)&&(catalogueCheckResult[0].categories_id.indexOf(',') == -1)) {
                        catalogueCategories.push(catalogueCheckResult[0].categories_id);
                    }
                    //Split Products
                    if((catalogueCheckResult[0].products_id != null) && (catalogueCheckResult[0].products_id.indexOf(',') != -1)) {
                        catalogueProducts = catalogueCheckResult[0].products_id.split(',');
                    }
                    else if((catalogueCheckResult[0].products_id != null) && (catalogueCheckResult[0].products_id.indexOf(',') == -1)) {
                        catalogueProducts.push(catalogueCheckResult[0].products_id);
                    }

                    // Check Category is exist in catalogue or not

                    // What if both categories and products exists in catalogue
                    if((catalogueCategories.indexOf(category_id) != -1) && (catalogueProducts.indexOf(product_id) != -1)) {
                        module.exports.setCatalogueApproval(1);
                    }

                    // What if category exists and product not exists in catalogue
                    else if((catalogueCategories.indexOf(category_id) != -1) && (catalogueProducts.indexOf(product_id) == -1)) {
                        // Add Product to catalogue
                        // If 'products_id' in catalogue is NotNull
                        if(catalogueCheckResult[0].products_id != null) {
                            var updatedProductsInCatalogue = catalogueCheckResult[0].products_id + ","+product_id+"";
                            var updateProductsQuery = "UPDATE merchant_catalogue SET products_id = '"+updatedProductsInCatalogue+"' WHERE merchant_id = "+merchant_id+"";
                            connection.query(updateProductsQuery, function(updateProductsErr, updateProductsResult) {
                                if(updateProductsErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : updateProductsErr
                                    });
                                }
                                else if(updateProductsResult.affectedRows != 0) {
                                    module.exports.setCatalogueApproval(1);
                                }
                                else {
                                    module.exports.setCatalogueApproval(0);
                                }
                            });
                        }

                        // If 'products_id' in catalogue is NULL
                            else if(catalogueCheckResult[0].products_id == null) {
                                var updateProductsQuery = "UPDATE merchant_catalogue SET products_id = '"+product_id+"' WHERE merchant_id = "+merchant_id+"";
                                connection.query(updateProductsQuery, function(updateProductsErr, updateProductsResult) {
                                    if(updateProductsErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : updateProductsErr
                                        });
                                    }
                                    else if(updateProductsResult.affectedRows != 0) {
                                        module.exports.setCatalogueApproval(1);
                                    }
                                    else {
                                        module.exports.setCatalogueApproval(0);
                                    }
                                });        
                            }
                    }

                    // What if product exists and category not exists in catalogue (Very rare case)
                    else if((catalogueCategories.indexOf(category_id) == -1) && (catalogueProducts.indexOf(product_id) != -1)) {
                        
                        // If 'categories_id' in catalogue is NotNull
                        if(catalogueCheckResult[0].categories_id != null) {
                            var updatedCategoriesInCatalogue = catalogueCheckResult[0].categories_id + ","+category_id+"";
                            var updateCategoriesQuery = "UPDATE merchant_catalogue SET categories_id = '"+updatedCategoriesInCatalogue+"' WHERE merchant_id = "+merchant_id+"";
                            connection.query(updateCategoriesQuery, function(updateCategoriesErr, updateCategoriesResult) {
                                if(updateCategoriesErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : updateCategoriesErr
                                    });
                                }
                                else if(updateCategoriesResult.affectedRows != 0) {
                                    module.exports.setCatalogueApproval(1);
                                }
                                else {
                                    module.exports.setCatalogueApproval(0);
                                }        
                            });
                        }

                        // If 'categories_id' in catalogue is NULL
                            else if(catalogueCheckResult[0].categories_id == null) {
                                var updateCategoriesQuery = "UPDATE merchant_catalogue SET categories_id = '"+category_id+"' WHERE merchant_id = "+merchant_id+"";
                                connection.query(updateCategoriesQuery, function(updateCategoriesErr, updateCategoriesResult) {
                                    if(updateCategoriesErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : updateCategoriesErr
                                        });
                                    }
                                    else if(updateCategoriesResult.affectedRows != 0) {
                                        module.exports.setCatalogueApproval(1);
                                    }
                                    else {
                                        module.exports.setCatalogueApproval(0);
                                    }        
                                });
                            }
                    }

                    // What if both 'categories_id' and 'products_id' not exists in catalogue
                    else if((catalogueCategories.indexOf(category_id) == -1) && (catalogueProducts.indexOf(product_id) == -1)) {
                        
                        // If 'categories_id' is NotNull
                        if(catalogueCheckResult[0].categories_id != null) {
                            var updatedCategoriesInCatalogue = catalogueCheckResult[0].categories_id + ","+category_id+"";
                            var updateCategoriesQuery = "UPDATE merchant_catalogue SET categories_id = '"+updatedCategoriesInCatalogue+"' WHERE merchant_id = "+merchant_id+"";
                            connection.query(updateCategoriesQuery, function(updateCategoriesErr, updateCategoriesResult) {
                                if(updateCategoriesErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : updateCategoriesErr
                                    });
                                }
                                else if(updateCategoriesResult.affectedRows != 0) {
                                    module.exports.setCatalogueApproval(1);
                                }
                                else {
                                    module.exports.setCatalogueApproval(0);
                                }        
                            });
                        }

                        // If 'categories_id' in catalogue is NULL
                            else if(catalogueCheckResult[0].categories_id == null) {
                                var updateCategoriesQuery = "UPDATE merchant_catalogue SET categories_id = '"+category_id+"' WHERE merchant_id = "+merchant_id+"";
                                connection.query(updateCategoriesQuery, function(updateCategoriesErr, updateCategoriesResult) {
                                    if(updateCategoriesErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : updateCategoriesErr
                                        });
                                    }
                                    else if(updateCategoriesResult.affectedRows != 0) {
                                        module.exports.setCatalogueApproval(1);
                                    }
                                    else {
                                        module.exports.setCatalogueApproval(0);
                                    }        
                                });
                            }

                        // If 'products_id' in catalogue is NotNull
                        if(catalogueCheckResult[0].products_id != null) {
                            var updatedProductsInCatalogue = catalogueCheckResult[0].products_id + ","+product_id+"";
                            var updateProductsQuery = "UPDATE merchant_catalogue SET products_id = '"+updatedProductsInCatalogue+"' WHERE merchant_id = "+merchant_id+"";
                            connection.query(updateProductsQuery, function(updateProductsErr, updateProductsResult) {
                                if(updateProductsErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : updateProductsErr
                                    });
                                }
                                else if(updateProductsResult.affectedRows != 0) {
                                    module.exports.setCatalogueApproval(1);
                                }
                                else {
                                    module.exports.setCatalogueApproval(0);
                                }
                            });
                        }
                        
                        // If 'products_id' in catalogue is NULL
                            else if(catalogueCheckResult[0].products_id == null) {
                                var updateProductsQuery = "UPDATE merchant_catalogue SET products_id = '"+product_id+"' WHERE merchant_id = "+merchant_id+"";
                                connection.query(updateProductsQuery, function(updateProductsErr, updateProductsResult) {
                                    if(updateProductsErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : updateProductsErr
                                        });
                                    }
                                    else if(updateProductsResult.affectedRows != 0) {
                                        module.exports.setCatalogueApproval(1);
                                    }
                                    else {
                                        module.exports.setCatalogueApproval(0);
                                    }
                                });        
                            }        
                    } 
                }
            });

            // Check if Catalogue contains both product and its corresponding category
            setTimeout(function() {
                var catalogueClearance = module.exports.setCatalogueApproval('get');

                //If catalogue is perfect
                if(catalogueClearance == 1) {

                    var getInventoryQuery = "SELECT * FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
                    connection.query(getInventoryQuery, function(getInventoryErr, getInventoryResult) {
                        if(getInventoryErr) {
                            res.status(200).send({
                                error : true,
                                message : getInventoryErr
                            });
                        }

                        // If query is successful
                        else if(getInventoryResult.length != 0) {

                            //Split Products in inventory
                            if(getInventoryResult[0].products_id == null) {
                                inventoryProducts = [];
                            }
                            else if(getInventoryResult[0].products_id.indexOf(',') != -1) {
                                inventoryProducts = getInventoryResult[0].products_id.split(',');
                            }
                            else if((getInventoryResult[0].products_id.indexOf(',') == -1) && (getInventoryResult[0].products_id != null)) {
                                inventoryProducts.push(getInventoryResult[0].products_id);
                            }
                            // If 'products_id' in merchant inventory is Not Null
                            if(getInventoryResult[0].products_id != null) {

                                // Check whether the product is already exists in inventory
                                // What if product not exist in inventory
                                if(inventoryProducts.indexOf(product_id) == -1) {

                                    // Update all existing strings with new product details
                                    var updatedProductsId = getInventoryResult[0].products_id + ","+product_id+"";
                                    var updatedCategoriesId = getInventoryResult[0].categories_id + ","+category_id+"";
                                    var updatedSKU = getInventoryResult[0].sku + ","+sku+"";
                                    var updatedAvailableForSale = getInventoryResult[0].available_for_sale + ","+available_for_sale+"";
                                    var updatedQuantification = getInventoryResult[0].quantification + ","+quantification+"";
                                    var updatedDefaultPrice = getInventoryResult[0].default_price + ","+default_price+"";
                                    var updatedSellingPrice = getInventoryResult[0].selling_price + ","+selling_price+"";
                                    var updatedPYGPrice = getInventoryResult[0].pyg_price + ","+pyg_price+"";
                                    var updatedSeasonal = getInventoryResult[0].seasonal + ","+seasonal+"";
                                    var updatedAvailableQty = getInventoryResult[0].available_qty + ","+available_qty+"";
                                    var updatedLowStock = getInventoryResult[0].low_stock + ","+low_stock+"";
                                    // Add to Inventory (Core Part of this API)
                                    var updateInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+updatedProductsId+"', categories_id = '"+updatedCategoriesId+"', sku = '"+updatedSKU+"', available_for_sale = '"+updatedAvailableForSale+"', quantification = '"+updatedQuantification+"', default_price = '"+updatedDefaultPrice+"', selling_price = '"+updatedSellingPrice+"', pyg_price = '"+updatedPYGPrice+"', seasonal = '"+updatedSeasonal+"', available_qty = '"+updatedAvailableQty+"', low_stock = '"+updatedLowStock+"', last_updated = CURRENT_TIMESTAMP WHERE merchant_id = "+merchant_id+"";
                                    connection.query(updateInventoryQuery, function(updateInventoryErr, updateInventoryResult) {
                                        if(updateInventoryErr) {
                                            res.status(200).send({
                                                error : true,
                                                message : updateInventoryErr
                                            });
                                        }
                                        else if(updateInventoryResult.affectedRows != 0) {
                                            module.exports.setProductAdded(1);
                                        }
                                        else {
                                            module.exports.setProductAdded(0);
                                        }
                                    });
                                }

                                // What if product exists in inventory
                                else if(inventoryProducts.indexOf(product_id) != -1) {
                                    module.exports.setProductAdded(0);
                                }
                            }

                            // If 'products_id' in merchant_inventory is NULL
                            else if(getInventoryResult[0].products_id == null) {

                                // Add to Inventory (Core Part of this API)
                                var updateInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+product_id+"', categories_id = '"+category_id+"', sku = '"+sku+"', available_for_sale = '"+available_for_sale+"', quantification = '"+quantification+"', default_price = '"+default_price+"', selling_price = '"+selling_price+"', pyg_price = '"+pyg_price+"', seasonal = '"+seasonal+"', available_qty = '"+available_qty+"', low_stock = '"+low_stock+"', last_updated = CURRENT_TIMESTAMP WHERE merchant_id = "+merchant_id+"";
                                connection.query(updateInventoryQuery, function(updateInventoryErr, updateInventoryResult) {
                                    if(updateInventoryErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : updateInventoryErr
                                        });
                                    }
                                    else if(updateInventoryResult.affectedRows != 0) {
                                        module.exports.setProductAdded(1);
                                    }
                                    else {
                                        module.exports.setProductAdded(0);
                                    }
                                });
                            }
                        }
                    });
                }

                //If catalogue operations are not cleared
                else if(catalogueClearance == 0) {
                    module.exports.setProductAdded(0);
                }
            }, 2000);
            setTimeout(function() {
                var productClearance = module.exports.setProductAdded('get');
                if(productClearance == 1) {
                    res.status(200).send({
                        error : false,
                        status : "Product Added Successfully",
                        merchant_id : merchant_id,
                        product : {
                            product_id : product_id,
                            category_id : category_id,
                            sku : sku,
                            available_for_sale : available_for_sale,
                            quantification : quantification,
                            default_price : default_price,
                            selling_price : selling_price,
                            pyg_price : pyg_price,
                            seasonal : seasonal,
                            available_qty : available_qty,
                            low_stock : low_stock
                        }
                    });
                }
                else if(productClearance == 0) {
                    res.status(200).send({
                        error : true,
                        message : "Product already exist"
                    });
                }
            }, 2500);
        } else {
            res.status(200).send({
                error : true,
                message : "Product doesn't exist"
            });
        }
    });
});

// Add inventory to Merchant old api

router.post('/addInventoryToMerchantOld', function(req, res) {

    //Required Params
    var merchant_id = req.body.merchant_id;
    var category_id = req.body.category_id;
    var product_id = req.body.product_id;
    var sku = req.body.sku;
    var available_for_sale = req.body.available_for_sale;
    var quantification = req.body.quantification;
    var default_price = req.body.default_price;
    var selling_price = req.body.selling_price;
    var pyg_price = req.body.pyg_price;
    var seasonal = req.body.seasonal;
    var available_qty = req.body.available_qty;
    var low_stock = req.body.low_stock

    var catalogueCategories = new Array();
    var catalogueProducts = new Array();
    var inventoryProducts = new Array();

    // Check merchant's catalogue whether category and product is available or not
    var catalogueCheckQuery = "SELECT * FROM merchant_catalogue WHERE merchant_id = "+merchant_id+"";
    connection.query(catalogueCheckQuery, function(catalogueCheckErr, catalogueCheckResult) {
        if(catalogueCheckErr) {
            res.status(200).send({
                error : true,
                message : catalogueCheckErr
            });
        }
        else if(catalogueCheckResult.length != 0) {
            //Split Categories
            if((catalogueCheckResult[0].categories_id != null) && (catalogueCheckResult[0].categories_id.indexOf(',') != -1)) {
                catalogueCategories = catalogueCheckResult[0].categories_id.split(',');
            }
            else if((catalogueCheckResult[0].categories_id != null)&&(catalogueCheckResult[0].categories_id.indexOf(',') == -1)) {
                catalogueCategories.push(catalogueCheckResult[0].categories_id);
            }
            //Split Products
            if((catalogueCheckResult[0].products_id != null) && (catalogueCheckResult[0].products_id.indexOf(',') != -1)) {
                catalogueProducts = catalogueCheckResult[0].products_id.split(',');
            }
            else if((catalogueCheckResult[0].products_id != null) && (catalogueCheckResult[0].products_id.indexOf(',') == -1)) {
                catalogueProducts.push(catalogueCheckResult[0].products_id);
            }

            // Check Category is exist in catalogue or not

            // What if both categories and products exists in catalogue
            if((catalogueCategories.indexOf(category_id) != -1) && (catalogueProducts.indexOf(product_id) != -1)) {
                module.exports.setCatalogueApproval(1);
            }

            // What if category exists and product not exists in catalogue
            else if((catalogueCategories.indexOf(category_id) != -1) && (catalogueProducts.indexOf(product_id) == -1)) {
                // Add Product to catalogue
                // If 'products_id' in catalogue is NotNull
                if(catalogueCheckResult[0].products_id != null) {
                    var updatedProductsInCatalogue = catalogueCheckResult[0].products_id + ","+product_id+"";
                    var updateProductsQuery = "UPDATE merchant_catalogue SET products_id = '"+updatedProductsInCatalogue+"' WHERE merchant_id = "+merchant_id+"";
                    connection.query(updateProductsQuery, function(updateProductsErr, updateProductsResult) {
                        if(updateProductsErr) {
                            res.status(200).send({
                                error : true,
                                message : updateProductsErr
                            });
                        }
                        else if(updateProductsResult.affectedRows != 0) {
                            module.exports.setCatalogueApproval(1);
                        }
                        else {
                            module.exports.setCatalogueApproval(0);
                        }
                    });
                }

                // If 'products_id' in catalogue is NULL
                    else if(catalogueCheckResult[0].products_id == null) {
                        var updateProductsQuery = "UPDATE merchant_catalogue SET products_id = '"+product_id+"' WHERE merchant_id = "+merchant_id+"";
                        connection.query(updateProductsQuery, function(updateProductsErr, updateProductsResult) {
                            if(updateProductsErr) {
                                res.status(200).send({
                                    error : true,
                                    message : updateProductsErr
                                });
                            }
                            else if(updateProductsResult.affectedRows != 0) {
                                module.exports.setCatalogueApproval(1);
                            }
                            else {
                                module.exports.setCatalogueApproval(0);
                            }
                        });        
                    }
            }

            // What if product exists and category not exists in catalogue (Very rare case)
            else if((catalogueCategories.indexOf(category_id) == -1) && (catalogueProducts.indexOf(product_id) != -1)) {
                
                // If 'categories_id' in catalogue is NotNull
                if(catalogueCheckResult[0].categories_id != null) {
                    var updatedCategoriesInCatalogue = catalogueCheckResult[0].categories_id + ","+category_id+"";
                    var updateCategoriesQuery = "UPDATE merchant_catalogue SET categories_id = '"+updatedCategoriesInCatalogue+"' WHERE merchant_id = "+merchant_id+"";
                    connection.query(updateCategoriesQuery, function(updateCategoriesErr, updateCategoriesResult) {
                        if(updateCategoriesErr) {
                            res.status(200).send({
                                error : true,
                                message : updateCategoriesErr
                            });
                        }
                        else if(updateCategoriesResult.affectedRows != 0) {
                            module.exports.setCatalogueApproval(1);
                        }
                        else {
                            module.exports.setCatalogueApproval(0);
                        }        
                    });
                }

                // If 'categories_id' in catalogue is NULL
                    else if(catalogueCheckResult[0].categories_id == null) {
                        var updateCategoriesQuery = "UPDATE merchant_catalogue SET categories_id = '"+category_id+"' WHERE merchant_id = "+merchant_id+"";
                        connection.query(updateCategoriesQuery, function(updateCategoriesErr, updateCategoriesResult) {
                            if(updateCategoriesErr) {
                                res.status(200).send({
                                    error : true,
                                    message : updateCategoriesErr
                                });
                            }
                            else if(updateCategoriesResult.affectedRows != 0) {
                                module.exports.setCatalogueApproval(1);
                            }
                            else {
                                module.exports.setCatalogueApproval(0);
                            }        
                        });
                    }
            }

            // What if both 'categories_id' and 'products_id' not exists in catalogue
            else if((catalogueCategories.indexOf(category_id) == -1) && (catalogueProducts.indexOf(product_id) == -1)) {
                
                // If 'categories_id' is NotNull
                if(catalogueCheckResult[0].categories_id != null) {
                    var updatedCategoriesInCatalogue = catalogueCheckResult[0].categories_id + ","+category_id+"";
                    var updateCategoriesQuery = "UPDATE merchant_catalogue SET categories_id = '"+updatedCategoriesInCatalogue+"' WHERE merchant_id = "+merchant_id+"";
                    connection.query(updateCategoriesQuery, function(updateCategoriesErr, updateCategoriesResult) {
                        if(updateCategoriesErr) {
                            res.status(200).send({
                                error : true,
                                message : updateCategoriesErr
                            });
                        }
                        else if(updateCategoriesResult.affectedRows != 0) {
                            module.exports.setCatalogueApproval(1);
                        }
                        else {
                            module.exports.setCatalogueApproval(0);
                        }        
                    });
                }

                // If 'categories_id' in catalogue is NULL
                    else if(catalogueCheckResult[0].categories_id == null) {
                        var updateCategoriesQuery = "UPDATE merchant_catalogue SET categories_id = '"+category_id+"' WHERE merchant_id = "+merchant_id+"";
                        connection.query(updateCategoriesQuery, function(updateCategoriesErr, updateCategoriesResult) {
                            if(updateCategoriesErr) {
                                res.status(200).send({
                                    error : true,
                                    message : updateCategoriesErr
                                });
                            }
                            else if(updateCategoriesResult.affectedRows != 0) {
                                module.exports.setCatalogueApproval(1);
                            }
                            else {
                                module.exports.setCatalogueApproval(0);
                            }        
                        });
                    }

                // If 'products_id' in catalogue is NotNull
                if(catalogueCheckResult[0].products_id != null) {
                    var updatedProductsInCatalogue = catalogueCheckResult[0].products_id + ","+product_id+"";
                    var updateProductsQuery = "UPDATE merchant_catalogue SET products_id = '"+updatedProductsInCatalogue+"' WHERE merchant_id = "+merchant_id+"";
                    connection.query(updateProductsQuery, function(updateProductsErr, updateProductsResult) {
                        if(updateProductsErr) {
                            res.status(200).send({
                                error : true,
                                message : updateProductsErr
                            });
                        }
                        else if(updateProductsResult.affectedRows != 0) {
                            module.exports.setCatalogueApproval(1);
                        }
                        else {
                            module.exports.setCatalogueApproval(0);
                        }
                    });
                }
                
                // If 'products_id' in catalogue is NULL
                    else if(catalogueCheckResult[0].products_id == null) {
                        var updateProductsQuery = "UPDATE merchant_catalogue SET products_id = '"+product_id+"' WHERE merchant_id = "+merchant_id+"";
                        connection.query(updateProductsQuery, function(updateProductsErr, updateProductsResult) {
                            if(updateProductsErr) {
                                res.status(200).send({
                                    error : true,
                                    message : updateProductsErr
                                });
                            }
                            else if(updateProductsResult.affectedRows != 0) {
                                module.exports.setCatalogueApproval(1);
                            }
                            else {
                                module.exports.setCatalogueApproval(0);
                            }
                        });        
                    }        
            } 
         }
    });

    // Check if Catalogue contains both product and its corresponding category
    setTimeout(function() {
        var catalogueClearance = module.exports.setCatalogueApproval('get');

        //If catalogue is perfect
        if(catalogueClearance == 1) {

            var getInventoryQuery = "SELECT * FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
            connection.query(getInventoryQuery, function(getInventoryErr, getInventoryResult) {
                if(getInventoryErr) {
                    res.status(200).send({
                        error : true,
                        message : getInventoryErr
                    });
                }

                // If query is successful
                else if(getInventoryResult.length != 0) {

                    //Split Products in inventory
                    if(getInventoryResult[0].products_id == null) {
                        inventoryProducts = [];
                    }
                    else if(getInventoryResult[0].products_id.indexOf(',') != -1) {
                        inventoryProducts = getInventoryResult[0].products_id.split(',');
                    }
                    else if((getInventoryResult[0].products_id.indexOf(',') == -1) && (getInventoryResult[0].products_id != null)) {
                        inventoryProducts.push(getInventoryResult[0].products_id);
                    }

                    // If 'products_id' in merchant inventory is Not Null
                    if(getInventoryResult[0].products_id != null) {

                        // Check whether the product is already exists in inventory
                        // What if product not exist in inventory
                        if(inventoryProducts.indexOf(product_id) == -1) {

                            // Update all existing strings with new product details
                            var updatedProductsId = getInventoryResult[0].products_id + ","+product_id+"";
                            var updatedCategoriesId = getInventoryResult[0].categories_id + ","+category_id+"";
                            var updatedSKU = getInventoryResult[0].sku + ","+sku+"";
                            var updatedAvailableForSale = getInventoryResult[0].available_for_sale + ","+available_for_sale+"";
                            var updatedQuantification = getInventoryResult[0].quantification + ","+quantification+"";
                            var updatedDefaultPrice = getInventoryResult[0].default_price + ","+default_price+"";
                            var updatedSellingPrice = getInventoryResult[0].selling_price + ","+selling_price+"";
                            var updatedPYGPrice = getInventoryResult[0].pyg_price + ","+pyg_price+"";
                            var updatedSeasonal = getInventoryResult[0].seasonal + ","+seasonal+"";
                            var updatedAvailableQty = getInventoryResult[0].available_qty + ","+available_qty+"";
                            var updatedLowStock = getInventoryResult[0].low_stock + ","+low_stock+"";

                            // Add to Inventory (Core Part of this API)
                            var updateInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+updatedProductsId+"', categories_id = '"+updatedCategoriesId+"', sku = '"+updatedSKU+"', available_for_sale = '"+updatedAvailableForSale+"', quantification = '"+updatedQuantification+"', default_price = '"+updatedDefaultPrice+"', selling_price = '"+updatedSellingPrice+"', pyg_price = '"+updatedPYGPrice+"', seasonal = '"+updatedSeasonal+"', available_qty = '"+updatedAvailableQty+"', low_stock = '"+updatedLowStock+"', last_updated = CURRENT_TIMESTAMP WHERE merchant_id = "+merchant_id+"";
                            connection.query(updateInventoryQuery, function(updateInventoryErr, updateInventoryResult) {
                                if(updateInventoryErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : updateInventoryErr
                                    });
                                }
                                else if(updateInventoryResult.affectedRows != 0) {
                                    module.exports.setProductAdded(1);
                                }
                                else {
                                    module.exports.setProductAdded(0);
                                }
                            });
                        }

                        // What if product exists in inventory
                        else if(inventoryProducts.indexOf(product_id) != -1) {
                            module.exports.setProductAdded(0);
                        }
                    }

                    // If 'products_id' in merchant_inventory is NULL
                    else if(getInventoryResult[0].products_id == null) {

                        // Add to Inventory (Core Part of this API)
                        var updateInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+product_id+"', categories_id = '"+category_id+"', sku = '"+sku+"', available_for_sale = '"+available_for_sale+"', quantification = '"+quantification+"', default_price = '"+default_price+"', selling_price = '"+selling_price+"', pyg_price = '"+pyg_price+"', seasonal = '"+seasonal+"', available_qty = '"+available_qty+"', low_stock = '"+low_stock+"', last_updated = CURRENT_TIMESTAMP WHERE merchant_id = "+merchant_id+"";
                        connection.query(updateInventoryQuery, function(updateInventoryErr, updateInventoryResult) {
                            if(updateInventoryErr) {
                                res.status(200).send({
                                    error : true,
                                    message : updateInventoryErr
                                });
                            }
                            else if(updateInventoryResult.affectedRows != 0) {
                                module.exports.setProductAdded(1);
                            }
                            else {
                                module.exports.setProductAdded(0);
                            }
                        });
                    }
                }
            });
        }

        //If catalogue operations are not cleared
        else if(catalogueClearance == 0) {
            module.exports.setProductAdded(0);
        }
    }, 2000);

    setTimeout(function() {
        var productClearance = module.exports.setProductAdded('get');
        if(productClearance == 1) {
            res.status(200).send({
                error : false,
                status : "Product Added Successfully",
                merchant_id : merchant_id,
                product : {
                    product_id : product_id,
                    category_id : category_id,
                    sku : sku,
                    available_for_sale : available_for_sale,
                    quantification : quantification,
                    default_price : default_price,
                    selling_price : selling_price,
                    pyg_price : pyg_price,
                    seasonal : seasonal,
                    available_qty : available_qty,
                    low_stock : low_stock
                }
            });
        }
        else if(productClearance == 0) {
            res.status(200).send({
                error : true,
                message : "Problem in adding product"
            });
        }
    }, 2500);
	
});



// Update a product in merchant's inventory

router.post('/UpdateMerchantInventory', function(req, res) {

    //Required Params
    var merchant_id = req.body.merchant_id;
    var product_id = req.body.product_id;
    var category_id = req.body.category_id;
    var sku = req.body.sku;
    var available_for_sale = req.body.available_for_sale;
    var quantification = req.body.quantification;
    var default_price = req.body.default_price;
    var selling_price = req.body.selling_price;
    var pyg_price = req.body.pyg_price;
    var seasonal = req.body.seasonal;
    var added_qty = req.body.added_qty;
    var low_stock = req.body.low_stock;

    // Select a product from merchant's inventory
    var selectInventoryQuery = "SELECT * FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
    connection.query(selectInventoryQuery, function(selectInventoryErr, selectInventoryResult) {
        if(selectInventoryErr) {
            res.status(200).send({
                error : true,
                message : selectInventoryErr,
                //check : 1
            });
        }
        else if(selectInventoryResult.length != 0) {
            
            // To find whether the produc is in inventory or not

            // If products in inventory is null

            if(selectInventoryResult[0].products_id == null || selectInventoryResult[0].products_id == '') {
                res.status(200).send({
                    error : true,
                    message : "No Products in Inventory",
                    //check : 2
                });
            }

            // If not null split inventory products into array

            // If products in inventory contains only 1 item
            
            else if(selectInventoryResult[0].products_id.indexOf(',') == -1) {

                // If that one item is not the required product

                if(selectInventoryResult[0].products_id != product_id) {
                    res.status(200).send({
                        error : true,
                        message : "Product in Inventory is not matched with the input",
                        //check : 3
                    });
                }

                // If that 1 item is equal to required product

                else if(selectInventoryResult[0].products_id == product_id) {

                    // Update that product in inventory

                    var availableQtyInt = parseInt(selectInventoryResult[0].available_qty, 10);
                    var addedQtyInt = parseInt(added_qty, 10);
                    var updatedQty = availableQtyInt + addedQtyInt;
                    var singleProductInInventory_UpdateQuery = "UPDATE merchant_inventory SET products_id = '"+product_id+"', categories_id = '"+category_id+"', sku = '"+sku+"', available_for_sale = '"+available_for_sale+"', quantification = '"+quantification+"', default_price = '"+default_price+"', selling_price = '"+selling_price+"', pyg_price = '"+pyg_price+"', seasonal = '"+seasonal+"', available_qty = '"+updatedQty+"', low_stock = '"+low_stock+"', last_updated = CURRENT_TIMESTAMP WHERE merchant_id = "+merchant_id+"";
                    connection.query(singleProductInInventory_UpdateQuery, function(singleProductInInventory_UpdateErr, singleProductInInventory_UpdateResult) {
                        if(singleProductInInventory_UpdateErr) {
                            res.status(200).send({
                                error : true,
                                message : singleProductInInventory_UpdateErr,
                                //check : 4
                            });
                        }
                        else if(singleProductInInventory_UpdateResult.affectedRows != 0) {
                            res.status(201).send({
                                error : false,
                                status : "Inventory Modified",
                                product_id : product_id,
                                available_qty : updatedQty,
                                low_stock : low_stock,
                                //check : 5
                            });
                        }
                    });
                }
            }

            // If products in inventory is more than one item

            else if(selectInventoryResult[0].products_id.indexOf(',') != -1) {

                //Split that multiple products and its other entities into array
                var splitProductsInInventory = selectInventoryResult[0].products_id.split(',');
                var splitCategoriesInInventory = selectInventoryResult[0].categories_id.split(',');
                var splitSKUInInventory = selectInventoryResult[0].sku.split(',');
                var splitAvailableForSaleInInventory = selectInventoryResult[0].available_for_sale.split(',');
                var splitQuantificationInInventory = selectInventoryResult[0].quantification.split(',');
                var splitDefaultPriceInInventory = selectInventoryResult[0].default_price.split(',');
                var splitSellingPriceInInventory = selectInventoryResult[0].selling_price.split(',');
                var splitPYGPriceInInventory = selectInventoryResult[0].pyg_price.split(',');
                var splitSeasonalInInventory = selectInventoryResult[0].seasonal.split(',');
                var splitAvailableQtyInInventory = selectInventoryResult[0].available_qty.split(',');
                var splitLowStockInInventory = selectInventoryResult[0].low_stock.split(',');
                
                // Check the input product is in inventory

                // If input product is not in inventory

                if(splitProductsInInventory.indexOf(product_id) == -1) {
                    res.status(200).send({
                        error : true,
                        message : "Product input is not in inventory",
                        //check : 6
                    });
                }

                // If input product is in inventory then find it's index and carry rest of updating operations

                else if(splitProductsInInventory.indexOf(product_id) != -1) {

                    // Find index of that product in inventory

                    var indexOfInputProductInInventory = splitProductsInInventory.indexOf(product_id);

                    // Increment the available_qty with added_qty

                    // For that convert that particular available_qty to Int
                    var availableQty = splitAvailableQtyInInventory[indexOfInputProductInInventory];
                    var availableQtyInt = parseInt(availableQty, 10);
                    var addedQtyInt = parseInt(added_qty, 10);
                    var updatedQty = availableQtyInt + addedQtyInt;

                    // Add that as a string 
                    splitAvailableQtyInInventory[indexOfInputProductInInventory] = ""+updatedQty+"";

                    // Other than available_qty update other entities with newly entered input params
                    splitCategoriesInInventory[indexOfInputProductInInventory] = category_id;
                    splitSKUInInventory[indexOfInputProductInInventory] = sku;
                    splitAvailableForSaleInInventory[indexOfInputProductInInventory] = available_for_sale;
                    splitQuantificationInInventory[indexOfInputProductInInventory] = quantification;
                    splitDefaultPriceInInventory[indexOfInputProductInInventory] = default_price;
                    splitSellingPriceInInventory[indexOfInputProductInInventory] = selling_price;
                    splitPYGPriceInInventory[indexOfInputProductInInventory] = pyg_price;
                    splitSeasonalInInventory[indexOfInputProductInInventory] = seasonal;
                    splitLowStockInInventory[indexOfInputProductInInventory] = low_stock;

                    // Merge all splitted entities into single string
                    var updatedProductsInInventory = splitProductsInInventory.join(',');
                    var updatedCategoriesInInventory = splitCategoriesInInventory.join(',');
                    var updatedSKUInInventory = splitSKUInInventory.join(',');
                    var updatedAvailableForSaleInInventory = splitAvailableForSaleInInventory.join(',');
                    var updatedQuantificationInInventory = splitQuantificationInInventory.join(',');
                    var updatedDefaultPriceInInventory = splitDefaultPriceInInventory.join(',');
                    var updatedSellingPriceInInventory = splitSellingPriceInInventory.join(',');
                    var updatedPYGPriceInInventory = splitPYGPriceInInventory.join(',');
                    var updatedSeasonalInInventory = splitSeasonalInInventory.join(',');
                    var updatedAvailableQtyInInventory = splitAvailableQtyInInventory.join(',');
                    var updatedLowStockInInventory = splitLowStockInInventory.join(',');

                    // Update into Database 

                    var multipleProductInInventory_UpdateQuery = "UPDATE merchant_inventory SET products_id = '"+updatedProductsInInventory+"', categories_id = '"+updatedCategoriesInInventory+"', sku = '"+updatedSKUInInventory+"', available_for_sale = '"+updatedAvailableForSaleInInventory+"', quantification = '"+updatedQuantificationInInventory+"', default_price = '"+updatedDefaultPriceInInventory+"', selling_price = '"+updatedSellingPriceInInventory+"', pyg_price = '"+updatedPYGPriceInInventory+"', seasonal = '"+updatedSeasonalInInventory+"', available_qty = '"+updatedAvailableQtyInInventory+"', low_stock = '"+updatedLowStockInInventory+"', last_updated = CURRENT_TIMESTAMP WHERE merchant_id = "+merchant_id+"";
                    connection.query(multipleProductInInventory_UpdateQuery, function(multipleProductInInventory_UpdateErr, multipleProductInInventory_UpdateResult) {
                        if(multipleProductInInventory_UpdateErr) {
                            res.status(200).send({
                                error : true,
                                message : multipleProductInInventory_UpdateErr,
                                //check : 7
                            });
                        }
                        else if(multipleProductInInventory_UpdateResult.affectedRows != 0) {
                            res.status(201).send({
                                error : false,
                                status : "Inventory Modified",
                                product_id : product_id,
                                available_qty : updatedQty,
                                low_stock : low_stock,
                                //check : 8
                            });
                        }
                    });
                }
            }
        }

        else {
            res.status(200).send({
                error : true,
                message : "Inventory not found",
                //check : 9
            });
        }
    });

});




// Add a product to master PYG Inventory

router.post('/addProductToMasterInventory', function(req, res) {

    //Required Params
    var product_name = req.body.product_name;
    var product_img = req.body.product_img;
    var category_id = req.body.category_id;
    var product_description = req.body.product_description;
    var sku = req.body.sku;
    var quantification = req.body.quantification;
    var merchant_id = req.body.merchant_id;                                   // 0 - If added by admin                           

    var repititionQuery = "SELECT product_name FROM products WHERE product_name LIKE '"+product_name+"'";
    connection.query(repititionQuery, function(repititionErr, repititionResult) {
        if(repititionErr) {
            res.status(200).send({
                error : true,
                message : repititionErr
            });
        }
        else if(repititionResult.length != 0) {
            res.status(200).send({
                error : true,
                message : 'Product already Exists'
            });
        }
        else {
            var search_query = "SELECT category_name FROM categories WHERE category_id = "+category_id+"";
            connection.query(search_query, function(searchErr, searchResult) {
                if(searchErr) {
                    res.status(200).send({
                        error : true,
                        message : searchErr
                    });
                }
                else if(merchant_id != 0) {
                    var searchMerchantQuery = "SELECT merchant_name FROM merchants WHERE merchant_id = "+merchant_id+"";
                    connection.query(searchMerchantQuery, function(searchMerchantErr, searchMerchantResult){
                        if(searchMerchantErr) {
                            res.status(200).send({
                                error : true,
                                message : searchMerchantErr
                            });
                        }
                        else {
                            var insert_query = "INSERT INTO products(product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('"+product_name+"', '"+product_img+"' , "+category_id+" , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , "+merchant_id+")";
                            connection.query(insert_query, function(insertErr, insertResult) {
                                if(insertErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : insertErr
                                    });
                                }
                                else if(insertResult.affectedRows != 0) {
                                    res.status(201).send({
                                        error : false,
                                        message : 'Inserted',
                                        product_id : insertResult.insertId,
                                        product_name : product_name,
                                        product_img : product_img,
                                        category_id : category_id,
                                        category_name : searchResult[0].category_name,
                                        product_description : product_description,
                                        sku : sku,
                                        quantification : quantification,
                                        added_by : {
                                            id : merchant_id,
                                            name : searchMerchantResult[0].merchant_name
                                        },
                                    });
                                }
                            });
                        }
                    });
                }
                else {
                    var insert_query = "INSERT INTO products(product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('"+product_name+"', '"+product_img+"' , "+category_id+" , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , "+merchant_id+")";
                    connection.query(insert_query, function(insertErr, insertResult) {
                        if(insertErr) {
                            res.status(200).send({
                                error : true,
                                message : insertErr
                            });
                        }
                        else if(insertResult.affectedRows != 0) {
                            res.status(201).send({
                                error : false,
                                message : 'Inserted',
                                product_id : insertResult.insertId,
                                product_name : product_name,
                                product_img : product_img,
                                category_id : category_id,
                                category_name : searchResult[0].category_name,
                                product_description : product_description,
                                sku : sku,
                                quantification : quantification,
                                added_by : {
                                    id : merchant_id,
                                    name : "Admin"
                                },
                            });
                        }
                    });
                }       
            });
        }
    });

});
  
  
// Add a product to master PYG Inventory Revised

router.post('/addProductToMasterInventoryRevised', function(req, res) {

    //Required Params
    var productName = req.body.productName;
    var product_img = req.body.product_img;
    var category_id = req.body.category_id;
    var product_description = req.body.product_description;
    var sku = req.body.sku;
    var quantification = req.body.quantification;
    var merchant_id = req.body.merchant_id;                                   // 0 - If added by admin                           

    
    var query = "SELECT products_id,categories_id,merchant_name FROM merchant_inventory,merchants WHERE merchant_inventory.merchant_id = merchants.merchant_id AND merchant_inventory.merchant_id = "+merchant_id+"";
	connection.query(query, function(err, result) {
		if(err) {
			res.status(200).send({
				error : true,
				message : err
			});
		}
		else if(result[0].products_id != null && result[0].products_id != '') {
			
            var productsArr = result[0].products_id.split(',');
            var categoriesArr = result[0].categories_id.split(',');

            var categoriesIndex = new Array();
			
			var strCategoryId = category_id.toString();
			for(var i = 0; i < categoriesArr.length; i++) {
				if(strCategoryId == categoriesArr[i]) {
					categoriesIndex.push(i);
				}
            }
            
			categoriesIndex.forEach(function(k) {
                
                var productIdInteger = parseInt(productsArr[k],10);

				var productNameQuery = "SELECT products.product_name as ProdName, products.category_id, categories.category_name FROM products LEFT JOIN categories ON products.category_id = categories.category_id WHERE products.product_id = "+productIdInteger+"";
				connection.query(productNameQuery, function(productNameErr, productNameResult) {
					if(productNameErr) {
						res.status(200).send({
							error : true,
							message : productNameErr
						});
                    }
                    else 
                        if(productNameResult.length!=null){
                        for(var i=0; i<productNameResult.length ;i++){
                            if(productNameResult[0].ProdName==productName){
                                res.status(200).send({
                                    error : true,
                                    message : 'Product already Exists'
                                });
                            }
                            else if(merchant_id!=0){
                                var insert_query = "INSERT INTO products(product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('"+productName+"', '"+product_img+"' , "+category_id+" , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , "+merchant_id+")";
                                connection.query(insert_query, function(insertErr, insertResult) {
                                    if(insertErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : insertErr
                                        });
                                    }
                                    else if(insertResult.affectedRows != 0) {
                                        res.status(201).send({
                                            error : false,
                                            message : 'Inserted',
                                            product_id : insertResult.insertId,
                                            product_name : productName,
                                            product_img : product_img,
                                            category_id : category_id,
                                            // category_name : productNameResult[0].category_name,
                                            product_description : product_description,
                                            sku : sku,
                                            quantification : quantification,
                                            added_by : {
                                                id : merchant_id,
                                                name : result[0].merchant_name
                                            },
                                        });
                                    }
                                });
                            }
                            else {
                                var insert_query = "INSERT INTO products(product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('"+productName+"', '"+product_img+"' , "+category_id+" , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , "+merchant_id+")";
                                connection.query(insert_query, function(insertErr, insertResult) {
                                    if(insertErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : insertErr
                                        });
                                    }
                                    else if(insertResult.affectedRows != 0) {
                                        res.status(201).send({
                                            error : false,
                                            message : 'Inserted',
                                            product_id : insertResult.insertId,
                                            product_name : productName,
                                            product_img : product_img,
                                            category_id : category_id,
                                            // category_name : productNameResult[0].category_name,
                                            product_description : product_description,
                                            sku : sku,
                                            quantification : quantification,
                                            added_by : {
                                                id : merchant_id,
                                                name : "Admin"
                                            },
                                        });
                                    }
                                });
                            }       
                        
                        }
                    }
                });
    
            });
        }
        else if(merchant_id!=0){
            var insert_query = "INSERT INTO products(product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('"+productName+"', '"+product_img+"' , "+category_id+" , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , "+merchant_id+")";
            connection.query(insert_query, function(insertErr, insertResult) {
                if(insertErr) {
                    res.status(200).send({
                        error : true,
                        message : insertErr
                    });
                }
                else if(insertResult.affectedRows != 0) {
                    res.status(201).send({
                        error : false,
                        message : 'Inserted',
                        product_id : insertResult.insertId,
                        product_name : productName,
                        product_img : product_img,
                        category_id : category_id,
                        // category_name : productNameResult[0].category_name,
                        product_description : product_description,
                        sku : sku,
                        quantification : quantification,
                        added_by : {
                            id : merchant_id,
                            name : result[0].merchant_name
                        },
                    });
                }
            });
        }
        else {
            var insert_query = "INSERT INTO products(product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('"+productName+"', '"+product_img+"' , "+category_id+" , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , "+merchant_id+")";
            connection.query(insert_query, function(insertErr, insertResult) {
                if(insertErr) {
                    res.status(200).send({
                        error : true,
                        message : insertErr
                    });
                }
                else if(insertResult.affectedRows != 0) {
                    res.status(201).send({
                        error : false,
                        message : 'Inserted',
                        product_id : insertResult.insertId,
                        product_name : productName,
                        product_img : product_img,
                        category_id : category_id,
                        // category_name : productNameResult[0].category_name,
                        product_description : product_description,
                        sku : sku,
                        quantification : quantification,
                        added_by : {
                            id : merchant_id,
                            name : "Admin"
                        },
                    });
                }
            });
        }        
    });
});


// Merchant reviewing Order(order approved/not approved)

router.post('/reviewOrder', function(req, res) {

    var merchant_id = req.body.merchant_id;
    var order_id = req.body.order_id;
    var user_id = req.body.user_id;
    var approved = req.body.approved;
    var order_status_id = req.body.order_status_id;
    var collecting_time = req.body.collecting_time;
    var mailFlag = 0;
    var merchant_email;
    var user_email;

    // Get merchant Email through order
    var merchantEmailQuery = "SELECT orders.merchant_id,merchants.merchant_email FROM orders LEFT JOIN merchants ON orders.merchant_id = merchants.merchant_id WHERE orders.order_id = "+order_id+"";
    connection.query(merchantEmailQuery, function(merchantEmailErr, merchantEmailResult) {
        if(!merchantEmailErr) {
            setMerchantEmail(merchantEmailResult[0].merchant_email);
        }
    });

    // Get user Email through order
    var UserEmailQuery = "SELECT orders.user_id,users.email as user_email FROM orders LEFT JOIN users ON orders.user_id = users.user_id WHERE orders.order_id = "+order_id+"";
    connection.query(UserEmailQuery, function(userEmailErr, userEmailResult) {
        if(!userEmailErr) {
            setUserEmail(userEmailResult[0].user_email);
        }
    });
    
    // If merchant not approved the order
    if(approved == 0) {
        
        var orderedDetailsQuery = "SELECT products_id,qty FROM orders WHERE order_id = "+order_id+"";
        connection.query(orderedDetailsQuery, function(orderedDetailsErr, orderedDetailsResult) {
            if(orderedDetailsErr) {
                res.status(200).send({
                    error : true,
                    message : orderedDetailsErr,
                    //check : 1
                });
            }
    
            //If ordered items are more than 1
    
            else if((orderedDetailsResult[0].products_id.indexOf(',')) > -1 && (orderedDetailsResult[0].qty.indexOf(',') > -1)) {
                var getOrderedProductsArr = orderedDetailsResult[0].products_id.split(','); //Split Ordered Products into Array
                var getOrderedQtyArr = orderedDetailsResult[0].qty.split(',');  //Split Ordered Qty into Array
    
                // Get Products and available_qty from inventory
                var inventoryDetailsQuery = "SELECT inventory_id,products_id,available_qty FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
                connection.query(inventoryDetailsQuery, function(inventoryDetailsErr, inventoryDetailsResult) {
                    if(inventoryDetailsErr) {
                        res.status(200).send({
                            error : true,
                            message : inventoryDetailsErr,
                            //check : 2
                        });
                    }
    
                    //Updation Process
                    else {
                        var getInventoryProductsArr = inventoryDetailsResult[0].products_id.split(','); //Split Inventory products into Array 
                        var getInventoryQtyArr = inventoryDetailsResult[0].available_qty.split(',');    //Split Inventory available_qty into Array
                        getOrderedProductsArr.forEach(function(item,index) { // For each ordered product add the ordered quantity to available quantity of each ordered product in inventory
                            var inventoryProductIndex = getInventoryProductsArr.indexOf(item);  // Find index of each ordered item in inventory products
                            var inventoryAvailableQty = parseInt(getInventoryQtyArr[inventoryProductIndex], 10);    //After finding product convert the corresponding quantity to integer
                            var orderedQty = parseInt(getOrderedQtyArr[index], 10); //Convert the ordered product quantity to integer based on it's index
                            inventoryAvailableQty = inventoryAvailableQty+orderedQty;   //update available_qty
                            getInventoryQtyArr[inventoryProductIndex] = inventoryAvailableQty;    //Assign the modified qty to that place
                        });
                        var inventoryProductsStr = getInventoryProductsArr.join(',');   
                        var inventoryQtyStr = getInventoryQtyArr.join(',');
                        
                        var updatedInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+inventoryProductsStr+"', available_qty = '"+inventoryQtyStr+"' WHERE merchant_id = "+merchant_id+"";
                        connection.query(updatedInventoryQuery, function(updatedInventoryErr, updatedInventoryResult) {
                            if(updatedInventoryErr) {
                                res.status(200).send({
                                    error : true,
                                    message : updatedInventoryErr,
                                    //check : 3
                                });
                            }
                            else if(updatedInventoryResult.changedRows != 0) {
                                
                                //Update the Status as declined
                                var updateDeclineQuery = "UPDATE orders SET order_status = "+order_status_id+" WHERE order_id = "+order_id+" AND merchant_id = "+merchant_id+"";
                                connection.query(updateDeclineQuery, function(updateDeclineErr, updateDeclineResult) {
                                    if(updateDeclineErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : updateDeclineErr,
                                            //check : 1
                                        });
                                    }
                                    else if(updateDeclineResult.changedRows != 0) {

                                        // Notification Alert //

                                        var notificationQuery = "SELECT users.*, orders.tracking_id, merchants.shop_name FROM users, orders, merchants WHERE orders.user_id = users.user_id AND merchants.merchant_id = "+merchant_id+" AND orders.order_id = "+order_id+"";
                                        // var notificationQuery = "SELECT users.*, merchants.shop_name FROM users LEFT JOIN merchants on merchants.merchant_id = "+merchant_id+" WHERE users.user_id = "+user_id+"";
                                        connection.query(notificationQuery, function(notificationErr, notificationResult) {
                                            if(notificationErr) {
                                                res.status(200).send({
                                                    error : true,
                                                    message : notificationErr
                                                });
                                            }
                                            else if(notificationResult.length != 0) {
                                                // Check device type and send notifications
                                                if(notificationResult[0].os_type.search('droid') != -1) {
                                                    Notify.userNotificationAndroid(notificationResult[0].fcm_id, "Order Declined", "Sorry your order declined", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, shop_name : notificationResult[0].shop_name});
                                                }
                                                else if(notificationResult[0].os_type.search('IOS') != -1) {
                                                    Notify.userNotificationIOS(notificationResult[0].fcm_id, "Order Declined", "Sorry your order declined", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, shop_name : notificationResult[0].shop_name});
                                                }
                                            }

                                            //Send Mail to user regarding this order placement...

                                            var fetchOrderDetailsUrl = "http://35.163.82.63:8080/pyg/user/getOrderById/?orderId="+order_id+"";
                                            request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                                                if(err) {
                                                    mailFlag = 0;
                                                }
                                                else if(!err && result.statusCode != 404) {
                                                    var products = new Array();
                                                    body.order.products.forEach(function(item) {
                                                        temp_arr = [
                                                            item.product_name
                                                        ];
                                                        products.push(temp_arr);
                                                    });

                                                    user_email = setUserEmail(1);
                                                    var html = "<p><h3>Hello,</h3></p><p>Thanks for shopping with us. Unfortunately, one or more of the items you ordered is currently out of stock and has been canceled.<br>You will not be charged for these items, and we apologize for this inconvenience</p><p><h4><strong>Order Details</strong></h4></p><br><p><strong>Shop Name :</strong> "+body.order.shop_name+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount :</strong>£ "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                                                    Mail.sendMail(user_email, "PYG Order Cancellation Details", html);

                                                    //Send to merchant also
                                                    merchant_email = setMerchantEmail(1);
                                                    var merchantHtml = "<p><h3>Hello,</h3></p><p>Order has been successfully cancelled. For more details visit PYG application</p><p><h4><strong>Order Details</strong></h4></p><p><strong>User Email:</strong> "+user_email.toString()+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount :</strong>£ "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                                                    Mail.sendMail(merchant_email, "PYG Order Cancellation by YOU", merchantHtml);
                                                    mailFlag = 1;
                                                }
                                            });
                                        });

                                        res.status(201).send({
                                            error : false,
                                            status : 'Order Declined by Merchant',
                                            order_id : order_id,
                                            user_id : user_id,
                                            merchant_id : merchant_id,
                                            //check : 2
                                        });
                                    }
                                });
                            }
                        });
                    }       
                });
            }
    
            //If ordered item is only 1
            else if((orderedDetailsResult[0].products_id.indexOf(',') == -1) && (orderedDetailsResult[0].qty.indexOf(',') == -1)) {
                var singleProductInventoryDetailsQuery = "SELECT inventory_id,products_id,available_qty FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
                connection.query(singleProductInventoryDetailsQuery, function(singleProductInventoryErr, singleProductInventoryResult) {
                    if(singleProductInventoryErr) {
                        res.status(200).send({
                            error : false,
                            message : singleProductInventoryErr,
                            //check : 5
                        });
                    }
    
                    //updation Process
                    else {
                        var getInventoryProducts_Arr = singleProductInventoryResult[0].products_id.split(',');  //Split Inventory Products into array in inventory
                        var getInventoryQty_Arr = singleProductInventoryResult[0].available_qty.split(','); //Split available quantity into array in inventory
                        var orderedProductInventoryIndex = getInventoryProducts_Arr.indexOf(orderedDetailsResult[0].products_id);   //Get index of the ordered product in inventory
                        var orderedQty_AvailabilityInInventory = parseInt(getInventoryQty_Arr[orderedProductInventoryIndex], 10);   //Convert the corresponding qty in inventory to integer 
                        var orderedQtyInt = parseInt(orderedDetailsResult[0].qty);  //Convert the ordered qty to integer
                        orderedQty_AvailabilityInInventory = orderedQty_AvailabilityInInventory+orderedQtyInt;  //Updation
                        getInventoryQty_Arr[orderedProductInventoryIndex] = orderedQty_AvailabilityInInventory; //Replace the modified qty to that place
    
                        var singleInventoryProductsStr = getInventoryProducts_Arr.join(',');   
                        var singleInventoryQtyStr = getInventoryQty_Arr.join(',');
                        
                        var singleUpdatedInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+singleInventoryProductsStr+"', available_qty = '"+singleInventoryQtyStr+"' WHERE merchant_id = "+merchant_id+"";
                        connection.query(singleUpdatedInventoryQuery, function(singleUpdatedInventoryErr, singleUpdatedInventoryResult) {
                            if(singleUpdatedInventoryErr) {
                                res.status(200).send({
                                    error : true,
                                    message : singleUpdatedInventoryErr,
                                    //check : 7
                                });
                            }
                            else if(singleUpdatedInventoryResult.changedRows != 0) {
                                
                                //Update the Status as declined
                                var updateDeclineQuery = "UPDATE orders SET order_status = "+order_status_id+" WHERE order_id = "+order_id+" AND merchant_id = "+merchant_id+"";
                                connection.query(updateDeclineQuery, function(updateDeclineErr, updateDeclineResult) {
                                    if(updateDeclineErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : updateDeclineErr,
                                            //check : 1
                                        });
                                    }
                                    else if(updateDeclineResult.changedRows != 0) {

                                        // Notification Alert //

                                        var notificationQuery = "SELECT users.*, orders.tracking_id, merchants.shop_name FROM users, orders, merchants WHERE orders.user_id = users.user_id AND merchants.merchant_id = "+merchant_id+" AND orders.order_id = "+order_id+"";
                                        // var notificationQuery = "SELECT users.*, merchants.shop_name FROM users LEFT JOIN merchants on merchants.merchant_id = "+merchant_id+" WHERE users.user_id = "+user_id+"";
                                        connection.query(notificationQuery, function(notificationErr, notificationResult) {
                                            if(notificationErr) {
                                                res.status(200).send({
                                                    error : true,
                                                    message : notificationErr
                                                });
                                            }
                                            else if(notificationResult.length != 0) {
                                                // Check device type and send notifications
                                                if(notificationResult[0].os_type.search('droid') != -1) {
                                                    Notify.userNotificationAndroid(notificationResult[0].fcm_id, "Order Declined", "Sorry your order declined", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, shop_name : notificationResult[0].shop_name});
                                                }
                                                else if(notificationResult[0].os_type.search('IOS') != -1) {
                                                    Notify.userNotificationIOS(notificationResult[0].fcm_id, "Order Declined", "Sorry your order declined", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, shop_name : notificationResult[0].shop_name});
                                                }
                                            }

                                            //Send Mail to user regarding this order placement...

                                            var fetchOrderDetailsUrl = "http://35.163.82.63:8080/pyg/user/getOrderById/?orderId="+order_id+"";
                                            request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                                                if(err) {
                                                    mailFlag = 0;
                                                }
                                                else if(!err && result.statusCode != 404) {
                                                    var products = new Array();
                                                    body.order.products.forEach(function(item) {
                                                        temp_arr = [
                                                            item.product_name
                                                        ];
                                                        products.push(temp_arr);
                                                    });

                                                    user_email = setUserEmail(1);
                                                    var html = "<p><h3>Hello,</h3></p><p>Thanks for shopping with us. Unfortunately, one or more of the items you ordered is currently out of stock and has been canceled.<br>You will not be charged for these items, and we apologize for this inconvenience</p><p><h4><strong>Order Details</strong></h4></p><br><p><strong>Shop Name :</strong> "+body.order.shop_name+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount :</strong>£ "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                                                    Mail.sendMail(user_email, "PYG Order Cancellation Details", html);

                                                    //Send to merchant also
                                                    merchant_email = setMerchantEmail(1);
                                                    var merchantHtml = "<p><h3>Hello,</h3></p><p>Order has been successfully cancelled. For more details visit PYG application</p><p><h4><strong>Order Details</strong></h4></p><p><strong>User Email:</strong> "+user_email.toString()+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount :</strong>£ "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                                                    Mail.sendMail(merchant_email, "PYG Order Cancellation by YOU", merchantHtml);
                                                    mailFlag = 1;
                                                }
                                            });
                                        });

                                        res.status(201).send({
                                            error : false,
                                            status : 'Order Declined by Merchant',
                                            order_id : order_id,
                                            user_id : user_id,
                                            merchant_id : merchant_id,
                                            //check : 2
                                        });
                                    }
                                });
                            }
                        });
                    }       
                });
            }
        });
    }

    // If merchant Approved
    else if(approved == 1) {

        // Update the status collecting time and approval status
        var updateStatusQuery = "UPDATE orders SET order_status = "+order_status_id+", collecting_time = '"+collecting_time+"', merchant_approved = 1 WHERE order_id = "+order_id+" AND merchant_id = "+merchant_id+"";
        connection.query(updateStatusQuery, function(updateStatusErr, updateStatusResult) {
            if(updateStatusErr) {
                res.status(200).send({
                    error : true,
                    message : updateStatusErr,
                    //check : 3
                });
            }

            else if(updateStatusResult.affectedRows != 0) {

                // Notification Alert //
                var notificationQuery = "SELECT users.*, orders.tracking_id, merchants.shop_name FROM users, orders, merchants WHERE orders.user_id = users.user_id AND merchants.merchant_id = "+merchant_id+" AND orders.order_id = "+order_id+"";
                // var notificationQuery = "SELECT users.*, merchants.shop_name FROM users LEFT JOIN merchants on merchants.merchant_id = "+merchant_id+" WHERE users.user_id = "+user_id+"";
                connection.query(notificationQuery, function(notificationErr, notificationResult) {
                    if(notificationErr) {
                        res.status(200).send({
                            error : true,
                            message : notificationErr
                        });
                    }
                    else if(notificationResult.length != 0) {
                        // Check device type and send notifications
                        if(notificationResult[0].os_type.search('droid') != -1) {
                            Notify.userNotificationAndroid(notificationResult[0].fcm_id, "Order accepted", ""+notificationResult[0].shop_name+" accepted your Order", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, collecting_time : collecting_time, shop_name : notificationResult[0].shop_name});
                        }
                        else if(notificationResult[0].os_type.search('IOS') != -1) {
                            Notify.userNotificationIOS(notificationResult[0].fcm_id, "Order accepted", ""+notificationResult[0].shop_name+" accepted your Order", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, collecting_time : collecting_time, shop_name : notificationResult[0].shop_name});
                        }
                    }

                    //Send Mail to user regarding this order placement...

                    var fetchOrderDetailsUrl = "http://35.163.82.63:8080/pyg/user/getOrderById/?orderId="+order_id+"";
                    request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                        if(err) {
                            mailFlag = 0;
                        }
                        else if(!err && result.statusCode != 404) {
                            var products = new Array();
                            body.order.products.forEach(function(item) {
                                temp_arr = [
                                    item.product_name
                                ];
                                products.push(temp_arr + " ; ");
                            });

                            user_email = setUserEmail(1);
                            var html = "<p><h3>Hello,</h3></p><p>Thanks for shopping with us. Your estimated collecting time has been confirmed.<br>If you would like to view the status of your order or make any changes to it please visit PYG Application</p><p><h4><strong>Order Details</strong></h4></p><p><strong>Shop Name :</strong> "+body.order.shop_name+"<br><br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount : </strong>£ "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Collecting Time :</strong> "+collecting_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                            Mail.sendMail(user_email, "PYG Order Confirmation", html);

                            //Send to merchant also
                            merchant_email = setMerchantEmail(1);
                            var merchantHtml = "<p><h3>Hello,</h3><p>You have confirmed one of your orders. For more details visit PYG application</p><h4><strong>Order Details</strong></h4></p><p><strong>User Email:</strong> "+user_email.toString()+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount</strong>:£  "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Collecting Time :</strong> "+collecting_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                            Mail.sendMail(merchant_email, "PYG Order Confirmation by YOU", merchantHtml);
                            mailFlag = 1;
                        }
                    });
                });

                res.status(200).send({
                    error : false,
                    status : "Order has been approved"
                });
            }
        });
    }

});

// Merchant reviewing Order V1(order approved/not approved)

router.post('/reviewOrderV1', function(req, res) {

    var merchant_id = req.body.merchant_id;
    var order_id = req.body.order_id;
    var user_id = req.body.user_id;
    var approved = req.body.approved;
    var order_status_id = req.body.order_status_id;
    var collecting_time = req.body.collecting_time;
    var mailFlag = 0;
    var merchant_email;
    var user_email;

    // Get merchant Email through order
    var merchantEmailQuery = "SELECT orders.merchant_id,merchants.merchant_email FROM orders LEFT JOIN merchants ON orders.merchant_id = merchants.merchant_id WHERE orders.order_id = "+order_id+"";
    connection.query(merchantEmailQuery, function(merchantEmailErr, merchantEmailResult) {
        if(!merchantEmailErr) {
            setMerchantEmail(merchantEmailResult[0].merchant_email);
        }
    });

    // Get user Email through order
    var UserEmailQuery = "SELECT orders.user_id,users.email as user_email FROM orders LEFT JOIN users ON orders.user_id = users.user_id WHERE orders.order_id = "+order_id+"";
    connection.query(UserEmailQuery, function(userEmailErr, userEmailResult) {
        if(!userEmailErr) {
            setUserEmail(userEmailResult[0].user_email);
        }
    });
    
    // If merchant not approved the order
    if(approved == 0) {         
        //Update the Status as declined
        var updateDeclineQuery = "UPDATE orders SET order_status = "+order_status_id+" WHERE order_id = "+order_id+" AND merchant_id = "+merchant_id+"";
        connection.query(updateDeclineQuery, function(updateDeclineErr, updateDeclineResult) {
            if(updateDeclineErr) {
                res.status(200).send({
                    error : true,
                    message : updateDeclineErr,
                    //check : 1
                });
            }
            else if(updateDeclineResult.changedRows != 0) {

                // Notification Alert //

                var notificationQuery = "SELECT users.*, orders.tracking_id, merchants.shop_name FROM users, orders, merchants WHERE orders.user_id = users.user_id AND merchants.merchant_id = "+merchant_id+" AND orders.order_id = "+order_id+"";
                // var notificationQuery = "SELECT users.*, merchants.shop_name FROM users LEFT JOIN merchants on merchants.merchant_id = "+merchant_id+" WHERE users.user_id = "+user_id+"";
                connection.query(notificationQuery, function(notificationErr, notificationResult) {
                    if(notificationErr) {
                        res.status(200).send({
                            error : true,
                            message : notificationErr
                        });
                    }
                    else if(notificationResult.length != 0) {
                        // Check device type and send notifications
                        if(notificationResult[0].os_type.search('droid') != -1) {
                            Notify.userNotificationAndroid(notificationResult[0].fcm_id, "Order Declined", "Sorry your order declined", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, shop_name : notificationResult[0].shop_name});
                        }
                        else if(notificationResult[0].os_type.search('IOS') != -1) {
                            Notify.userNotificationIOS(notificationResult[0].fcm_id, "Order Declined", "Sorry your order declined", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, shop_name : notificationResult[0].shop_name});
                        }
                    }

                    //Send Mail to user regarding this order placement...

                    var fetchOrderDetailsUrl = "http://35.163.82.63:8080/pyg/user/getOrderById/?orderId="+order_id+"";
                    request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                        if(err) {
                            mailFlag = 0;
                        }
                        else if(!err && result.statusCode != 404) {
                            var products = new Array();
                            body.order.products.forEach(function(item) {
                                temp_arr = [
                                    item.product_name
                                ];
                                products.push(temp_arr);
                            });

                            user_email = setUserEmail(1);
                            var html = "<p><h3>Hello,</h3></p><p>Thanks for shopping with us. Unfortunately, one or more of the items you ordered is currently out of stock and has been canceled.<br>You will not be charged for these items, and we apologize for this inconvenience</p><p><h4><strong>Order Details</strong></h4></p><br><p><strong>Shop Name :</strong> "+body.order.shop_name+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount :</strong>£ "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                            Mail.sendMail(user_email, "PYG Order Cancellation Details", html);

                            //Send to merchant also
                            merchant_email = setMerchantEmail(1);
                            var merchantHtml = "<p><h3>Hello,</h3></p><p>Order has been successfully cancelled. For more details visit PYG application</p><p><h4><strong>Order Details</strong></h4></p><p><strong>User Email:</strong> "+user_email.toString()+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount :</strong>£ "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                            Mail.sendMail(merchant_email, "PYG Order Cancellation by YOU", merchantHtml);
                            mailFlag = 1;
                        }
                    });
                });

                res.status(201).send({
                    error : false,
                    status : 'Order Declined by Merchant',
                    order_id : order_id,
                    user_id : user_id,
                    merchant_id : merchant_id,
                    //check : 2
                });
            }
        });
    }
    // If merchant Approved
    else if(approved == 1) {
        // Update the status collecting time and approval status
        var updateStatusQuery = "UPDATE orders SET order_status = "+order_status_id+", collecting_time = '"+collecting_time+"', merchant_approved = 1 WHERE order_id = "+order_id+" AND merchant_id = "+merchant_id+"";
        connection.query(updateStatusQuery, function(updateStatusErr, updateStatusResult) {
            if(updateStatusErr) {
                res.status(200).send({
                    error : true,
                    message : updateStatusErr,
                    //check : 3
                });
            }

            else if(updateStatusResult.affectedRows != 0) {

                // Notification Alert //
                var notificationQuery = "SELECT users.*, orders.tracking_id, merchants.shop_name FROM users, orders, merchants WHERE orders.user_id = users.user_id AND merchants.merchant_id = "+merchant_id+" AND orders.order_id = "+order_id+"";
                // var notificationQuery = "SELECT users.*, merchants.shop_name FROM users LEFT JOIN merchants on merchants.merchant_id = "+merchant_id+" WHERE users.user_id = "+user_id+"";
                connection.query(notificationQuery, function(notificationErr, notificationResult) {
                    if(notificationErr) {
                        res.status(200).send({
                            error : true,
                            message : notificationErr
                        });
                    }
                    else if(notificationResult.length != 0) {
                        // Check device type and send notifications
                        if(notificationResult[0].os_type.search('droid') != -1) {
                            Notify.userNotificationAndroid(notificationResult[0].fcm_id, "Order accepted", ""+notificationResult[0].shop_name+" accepted your Order", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, collecting_time : collecting_time, shop_name : notificationResult[0].shop_name});
                        }
                        else if(notificationResult[0].os_type.search('IOS') != -1) {
                            Notify.userNotificationIOS(notificationResult[0].fcm_id, "Order accepted", ""+notificationResult[0].shop_name+" accepted your Order", {order_id : order_id, tracking_id : notificationResult[0].tracking_id, collecting_time : collecting_time, shop_name : notificationResult[0].shop_name});
                        }
                    }

                    //Send Mail to user regarding this order placement...

                    var fetchOrderDetailsUrl = "http://35.163.82.63:8080/pyg/user/getOrderById/?orderId="+order_id+"";
                    request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                        if(err) {
                            mailFlag = 0;
                        }
                        else if(!err && result.statusCode != 404) {
                            var products = new Array();
                            body.order.products.forEach(function(item) {
                                temp_arr = [
                                    item.product_name 
                                    // item.quantification,
                                    // item.ordered_qty,
                                    // item.price,
                                    // item.total_price
                                ];
                                products.push(temp_arr + " ; ");
                            });

                            user_email = setUserEmail(1);
                            var html = "<p><h3>Hello,</h3></p><p>Thanks for shopping with us. Your estimated collecting time has been confirmed.<br>If you would like to view the status of your order or make any changes to it please visit PYG Application</p><p><h4><strong>Order Details</strong></h4></p><p><strong>Shop Name :</strong> "+body.order.shop_name+"<br><br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount : </strong>£ "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Collecting Time :</strong> "+collecting_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                            Mail.sendMail(user_email, "PYG Order Confirmation", html);

                            //Send to merchant also
                            merchant_email = setMerchantEmail(1);
                            var merchantHtml = "<p><h3>Hello,</h3><p>You have confirmed one of your orders. For more details visit PYG application</p><h4><strong>Order Details</strong></h4></p><p><strong>User Email:</strong> "+user_email.toString()+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount</strong>:£  "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Collecting Time :</strong> "+collecting_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                            Mail.sendMail(merchant_email, "PYG Order Confirmation by YOU", merchantHtml);
                            mailFlag = 1;
                        }
                    });
                });

                res.status(200).send({
                    error : false,
                    status : "Order has been approved"
                });
            }
        });
    }
});

// Deliver Order to Consumer V1

router.post('/deliverOrderV1', function(req, res) {

    // Required Params
    var merchant_id = req.body.merchant_id;
    var user_id = req.body.user_id;
    var order_id = req.body.order_id;
    var order_status_id = req.body.order_status_id;
    var mailFlag = 0;
    var merchant_email;
    var user_email;
    var productCount = 0;
    var count = 0;

    // To Send Mail to Merchants regarding Order
    var merchantEmailQuery = "SELECT merchant_email FROM merchants WHERE merchant_id = "+merchant_id+"";
    connection.query(merchantEmailQuery, function(merchantEmailErr, merchantEmailResult) {
        if(!merchantEmailErr) {
            setMerchantEmail(merchantEmailResult[0].merchant_email);
        }
    });

    // Get user Email through order
    var UserEmailQuery = "SELECT orders.user_id,users.email as user_email FROM orders LEFT JOIN users ON orders.user_id = users.user_id WHERE orders.order_id = "+order_id+"";
    connection.query(UserEmailQuery, function(userEmailErr, userEmailResult) {
        if(!userEmailErr) {
            setUserEmail(userEmailResult[0].user_email);
        }
    });
    //If merchant delivered his inventory should be modified here get the ordered products and ordered qty

    var orderedDetailsQuery = "SELECT products_id,qty FROM orders WHERE order_id = "+order_id+"";
    connection.query(orderedDetailsQuery, function(orderedDetailsErr, orderedDetailsResult) {
        if(orderedDetailsErr) {
            res.status(200).send({
                error : true,
                message : orderedDetailsErr,
                //check : 1
            });
        }

        //If ordered items are more than 1

        else if((orderedDetailsResult[0].products_id.indexOf(',')) > -1 && (orderedDetailsResult[0].qty.indexOf(',') > -1)) {
            var getOrderedProductsArr = orderedDetailsResult[0].products_id.split(','); //Split Ordered Products into Array
            var getOrderedQtyArr = orderedDetailsResult[0].qty.split(',');  //Split Ordered Qty into Array

            // Get Products and available_qty from inventory
            var inventoryDetailsQuery = "SELECT inventory_id,products_id,available_qty FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
            connection.query(inventoryDetailsQuery, function(inventoryDetailsErr, inventoryDetailsResult) {
                if(inventoryDetailsErr) {
                    res.status(200).send({
                        error : true,
                        message : inventoryDetailsErr,
                        //check : 2
                    });
                }

                //Deduction Process
                else {
                    var getInventoryProductsArr = inventoryDetailsResult[0].products_id.split(','); //Split Inventory products into Array 
                    var getInventoryQtyArr = inventoryDetailsResult[0].available_qty.split(',');    //Split Inventory available_qty into Array
                    var inventoryAvailableQty = 0;
                    var orderedQty = 0;
                    var inventoryProductIndex = 0;
                    getOrderedProductsArr.forEach(function(item,index) { // For each ordered product deduct the ordered quantity from available quantity of each ordered product in inventory
                        inventoryProductIndex = getInventoryProductsArr.indexOf(item);  // Find index of each ordered item in inventory products
                        inventoryAvailableQty = parseInt(getInventoryQtyArr[inventoryProductIndex], 10);    //After finding product convert the corresponding quantity to integer
                        orderedQty = parseInt(getOrderedQtyArr[index], 10); //Convert the ordered product quantity to integer based on it's index
                        if(inventoryAvailableQty>=orderedQty) {
                            count = count+1;
                        } 
                        inventoryAvailableQty = inventoryAvailableQty-orderedQty;   //Deduction from available_qty
                        getInventoryQtyArr[inventoryProductIndex] = inventoryAvailableQty;    //Assign the modified qty to that place
                        productCount = index;
                    });
                    var inventoryProductsStr = getInventoryProductsArr.join(',');   
                    var inventoryQtyStr = getInventoryQtyArr.join(',');
                    
                    if(count == (productCount+1)) {
                        var updatedInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+inventoryProductsStr+"', available_qty = '"+inventoryQtyStr+"' WHERE merchant_id = "+merchant_id+"";
                        connection.query(updatedInventoryQuery, function(updatedInventoryErr, updatedInventoryResult) {
                            if(updatedInventoryErr) {
                                res.status(200).send({
                                    error : true,
                                    message : updatedInventoryErr,
                                    //check : 3
                                });
                            }
                            else if(updatedInventoryResult.changedRows != 0) {
                                // Order status should be changed to 'Delivered'
                                var orderDeliverQuery = "UPDATE orders SET order_status = "+order_status_id+" WHERE order_id = "+order_id+" AND user_id = "+user_id+"";
                                connection.query(orderDeliverQuery, function(orderDeliverErr, orderDeliverResult) {
                                    if(orderDeliverErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : orderDeliverErr
                                        });
                                    }
                                    else if(orderDeliverResult.affectedRows != 0 && order_status_id == 4) {
                                        res.status(201).send({
                                            error : false,
                                            status : 'Order Delivered and Inventory Modified',
                                            merchant_id : merchant_id,
                                            inventory_id : inventoryDetailsResult[0].inventory_id,
                                            products : inventoryProductsStr,
                                            available_qty : inventoryQtyStr,
                                            //check : 4
                                        });
                                        //Send Mail to user regarding this order placement...
                                        var fetchOrderDetailsUrl = "http://35.163.82.63:8080/pyg/user/getOrderById/?orderId="+order_id+"";
                                        request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                                            if(err) {
                                                mailFlag = 0;
                                            }
                                            else if(!err && result.statusCode != 404) {
                                                var products = new Array();
                                                body.order.products.forEach(function(item) {
                                                    temp_arr = [
                                                        item.product_name
                                                    ];
                                                    products.push(temp_arr);
                                                });

                                                user_email = setUserEmail(1);
                                                var html = "<p><h3>Hello,</h3></p><p>Thanks for shopping with us. Hope you have collected your item.<br>Please rate your experience</p><h4><strong>Order Details</strong></h4><strong>Shop Name</strong>: "+body.order.shop_name+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount</strong>: £ "+body.order.bill_amount+"<br><strong>Tracking ID</strong> : "+body.order.tracking_id+"<br><strong>Order Time</strong> : "+body.order.order_time+"<br><strong>Payment Mode</strong> : "+body.order.payment_mode+"<br><strong>Order Status</strong> : "+body.order.order_status_name+"</p>"
                                                Mail.sendMail(user_email, "Your PYG Order has been delivered", html);

                                                //Send to merchant also
                                                merchant_email = setMerchantEmail(1);
                                                var merchantHtml = "<p><h3>Hello,</h3></p><p>Your order has been completed successfully</p><h4><strong>Order Details</strong></h4></p><p><strong>User Email:</strong> "+user_email.toString()+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount</strong>:£  "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                                                Mail.sendMail(merchant_email, "PYG Order Completion", merchantHtml);
                                                mailFlag = 1;
                                            }
                                        });

                                        // Notification Segment
                                        var notificationQuery = "SELECT users.*, orders.tracking_id, merchants.shop_name FROM users, orders, merchants WHERE orders.user_id = users.user_id AND merchants.merchant_id = "+merchant_id+" AND orders.order_id = "+order_id+"";
                                        // var notificationQuery = "SELECT users.*,merchants.shop_name FROM users LEFT JOIN merchants ON merchants.merchant_id = "+merchant_id+" WHERE users.user_id = "+user_id+"";
                                        connection.query(notificationQuery, function(notificationErr, notificationResult) {
                                            if(notificationErr) {
                                                res.status(200).send({
                                                    error : true,
                                                    message : notificationErr
                                                });
                                            }
                                            else if(notificationResult.length != 0) {

                                                // Check device type and send notifications
                                                if(notificationResult[0].os_type.search('droid') != -1) {
                                                    Notify.userNotificationAndroid(notificationResult[0].fcm_id, "Order Completion", "Your order was completed successfully. Please share your feedback", {order_id : order_id, tracking_id : notificationResult[0].tracking_id,shop_name : notificationResult[0].shop_name});
                                                }
                                                else if(notificationResult[0].os_type.search('IOS') != -1) {
                                                    Notify.userNotificationIOS(notificationResult[0].fcm_id, "Order Completion", "Your order was completed successfully. Please share your feedback", {order_id : order_id, tracking_id : notificationResult[0].tracking_id,shop_name : notificationResult[0].shop_name});
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        res.status(200).send({
                                            error : true,
                                            message : "Problem in updating inventory"
                                        });
                                    }
                                });
                                
                            }
                        });
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : "low stock"
                        });
                    }
                }
            });       
        }

        //If ordered item is only 1
        else if((orderedDetailsResult[0].products_id.indexOf(',') == -1) && (orderedDetailsResult[0].qty.indexOf(',') == -1)) {
            var singleProductInventoryDetailsQuery = "SELECT inventory_id,products_id,available_qty FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
            connection.query(singleProductInventoryDetailsQuery, function(singleProductInventoryErr, singleProductInventoryResult) {
                if(singleProductInventoryErr) {
                    res.status(200).send({
                        error : false,
                        message : singleProductInventoryErr,
                        //check : 5
                    });
                }

                //Deduction Process
                else {
                    var getInventoryProducts_Arr = singleProductInventoryResult[0].products_id.split(',');  //Split Inventory Products into array in inventory
                    var getInventoryQty_Arr = singleProductInventoryResult[0].available_qty.split(','); //Split available quantity into array in inventory
                    var orderedProductInventoryIndex = getInventoryProducts_Arr.indexOf(orderedDetailsResult[0].products_id);   //Get index of the ordered product in inventory
                    var orderedQty_AvailabilityInInventory = parseInt(getInventoryQty_Arr[orderedProductInventoryIndex], 10);   //Convert the corresponding qty in inventory to integer 
                    var orderedQtyInt = parseInt(orderedDetailsResult[0].qty);  //Convert the ordered qty to integer
                    if(orderedQty_AvailabilityInInventory>=orderedQtyInt) {
                        count = count+1;
                    }
                    orderedQty_AvailabilityInInventory = orderedQty_AvailabilityInInventory-orderedQtyInt;  //Deduction
                    getInventoryQty_Arr[orderedProductInventoryIndex] = orderedQty_AvailabilityInInventory; //Replace the modified qty to that place

                    var singleInventoryProductsStr = getInventoryProducts_Arr.join(',');   
                    var singleInventoryQtyStr = getInventoryQty_Arr.join(',');
                    if(count == 1) {
                        var singleUpdatedInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+singleInventoryProductsStr+"', available_qty = '"+singleInventoryQtyStr+"' WHERE merchant_id = "+merchant_id+"";
                        connection.query(singleUpdatedInventoryQuery, function(singleUpdatedInventoryErr, singleUpdatedInventoryResult) {
                            if(singleUpdatedInventoryErr) {
                                res.status(200).send({
                                    error : true,
                                    message : singleUpdatedInventoryErr,
                                    //check : 7
                                });
                            }
                            else if(singleUpdatedInventoryResult.changedRows != 0) {
                                // Order status should be changed to 'Delivered'
                                var orderDeliverQuery = "UPDATE orders SET order_status = "+order_status_id+" WHERE order_id = "+order_id+" AND user_id = "+user_id+"";
                                connection.query(orderDeliverQuery, function(orderDeliverErr, orderDeliverResult) {
                                    if(orderDeliverErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : orderDeliverErr
                                        });
                                    }
                                    else if(orderDeliverResult.affectedRows != 0 && order_status_id == 4) {
                                        res.status(201).send({
                                            error : false,
                                            status : 'Order Delivered and Inventory Modified',
                                            merchant_id : merchant_id,
                                            inventory_id : singleProductInventoryResult[0].inventory_id,
                                            products : singleInventoryProductsStr,
                                            available_qty : singleInventoryQtyStr,
                                            //check : 8
                                        });
                                        //Send Mail to user regarding this order placement...
                                        var fetchOrderDetailsUrl = "http://35.163.82.63:8080/pyg/user/getOrderById/?orderId="+order_id+"";
                                        request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                                            if(err) {
                                                mailFlag = 0;
                                            }
                                            else if(!err && result.statusCode != 404) {
                                                var products = new Array();
                                                body.order.products.forEach(function(item) {
                                                    temp_arr = [
                                                        item.product_name
                                                    ];
                                                    products.push(temp_arr);
                                                });

                                                user_email = setUserEmail(1);
                                                var html = "<p><h3>Hello,</h3></p><p>Thanks for shopping with us. Hope you have collected your item.<br>Please rate your experience</p><h4><strong>Order Details</strong></h4><strong>Shop Name</strong>: "+body.order.shop_name+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount</strong>: £ "+body.order.bill_amount+"<br><strong>Tracking ID</strong> : "+body.order.tracking_id+"<br><strong>Order Time</strong> : "+body.order.order_time+"<br><strong>Payment Mode</strong> : "+body.order.payment_mode+"<br><strong>Order Status</strong> : "+body.order.order_status_name+"</p>"
                                                Mail.sendMail(user_email, "Your PYG Order has been delivered", html);

                                                //Send to merchant also
                                                merchant_email = setMerchantEmail(1);
                                                var merchantHtml = "<p><h3>Hello,</h3></p><p>Your order has been completed successfully</p><h4><strong>Order Details</strong></h4></p><p><strong>User Email:</strong> "+user_email.toString()+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount</strong>:£  "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                                                Mail.sendMail(merchant_email, "PYG Order Completion", merchantHtml);
                                                mailFlag = 1;
                                            }
                                        });

                                        // Notification Segment
                                        var notificationQuery = "SELECT users.*, orders.tracking_id, merchants.shop_name FROM users, orders, merchants WHERE orders.user_id = users.user_id AND merchants.merchant_id = "+merchant_id+" AND orders.order_id = "+order_id+"";
                                        // var notificationQuery = "SELECT users.*,merchants.shop_name FROM users LEFT JOIN merchants ON merchants.merchant_id = "+merchant_id+" WHERE users.user_id = "+user_id+"";
                                        connection.query(notificationQuery, function(notificationErr, notificationResult) {
                                            if(notificationErr) {
                                                res.status(200).send({
                                                    error : true,
                                                    message : notificationErr
                                                });
                                            }
                                            else if(notificationResult.length != 0) {

                                                // Check device type and send notifications
                                                if(notificationResult[0].os_type.search('droid') != -1) {
                                                    Notify.userNotificationAndroid(notificationResult[0].fcm_id, "Order Completion", "Your order was completed successfully. Please share your feedback", {order_id : order_id, tracking_id : notificationResult[0].tracking_id,shop_name : notificationResult[0].shop_name});
                                                }
                                                else if(notificationResult[0].os_type.search('IOS') != -1) {
                                                    Notify.userNotificationIOS(notificationResult[0].fcm_id, "Order Completion", "Your order was completed successfully. Please share your feedback", {order_id : order_id, tracking_id : notificationResult[0].tracking_id,shop_name : notificationResult[0].shop_name});
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        res.status(200).send({
                                            error : true,
                                            message : "Problem in updating inventory"
                                        });
                                    }
                                });    
                            }
                        });
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : "low stock"
                        });
                    }
                }
            });
        }
    });
});

// Deliver Order to Consumer

router.post('/deliverOrder', function(req, res) {

    // Required Params
    var merchant_id = req.body.merchant_id;
    var user_id = req.body.user_id;
    var order_id = req.body.order_id;
    var order_status_id = req.body.order_status_id;
    var mailFlag = 0;
    var merchant_email;
    var user_email;

    // To Send Mail to Merchants regarding Order
    var merchantEmailQuery = "SELECT merchant_email FROM merchants WHERE merchant_id = "+merchant_id+"";
    connection.query(merchantEmailQuery, function(merchantEmailErr, merchantEmailResult) {
        if(!merchantEmailErr) {
            setMerchantEmail(merchantEmailResult[0].merchant_email);
        }
    });

    // Get user Email through order
    var UserEmailQuery = "SELECT orders.user_id,users.email as user_email FROM orders LEFT JOIN users ON orders.user_id = users.user_id WHERE orders.order_id = "+order_id+"";
    connection.query(UserEmailQuery, function(userEmailErr, userEmailResult) {
        if(!userEmailErr) {
            setUserEmail(userEmailResult[0].user_email);
        }
    });

    // Order status should be changed to 'Delivered'
    var orderDeliverQuery = "UPDATE orders SET order_status = "+order_status_id+" WHERE order_id = "+order_id+" AND user_id = "+user_id+"";
    connection.query(orderDeliverQuery, function(orderDeliverErr, orderDeliverResult) {
        if(orderDeliverErr) {
            res.status(200).send({
                error : true,
                message : orderDeliverErr
            });
        }
        else if(orderDeliverResult.affectedRows != 0 && order_status_id == 4) {

            //Send Mail to user regarding this order placement...

            var fetchOrderDetailsUrl = "http://35.163.82.63:8080/pyg/user/getOrderById/?orderId="+order_id+"";
            request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                if(err) {
                    mailFlag = 0;
                }
                else if(!err && result.statusCode != 404) {
                    var products = new Array();
                    body.order.products.forEach(function(item) {
                        temp_arr = [
                            item.product_name
                        ];
                        products.push(temp_arr);
                    });

                    user_email = setUserEmail(1);
                    var html = "<p><h3>Hello,</h3></p><p>Thanks for shopping with us. Hope you have collected your item.<br>Please rate your experience</p><h4><strong>Order Details</strong></h4><strong>Shop Name</strong>: "+body.order.shop_name+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount</strong>: £ "+body.order.bill_amount+"<br><strong>Tracking ID</strong> : "+body.order.tracking_id+"<br><strong>Order Time</strong> : "+body.order.order_time+"<br><strong>Payment Mode</strong> : "+body.order.payment_mode+"<br><strong>Order Status</strong> : "+body.order.order_status_name+"</p>"
                    Mail.sendMail(user_email, "Your PYG Order has been delivered", html);

                    //Send to merchant also
                    merchant_email = setMerchantEmail(1);
                    var merchantHtml = "<p><h3>Hello,</h3></p><p>Your order has been completed successfully</p><h4><strong>Order Details</strong></h4></p><p><strong>User Email:</strong> "+user_email.toString()+"<br><strong>Products Name</strong>: "+products.toString()+"<br><strong>Bill Amount</strong>:£  "+body.order.bill_amount+"<br><strong>Tracking ID :</strong> "+body.order.tracking_id+"<br><strong>Order Time :</strong> "+body.order.order_time+"<br><strong>Payment Mode :</strong> "+body.order.payment_mode+"<br><strong>Order Status :</strong> "+body.order.order_status_name+"</p>";
                    Mail.sendMail(merchant_email, "PYG Order Completion", merchantHtml);
                    mailFlag = 1;
                }
            });

            // Notification Segment
            var notificationQuery = "SELECT users.*, orders.tracking_id, merchants.shop_name FROM users, orders, merchants WHERE orders.user_id = users.user_id AND merchants.merchant_id = "+merchant_id+" AND orders.order_id = "+order_id+"";
            // var notificationQuery = "SELECT users.*,merchants.shop_name FROM users LEFT JOIN merchants ON merchants.merchant_id = "+merchant_id+" WHERE users.user_id = "+user_id+"";
            connection.query(notificationQuery, function(notificationErr, notificationResult) {
                if(notificationErr) {
                    res.status(200).send({
                        error : true,
                        message : notificationErr
                    });
                }
                else if(notificationResult.length != 0) {

                    // Check device type and send notifications
                    if(notificationResult[0].os_type.search('droid') != -1) {
                        Notify.userNotificationAndroid(notificationResult[0].fcm_id, "Order Completion", "Your order was completed successfully. Please share your feedback", {order_id : order_id, tracking_id : notificationResult[0].tracking_id,shop_name : notificationResult[0].shop_name});
                    }
                    else if(notificationResult[0].os_type.search('IOS') != -1) {
                        Notify.userNotificationIOS(notificationResult[0].fcm_id, "Order Completion", "Your order was completed successfully. Please share your feedback", {order_id : order_id, tracking_id : notificationResult[0].tracking_id,shop_name : notificationResult[0].shop_name});
                    }
                }
            })

            //If merchant delivered his inventory should be modified here get the ordered products and ordered qty

            var orderedDetailsQuery = "SELECT products_id,qty FROM orders WHERE order_id = "+order_id+"";
                connection.query(orderedDetailsQuery, function(orderedDetailsErr, orderedDetailsResult) {
                    if(orderedDetailsErr) {
                        res.status(200).send({
                            error : true,
                            message : orderedDetailsErr,
                            //check : 1
                        });
                    }

                    //If ordered items are more than 1

                        else if((orderedDetailsResult[0].products_id.indexOf(',')) > -1 && (orderedDetailsResult[0].qty.indexOf(',') > -1)) {
                            var getOrderedProductsArr = orderedDetailsResult[0].products_id.split(','); //Split Ordered Products into Array
                            var getOrderedQtyArr = orderedDetailsResult[0].qty.split(',');  //Split Ordered Qty into Array

                            // Get Products and available_qty from inventory
                            var inventoryDetailsQuery = "SELECT inventory_id,products_id,available_qty FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
                            connection.query(inventoryDetailsQuery, function(inventoryDetailsErr, inventoryDetailsResult) {
                                if(inventoryDetailsErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : inventoryDetailsErr,
                                        //check : 2
                                    });
                                }

                                //Deduction Process
                                else {
                                    var getInventoryProductsArr = inventoryDetailsResult[0].products_id.split(','); //Split Inventory products into Array 
                                    var getInventoryQtyArr = inventoryDetailsResult[0].available_qty.split(',');    //Split Inventory available_qty into Array
                                    getOrderedProductsArr.forEach(function(item,index) { // For each ordered product deduct the ordered quantity from available quantity of each ordered product in inventory
                                        var inventoryProductIndex = getInventoryProductsArr.indexOf(item);  // Find index of each ordered item in inventory products
                                        var inventoryAvailableQty = parseInt(getInventoryQtyArr[inventoryProductIndex], 10);    //After finding product convert the corresponding quantity to integer
                                        var orderedQty = parseInt(getOrderedQtyArr[index], 10); //Convert the ordered product quantity to integer based on it's index
                                        inventoryAvailableQty = inventoryAvailableQty-orderedQty;   //Deduction from available_qty
                                        getInventoryQtyArr[inventoryProductIndex] = inventoryAvailableQty;    //Assign the modified qty to that place
                                    });
                                    var inventoryProductsStr = getInventoryProductsArr.join(',');   
                                    var inventoryQtyStr = getInventoryQtyArr.join(',');
                                    
                                    var updatedInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+inventoryProductsStr+"', available_qty = '"+inventoryQtyStr+"' WHERE merchant_id = "+merchant_id+"";
                                    connection.query(updatedInventoryQuery, function(updatedInventoryErr, updatedInventoryResult) {
                                        if(updatedInventoryErr) {
                                            res.status(200).send({
                                                error : true,
                                                message : updatedInventoryErr,
                                                //check : 3
                                            });
                                        }
                                        else if(updatedInventoryResult.changedRows != 0) {
                                            res.status(201).send({
                                                error : false,
                                                status : 'Inventory Modified',
                                                merchant_id : merchant_id,
                                                inventory_id : inventoryDetailsResult[0].inventory_id,
                                                products : inventoryProductsStr,
                                                available_qty : inventoryQtyStr,
                                                //check : 4
                                            });
                                        }
                                    });
                                }
                            });       
                        }

                        //If ordered item is only 1
                        else if((orderedDetailsResult[0].products_id.indexOf(',') == -1) && (orderedDetailsResult[0].qty.indexOf(',') == -1)) {
                            var singleProductInventoryDetailsQuery = "SELECT inventory_id,products_id,available_qty FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
                            connection.query(singleProductInventoryDetailsQuery, function(singleProductInventoryErr, singleProductInventoryResult) {
                                if(singleProductInventoryErr) {
                                    res.status(200).send({
                                        error : false,
                                        message : singleProductInventoryErr,
                                        //check : 5
                                    });
                                }

                                //Deduction Process
                                else {
                                    var getInventoryProducts_Arr = singleProductInventoryResult[0].products_id.split(',');  //Split Inventory Products into array in inventory
                                    var getInventoryQty_Arr = singleProductInventoryResult[0].available_qty.split(','); //Split available quantity into array in inventory
                                    var orderedProductInventoryIndex = getInventoryProducts_Arr.indexOf(orderedDetailsResult[0].products_id);   //Get index of the ordered product in inventory
                                    var orderedQty_AvailabilityInInventory = parseInt(getInventoryQty_Arr[orderedProductInventoryIndex], 10);   //Convert the corresponding qty in inventory to integer 
                                    var orderedQtyInt = parseInt(orderedDetailsResult[0].qty);  //Convert the ordered qty to integer
                                    orderedQty_AvailabilityInInventory = orderedQty_AvailabilityInInventory-orderedQtyInt;  //Deduction
                                    getInventoryQty_Arr[orderedProductInventoryIndex] = orderedQty_AvailabilityInInventory; //Replace the modified qty to that place

                                    var singleInventoryProductsStr = getInventoryProducts_Arr.join(',');   
                                    var singleInventoryQtyStr = getInventoryQty_Arr.join(',');
                                    
                                    var singleUpdatedInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+singleInventoryProductsStr+"', available_qty = '"+singleInventoryQtyStr+"' WHERE merchant_id = "+merchant_id+"";
                                    connection.query(singleUpdatedInventoryQuery, function(singleUpdatedInventoryErr, singleUpdatedInventoryResult) {
                                        if(singleUpdatedInventoryErr) {
                                            res.status(200).send({
                                                error : true,
                                                message : singleUpdatedInventoryErr,
                                                //check : 7
                                            });
                                        }
                                        else if(singleUpdatedInventoryResult.changedRows != 0) {
                                            res.status(201).send({
                                                error : false,
                                                status : 'Order Delivered and Inventory Modified',
                                                merchant_id : merchant_id,
                                                inventory_id : singleProductInventoryResult[0].inventory_id,
                                                products : singleInventoryProductsStr,
                                                available_qty : singleInventoryQtyStr,
                                                //check : 8
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    
                });
        }
        else {
            res.status(200).send({
                error : true,
                message : "Problem in updating inventory"
            });
        }
    });

});


// Get Orders for a particular merchant and his order states under one roof

router.get('/getMerchantReports/', function(req, res) {

    //Required Params
    var merchantId = req.query.merchantId;
    total_orders = 0;
    pending_orders = 0;
    inprogress_orders = 0;
    completed_orders = 0;
    cancelled_orders = 0;
    declined_orders = 0;
    returned_orders = 0;

    // To get count of total_orders placed with a merchant
    var totalOrdersQuery = "SELECT COUNT(order_id) AS total_orders FROM orders WHERE merchant_id = "+merchantId+"";
    connection.query(totalOrdersQuery, function(totalOrdersErr, totalOrdersResult) {
        if(totalOrdersErr) {
            res.status(200).send({
                error : true,
                message : totalOrdersErr
            });
        }
        else if(totalOrdersResult.length != 0) {
            total_orders = totalOrdersResult[0].total_orders;
        }
    });

    // To get count of pending orders in 'Wating for Merchant Approval' and 'Delayed' State
    var pendingOrdersQuery = "SELECT COUNT(order_id) AS pending_orders FROM orders WHERE (merchant_id = "+merchantId+" AND (order_status = 1 OR order_status = 9))";
    connection.query(pendingOrdersQuery, function(pendingOrdersErr, pendingOrdersResult) {
        if(pendingOrdersErr) {
            res.status(200).send({
                error : true,
                message : pendingOrdersErr
            });
        }
        else if(pendingOrdersResult.length != 0) {
            pending_orders = pendingOrdersResult[0].pending_orders;
        }
    });

    // To get count of inProgress orders in 'Approved and Processing' and 'Ready to Deliver' State
    var inProgressOrdersQuery = "SELECT COUNT(order_id) AS inprogress_orders FROM orders WHERE (merchant_id = "+merchantId+" AND (order_status = 2 OR order_status = 3))";
    connection.query(inProgressOrdersQuery, function(inProgressOrdersErr, inProgressOrdersResult) {
        if(inProgressOrdersErr) {
            res.status(200).send({
                error : true,
                message : inProgressOrdersErr
            });
        }
        else if(inProgressOrdersResult.length != 0) {
            inprogress_orders = inProgressOrdersResult[0].inprogress_orders;
        }
    });

    // To get count of completed orders in 'Delivered' state
    var completedOrdersQuery = "SELECT COUNT(order_id) AS completed_orders FROM orders WHERE (merchant_id = "+merchantId+" AND order_status = 4)";
    connection.query(completedOrdersQuery, function(completedOrdersErr, completedOrdersResult) {
        if(completedOrdersErr) {
            res.status(200).send({
                error : true,
                message : completedOrdersErr
            });
        }
        else if(completedOrdersResult.length != 0) {
            completed_orders = completedOrdersResult[0].completed_orders;
        }
    });

    // To get count of cancelled orders in 'Cancelled by User' state
    var cancelledOrdersQuery = "SELECT COUNT(order_id) AS cancelled_orders FROM orders WHERE (merchant_id = "+merchantId+" AND order_status = 5)";
    connection.query(cancelledOrdersQuery, function(cancelledOrdersErr, cancelledOrdersResult) {
        if(cancelledOrdersErr) {
            res.status(200).send({
                error : true,
                message : cancelledOrdersErr
            });
        }
        else if(cancelledOrdersResult.length != 0) {
            cancelled_orders = cancelledOrdersResult[0].cancelled_orders;
        }
    });

    // To get count of declined orders in 'Declined by Merchant' state
    var declinedOrdersQuery = "SELECT COUNT(order_id) AS declined_orders FROM orders WHERE (merchant_id = "+merchantId+" AND order_status = 6)";
    connection.query(declinedOrdersQuery, function(declinedOrdersErr, declinedOrdersResult) {
        if(declinedOrdersErr) {
            res.status(200).send({
                error : true,
                message : declinedOrdersErr
            });
        }
        else if(declinedOrdersResult.length != 0) {
            declined_orders = declinedOrdersResult[0].declined_orders;
        }
    });

    // To get count of returned orders in 'Request to Return' and 'Returned' State
    var returnedOrdersQuery = "SELECT COUNT(order_id) AS returned_orders FROM orders WHERE (merchant_id = "+merchantId+" AND (order_status = 7 OR order_status = 8))";
    connection.query(returnedOrdersQuery, function(returnedOrdersErr, returnedOrdersResult) {
        if(returnedOrdersErr) {
            res.status(200).send({
                error : true,
                message : returnedOrdersErr
            });
        }
        else if(returnedOrdersResult.length != 0) {
            returned_orders = returnedOrdersResult[0].returned_orders;
        }
    });

    setTimeout(function() {
        res.status(200).send({
            error : false,
            reports : {
                total_orders : total_orders,
                pending_orders : pending_orders,
                inprogress_orders : inprogress_orders,
                completed_orders : completed_orders,
                cancelled_orders : cancelled_orders,
                declined_orders : declined_orders,
                returned_orders : returned_orders
            }
        });
    }, 5000);

});




// Get Order through Order Id

router.get('/getOrderById/', function(req, res) {

    var orderId = req.query.orderId;
    var orderQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status WHERE orders.order_id = "+orderId+"";

    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult != null && orderResult.length != 0) {

            //Fetch Product Details
            
            //If ordered products is more than 1
            if(orderResult[0].products_id.indexOf(',') != -1) {

                var orderedProductArr = orderResult[0].products_id.split(',');
                var orderedQtyArr = orderResult[0].qty.split(',');
                var orderedProductsPriceArr = orderResult[0].price.split(',');
                var orderedProductsTotalPriceArr = orderResult[0].total_price.split(',');

                var products = new Array();

                orderedProductArr.forEach(function(item, index) {
                    var product_id = parseInt(item, 10);
                    var productDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+product_id+"";
                    connection.query(productDetailsQuery, function(productDetailsErr, productDetailsResult) {
                        if(productDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : productDetailsErr
                            });
                        }
                        else {
                            temp_arr = {
                                product_id : product_id,
                                product_name : productDetailsResult[0].product_name,
                                product_description : productDetailsResult[0].product_description,
                                sku : productDetailsResult[0].sku,
                                quantification : productDetailsResult[0].quantification,
                                ordered_qty : orderedQtyArr[index],
                                price : orderedProductsPriceArr[index],
                                total_price : orderedProductsTotalPriceArr[index]
                            };
                            module.exports.orderedProducts(temp_arr);
                        }
                    }); 
                });
                var otherInfoQuery = "SELECT merchants.shop_name,feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                    connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                    if(otherInfoErr) {
                        res.status(200).send({
                            error : true,
                            message : otherInfoErr,
                        });
                    }
                    else {
                        products = module.exports.orderedProducts(1);
                        setTimeout(function() {
                            module.exports.orderedProducts(0);
                            res.status(200).send({
                                error : false,
                                order : {
                                    order_id : orderResult[0].order_id,
                                    user_id : orderResult[0].user_id,
                                    merchant_id : orderResult[0].merchant_id,
                                    shop_name : otherInfoResult[0].shop_name,
                                    products : products,
                                    taxes : orderResult[0].taxes,
                                    bill_amount : orderResult[0].bill_amount,
                                    tracking_id : orderResult[0].tracking_id,
                                    order_time : orderResult[0].order_time,
                                    collecting_time : orderResult[0].collecting_time,
                                    payment_mode : orderResult[0].payment_mode,
                                    payment_status : orderResult[0].payment_status,
                                    transaction_id : orderResult[0].transaction_id,
                                    merchant_approved : orderResult[0].merchant_approved,
                                    order_status : orderResult[0].order_status,
                                    order_status_name : orderResult[0].order_status_name,
                                    rating : otherInfoResult[0].rating,
                                    review : otherInfoResult[0].review
                                }
                            });
                        }, 2000);
                    }
                });
            }

            // If order contains only 1 item
            else if(orderResult[0].products_id.indexOf(',') == -1) {
                var singleProduct_id = parseInt(orderResult[0].products_id);
                var singleProductDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+singleProduct_id+"";
                    connection.query(singleProductDetailsQuery, function(singleproductDetailsErr, singleproductDetailsResult) {
                        if(singleproductDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : singleproductDetailsErr
                            });
                        }
                        else {
                            temp_arr = [{
                                product_id : singleProduct_id,
                                product_name : singleproductDetailsResult[0].product_name,
                                product_description : singleproductDetailsResult[0].product_description,
                                sku : singleproductDetailsResult[0].sku,
                                quantification : singleproductDetailsResult[0].quantification,
                                ordered_qty : orderResult[0].qty,
                                price : orderResult[0].price,
                                total_price : orderResult[0].total_price
                            }];

                            var otherInfoQuery = "SELECT merchants.shop_name,feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                            connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                                if(otherInfoErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : otherInfoErr
                                    });
                                }
                                else {
                                    res.status(200).send({
                                        error : false,
                                        order : {
                                            order_id : orderResult[0].order_id,
                                            user_id : orderResult[0].user_id,
                                            merchant_id : orderResult[0].merchant_id,
                                            shop_name : otherInfoResult[0].shop_name,
                                            products : temp_arr,
                                            taxes : orderResult[0].taxes,
                                            bill_amount : orderResult[0].bill_amount,
                                            tracking_id : orderResult[0].tracking_id,
                                            order_time : orderResult[0].order_time,
                                            collecting_time : orderResult[0].collecting_time,
                                            payment_mode : orderResult[0].payment_mode,
                                            payment_status : orderResult[0].payment_status,
                                            transaction_id : orderResult[0].transaction_id,
                                            merchant_approved : orderResult[0].merchant_approved,
                                            order_status : orderResult[0].order_status,
                                            order_status_name : orderResult[0].order_status_name,
                                            rating : otherInfoResult[0].rating,
                                            review : otherInfoResult[0].review
                                        }
                                    });
                                }
                            });
                        }
                    }); 

            } 
        }
        else if(orderResult == null || orderResult.length == 0) {
            res.status(200).send({
                error : false,
                message : 'No Orders found'
            });
        }
    });

});


// Get Order through Order Id Revised

router.get('/getOrderByIdRevised/', function(req, res) {

    var orderId = req.query.orderId;
    var orderQuery = "SELECT users.name as user_name, orders.*, order_status.order_status_name FROM orders, order_status, users WHERE order_status.order_status_id = orders.order_status AND orders.user_id=users.user_id AND orders.order_id = "+orderId+"";

    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult != null && orderResult.length != 0) {

            //Fetch Product Details
            
            //If ordered products is more than 1
            if(orderResult[0].products_id.indexOf(',') != -1) {

                var orderedProductArr = orderResult[0].products_id.split(',');
                var orderedQtyArr = orderResult[0].qty.split(',');
                var orderedProductsPriceArr = orderResult[0].price.split(',');
                var orderedProductsTotalPriceArr = orderResult[0].total_price.split(',');

                var products = new Array();

                orderedProductArr.forEach(function(item, index) {
                    var product_id = parseInt(item, 10);
                    var productDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+product_id+"";
                    connection.query(productDetailsQuery, function(productDetailsErr, productDetailsResult) {
                        if(productDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : productDetailsErr
                            });
                        }
                        else {
                            temp_arr = {
                                product_id : product_id,
                                product_name : productDetailsResult[0].product_name,
                                product_description : productDetailsResult[0].product_description,
                                sku : productDetailsResult[0].sku,
                                quantification : productDetailsResult[0].quantification,
                                ordered_qty : orderedQtyArr[index],
                                price : orderedProductsPriceArr[index],
                                total_price : orderedProductsTotalPriceArr[index]
                            };
                            module.exports.orderedProducts(temp_arr);
                        }
                    }); 
                });
                var otherInfoQuery = "SELECT merchants.shop_name,feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                    connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                    if(otherInfoErr) {
                        res.status(200).send({
                            error : true,
                            message : otherInfoErr,
                        });
                    }
                    else {
                        products = module.exports.orderedProducts(1);
                        setTimeout(function() {
                            module.exports.orderedProducts(0);
                            res.status(200).send({
                                error : false,
                                order : {
                                    order_id : orderResult[0].order_id,
                                    user_id : orderResult[0].user_id,
                                    user_name : orderResult[0].user_name,
                                    merchant_id : orderResult[0].merchant_id,
                                    shop_name : otherInfoResult[0].shop_name,
                                    products : products,
                                    taxes : orderResult[0].taxes,
                                    bill_amount : orderResult[0].bill_amount,
                                    tracking_id : orderResult[0].tracking_id,
                                    order_time : orderResult[0].order_time,
                                    collecting_time : orderResult[0].collecting_time,
                                    payment_mode : orderResult[0].payment_mode,
                                    payment_status : orderResult[0].payment_status,
                                    transaction_id : orderResult[0].transaction_id,
                                    merchant_approved : orderResult[0].merchant_approved,
                                    order_status : orderResult[0].order_status,
                                    order_status_name : orderResult[0].order_status_name,
                                    rating : otherInfoResult[0].rating,
                                    review : otherInfoResult[0].review
                                }
                            });
                        }, 2000);
                    }
                });
            }

            // If order contains only 1 item
            else if(orderResult[0].products_id.indexOf(',') == -1) {
                var singleProduct_id = parseInt(orderResult[0].products_id);
                var singleProductDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+singleProduct_id+"";
                    connection.query(singleProductDetailsQuery, function(singleproductDetailsErr, singleproductDetailsResult) {
                        if(singleproductDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : singleproductDetailsErr
                            });
                        }
                        else {
                            temp_arr = [{
                                product_id : singleProduct_id,
                                product_name : singleproductDetailsResult[0].product_name,
                                product_description : singleproductDetailsResult[0].product_description,
                                sku : singleproductDetailsResult[0].sku,
                                quantification : singleproductDetailsResult[0].quantification,
                                ordered_qty : orderResult[0].qty,
                                price : orderResult[0].price,
                                total_price : orderResult[0].total_price
                            }];

                            var otherInfoQuery = "SELECT merchants.shop_name,feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                            connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                                if(otherInfoErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : otherInfoErr
                                    });
                                }
                                else {
                                    res.status(200).send({
                                        error : false,
                                        order : {
                                            order_id : orderResult[0].order_id,
                                            user_id : orderResult[0].user_id,
                                            user_name : orderResult[0].user_name,
                                            merchant_id : orderResult[0].merchant_id,
                                            shop_name : otherInfoResult[0].shop_name,
                                            products : temp_arr,
                                            taxes : orderResult[0].taxes,
                                            bill_amount : orderResult[0].bill_amount,
                                            tracking_id : orderResult[0].tracking_id,
                                            order_time : orderResult[0].order_time,
                                            collecting_time : orderResult[0].collecting_time,
                                            payment_mode : orderResult[0].payment_mode,
                                            payment_status : orderResult[0].payment_status,
                                            transaction_id : orderResult[0].transaction_id,
                                            merchant_approved : orderResult[0].merchant_approved,
                                            order_status : orderResult[0].order_status,
                                            order_status_name : orderResult[0].order_status_name,
                                            rating : otherInfoResult[0].rating,
                                            review : otherInfoResult[0].review
                                        }
                                    });
                                }
                            });
                        }
                    }); 

            } 
        }
        else if(orderResult == null || orderResult.length == 0) {
            res.status(200).send({
                error : false,
                message : 'No Orders found'
            });
        }
    });

});




// Get Order through Tracking Id
router.get('/getOrderByTrackingId/', function(req, res) {

    var trackingId = req.query.trackingId;
    var orderQuery = "SELECT users.name as user_name, users.user_img, orders.*, order_status.order_status_name FROM orders, order_status, users WHERE order_status.order_status_id = orders.order_status AND orders.user_id=users.user_id AND orders.tracking_id = 'PYG#ID"+trackingId+"'";
    // var orderQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status WHERE orders.tracking_id = 'PYG#ID"+trackingId+"'";

    connection.query(orderQuery, function(orderErr, orderResult) {
        setTimeout(function() {
            if(orderErr) {
                res.status(200).send({
                    error : true,
                    message : orderErr
                });
            }
            else if(orderResult != null && orderResult.length != 0) {
                //Fetch Product Details
                
                //If ordered products is more than 1
                if(orderResult[0].products_id.indexOf(',') != -1) {

                    var orderedProductArr = orderResult[0].products_id.split(',');
                    var orderedQtyArr = orderResult[0].qty.split(',');
                    var orderedProductsPriceArr = orderResult[0].price.split(',');
                    var orderedProductsTotalPriceArr = orderResult[0].total_price.split(',');

                    var products = new Array();

                    orderedProductArr.forEach(function(item, index) {
                        var product_id = parseInt(item, 10);
                        var productDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+product_id+"";
                        connection.query(productDetailsQuery, function(productDetailsErr, productDetailsResult) {
                            if(productDetailsErr) {
                                res.status(200).send({
                                    error : true,
                                    message : productDetailsErr
                                });
                            }
                            else {
                                temp_arr = {
                                    product_id : product_id,
                                    product_name : productDetailsResult[0].product_name,
                                    product_description : productDetailsResult[0].product_description,
                                    sku : productDetailsResult[0].sku,
                                    quantification : productDetailsResult[0].quantification,
                                    ordered_qty : orderedQtyArr[index],
                                    price : orderedProductsPriceArr[index],
                                    total_price : orderedProductsTotalPriceArr[index]
                                };
                                module.exports.orderedProducts(temp_arr);
                            }
                        }); 
                    });
                    var otherInfoQuery = "SELECT merchants.shop_name, feedback.rating, feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                    connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                        if(otherInfoErr) {
                            res.status(200).send({
                                error : true,
                                message : otherInfoErr,
                            });
                        }
                        else {
                            products = module.exports.orderedProducts(1);
                            setTimeout(function() {
                                module.exports.orderedProducts(0);
                                res.status(200).send({
                                    error : false,
                                    order : {
                                        order_id : orderResult[0].order_id,
                                        user_id : orderResult[0].user_id,
                                        user_name : orderResult[0].user_name,
                                        user_img : orderResult[0].user_img,
                                        merchant_id : orderResult[0].merchant_id,
                                        shop_name : otherInfoResult[0].shop_name,
                                        products : products,
                                        taxes : orderResult[0].taxes,
                                        bill_amount : orderResult[0].bill_amount,
                                        tracking_id : orderResult[0].tracking_id,
                                        order_time : orderResult[0].order_time,
                                        collecting_time : orderResult[0].collecting_time,
                                        payment_mode : orderResult[0].payment_mode,
                                        payment_status : orderResult[0].payment_status,
                                        transaction_id : orderResult[0].transaction_id,
                                        merchant_approved : orderResult[0].merchant_approved,
                                        order_status : orderResult[0].order_status,
                                        order_status_name : orderResult[0].order_status_name,
                                        rating : otherInfoResult[0].rating,
                                        review : otherInfoResult[0].review
                                    }
                                });
                            }, 2000);
                        }
                    });
                }

                // If order contains only 1 item
                else if(orderResult[0].products_id.indexOf(',') == -1) {
                    var singleProduct_id = parseInt(orderResult[0].products_id);
                    var singleProductDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+singleProduct_id+"";
                        connection.query(singleProductDetailsQuery, function(singleproductDetailsErr, singleproductDetailsResult) {
                            if(singleproductDetailsErr) {
                                res.status(200).send({
                                    error : true,
                                    message : singleproductDetailsErr
                                });
                            }
                            else {
                                temp_arr = [{
                                    product_id : singleProduct_id,
                                    product_name : singleproductDetailsResult[0].product_name,
                                    product_description : singleproductDetailsResult[0].product_description,
                                    sku : singleproductDetailsResult[0].sku,
                                    quantification : singleproductDetailsResult[0].quantification,
                                    ordered_qty : orderResult[0].qty,
                                    price : orderResult[0].price,
                                    total_price : orderResult[0].total_price
                                }];

                                var otherInfoQuery = "SELECT merchants.shop_name, feedback.rating, feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                                connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                                    if(otherInfoErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : otherInfoErr,
                                        });
                                    }
                                    else {
                                        res.status(200).send({
                                            error : false,
                                            order : {
                                                order_id : orderResult[0].order_id,
                                                user_id : orderResult[0].user_id,
                                                user_name : orderResult[0].user_name,
                                                user_img : orderResult[0].user_img,
                                                merchant_id : orderResult[0].merchant_id,
                                                shop_name : otherInfoResult[0].shop_name,
                                                products : temp_arr,
                                                taxes : orderResult[0].taxes,
                                                bill_amount : orderResult[0].bill_amount,
                                                tracking_id : orderResult[0].tracking_id,
                                                order_time : orderResult[0].order_time,
                                                collecting_time : orderResult[0].collecting_time,   
                                                payment_mode : orderResult[0].payment_mode,
                                                payment_status : orderResult[0].payment_status,
                                                transaction_id : orderResult[0].transaction_id,
                                                merchant_approved : orderResult[0].merchant_approved,
                                                order_status : orderResult[0].order_status,
                                                order_status_name : orderResult[0].order_status_name,
                                                rating : otherInfoResult[0].rating,
                                                review : otherInfoResult[0].review
                                            }
                                        });
                                    }
                                });
                            }
                        }); 
                } 
            }
            else if(orderResult == null || orderResult.length == 0) {
                res.status(200).send({
                    error : false,
                    message : 'No Orders found'
                });
            }
        }, 2000);
    });
    
});



// Rating Calculation for Merchants

router.get('/getMerchantRating/', function(req, res) {

    // Required Params
    var merchantId = req.query.merchantId;

    // Select feedbacks for that particular merchant
    var selectFeedbacksQuery = "SELECT feedback.*, merchants.shop_name FROM feedback LEFT JOIN merchants ON merchants.merchant_id = feedback.merchant_id WHERE feedback.merchant_id = "+merchantId+"";
    connection.query(selectFeedbacksQuery, function(selectFeedbacksErr, selectFeedbacksResult) {
        if(selectFeedbacksErr) {
            res.status(200).send({
                error : true,
                message : selectFeedbacksErr
            });
        }
        else if(selectFeedbacksResult.length != 0) {

            // Overall Feedback Count
            var overAllFeedbackCount = selectFeedbacksResult.length;
            overAllRating = 0;
            overAllRatingsTotal = 0;

            // Calculate the sum of overall ratings
            selectFeedbacksResult.forEach(function(item) {
                overAllRatingsTotal += item.rating;
            });

            // Calculate overAll ratings
            overAllRating = overAllRatingsTotal/overAllFeedbackCount;

            // Response
            res.status(200).send({
                error : false,
                merchant_id : merchantId,
                shop_name : selectFeedbacksResult[0].shop_name,
                overAllRating : overAllRating
            });
        }
        else if(selectFeedbacksResult.length == 0) {
            res.status(200).send({
                error : false,
                merchant_id : merchantId,
                shop_name : selectFeedbacksResult[0].shop_name,
                overAllRating : 0.0
            });
        }
    });

});


// Set merchant Email Address from reviewOrder and delivery
var merchant_email;
function setMerchantEmail(email) {
    
    if(email == 1) {
        return merchant_email;
    }
    else if(email == 0) {
        merchant_email = "";
    }
    else {
        merchant_email = email;
    }
}

// Set user Email Address from reviewOrder and delivery
var user_email;
function setUserEmail(email) {
    
    if(email == 1) {
        return user_email;
    }
    else if(email == 0) {
        user_email = "";
    }
    else {
        user_email = email;
    }
}


// Registering routes to our API's
// all routes will be prefixed with /pyg/merchant
app.use('/pyg/merchant', router);

// Start the server
app.listen(port);

},


// Get Categories selling by merchants in their shop by passing Merchant Id(Used in API named 'getMerchantCategories')

categoriesWithMerchant : new Array(),
getcategoriesByMerchant : function(categories) {

    if(categories == 1) {
        return module.exports.categoriesWithMerchant;
    }
    else if(categories == 0) {
        module.exports.categoriesWithMerchant = [];
    }
    else {
        module.exports.categoriesWithMerchant.push(categories);
    }

},


// Set Product Details passed from query function of 'getMerchantProducts' API

productsWithMerchant : new Array(),
setProductDetails : function(products) {

    if(products == 1) {
        return module.exports.productsWithMerchant;
    }
    else if(products == 0) {
        module.exports.productsWithMerchant = [];
    }
    else {
        module.exports.productsWithMerchant.push(products);
    }
},


// 'addInventoryToMerchant' API - Catalogue Approval

catalogueApproval : 0,
setCatalogueApproval : function(value) {

    if(value == 'get') {
        return module.exports.catalogueApproval;
    }
    else if(value == 0) {
        module.exports.catalogueApproval = 0;
    }
    else {
        module.exports.catalogueApproval = 1;
    }
},




// 'addInventoryToMerchant' API - ProductAdded

productAdded : 0,
setProductAdded : function(value) {

    if(value == 'get') {
        return module.exports.productAdded;
    }
    else if(value == 0) {
        module.exports.productAdded = 0;
    }
    else {
        module.exports.productAdded = 1;
    }
},




// Get product details for ordered items
orderedProductDetails : [],
orderedProducts : function(product) {

    if(product == 1) {
        return module.exports.orderedProductDetails;
    }
    else if(product == 0) {
        module.exports.orderedProductDetails = [];
    }
    else {
        module.exports.orderedProductDetails.push(product);
    }

},




// Encryption of passwords

encrypt : function(data) {
    const crypted = cryptr.encrypt(data);
    return crypted;
},




// Decryption of passwords

decrypt : function(data) {
    var decrypted = cryptr.decrypt(data);
    return decrypted;
},




// Generating OTP for Mail Verification

otpGenerator : function() {
    var otp = Math.floor(1000 + Math.random() * 9000);
    return otp;
}

}