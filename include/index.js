// PYG v1.0

// Required Modules and Files
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('secretkey');
const Geo = require('geo-nearby');
const randomstring = require('randomstring');
const Excel = require('xlsx');
const request = require('request');
const aws4 = require('aws4');
const asn1 = require('asn1');
const cors = require('cors');
const objectAssign = require('object-assign');
const Mail = require('./include/SendMail.js');
const Notify = require('./include/Notifications.js');
const Merchant = require('./include/merchant.js');
const Dashboard = require('./include/dashboard.js');
const DbConnect = require('./include/DbConnect.js');
const connection = new DbConnect();

//URL Encoding
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

//Access Control Mechanisms (Dashboard Access)
app.use(cors());

//Routing API's
var port = process.env.PORT || 8080;
var router = express.Router();

//Connecting Merchant App API's
Merchant.merchantApp();

//Connecting Dashboard API's
Dashboard.dashboard();





//API's in PYG USER as follows

// Dummy API just to check notifications

router.post('/notify', function(req, res) {

    var registrationToken = req.body.registrationToken;
    var title = req.body.title;
    var content = req.body.content;

    setTimeout(function() {
    var send = Notify.userNotificationAndroid(registrationToken,title,content,{order_id : 5});
        if(send == 1) {
            res.status(200).send({
                error : false,
                status : "Notification Sent"
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : send
            });
        }
    }, 5000);

});




// Registration and Verification API's for Consumers

//Registration

router.post('/register', function(req, res) {

    var email = req.body.email;
    var user_type = req.body.user_type;
    var otp = otpGenerator();

    var query = "SELECT email,user_type FROM email_verify";
    connection.query(query, function(err, result, existing) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else {
            for(var i = 0; i < result.length; i++) {
                if(result[i].email == email && result[i].user_type == user_type) {
                    existing = 1;
                    break;
                }
            }
            if(existing == 1) {
                res.status(200).send({
                    error : true,
                    message : 'User already exists',
                });
            }
            else {
                var html = "<p>‘Your OTP for PYG Registration is : '"+otp+"'</p><br><br><strong>Thanks for registering with PYG</strong>";
                Mail.sendMail(email, 'PYG Verification', html);
                var query = "INSERT INTO email_verify(email,user_type,otp,status) VALUES('"+email+"' , '"+user_type+"' , '"+otp+"' , 0)";
                connection.query(query, function(err, result) {
                    if(err) {
                        res.status(200).send({
                            error : true,
                            message : err
                        });
                    }
                    else if(result.affectedRows != 0) {                  
                        res.status(201).send({
                            error : false,
                            email : email,
                            user_type : user_type,
                            status : 'OTP has been sent to your mail'
                        });
                    }
                });
            }
        }
    });

});




// Email Verification

router.post('/verify', function(req, res) {

    var entered_otp = req.body.entered_otp;
    var email = req.body.email;
    var quoted_email = "'"+email+"'";

    var query = "SELECT otp FROM email_verify WHERE email LIKE "+quoted_email+" AND user_type LIKE 'Consumer'";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(201).send({
                error : true,
                message : err
            });
        }
        else if(result[0].otp == entered_otp) {
            var update_query = "UPDATE email_verify SET status = 1 WHERE otp = "+entered_otp+"";
            connection.query(update_query, function(updateError, updateResult) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : updateError
                    });
                }
                else if(updateResult.affectedRows != 0) {
                    res.status(201).send({
                        error : false,
                        message : 'Verified'
                    });
                }
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Incorrect OTP',
            });
        }
    });

});




// Creating User Entry after verification

router.post('/insertUser', function(req, res) {

    var name = req.body.name;
    var email = req.body.email;
    var password = req.body.password;
    var mobile_num = req.body.mobile_num;
    var user_img = req.body.user_img;
    var os_type = req.body.os_type;
    var fcm_id = req.body.fcm_id;
    var encrypted_password = encrypt(password);
    var quoted_email = "'"+email+"'";

    var query = "SELECT status FROM email_verify WHERE email = "+quoted_email+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length == 0) {
            res.status(200).send({
                error : true,
                message : "User doesn't Exists"
            });
        }
        else if(result[0].status == 1) {
            var checkUserEntry_query = "SELECT email FROM users";
            connection.query(checkUserEntry_query, function(checkUserEntryErr, checkUserEntryResult, existing) {
                if(checkUserEntryErr) {
                    res.status(200).send({
                        error : true,
                        message : checkUserEntryErr
                    });
                }
                else {
                    for(var i = 0; i < checkUserEntryResult.length; i++) {
                        if(checkUserEntryResult[i].email == email) {
                            existing = 1;
                            break;
                        }
                    }
                    if(existing == 1) {
                        res.status(200).send({
                            error : true,
                            message : 'User already registered',
                        });
                    }
                    else {
                        var insert_query = "INSERT INTO users(name,email,password,mobile_num,user_img,os_type,fcm_id) VALUES('"+name+"' , '"+email+"' , '"+encrypted_password+"' , '"+mobile_num+"' , '"+user_img+"' , '"+os_type+"', '"+fcm_id+"')";
                        connection.query(insert_query, function(insertErr, insertResult) {
                            if(insertErr) {
                                res.status(200).send({
                                    error : true,
                                    message : insertErr
                                });
                            }
                            else if(insertResult.affectedRows != 0) {
                                var cart_insertQuery = "INSERT INTO cart(user_id) VALUES('"+insertResult.insertId+"')";
                                connection.query(cart_insertQuery, function(cartinsertErr, cartinsertResult) {
                                    if(cartinsertErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : cartinsertErr
                                        });
                                    }
                                    else {
                                        res.status(201).send({
                                            error : false,
                                            status : "Thanks for registering with PYG",
                                            user_details : {
                                                user_id : insertResult.insertId,
                                                name : name,
                                                email : email,
                                                mobile_num : mobile_num,
                                                user_img : user_img,
                                                os_type : os_type,
                                                fcm_id : fcm_id,
                                                blocked : 0,
                                                cart_id : cartinsertResult.insertId,
                                                merchant_in_cart : null,
                                                productsCountInCart : 0
                                            }
                                        });
                                    }
                                });        
                            }
                        });
                    }
                }
            });
        }
        else if(result[0].status == 0) {
            res.status(200).send({
                error : true,
                message : 'Complete your email verification'
            });
        }
    });
 
});




// Get All User Details

router.get('/getAllUsers', function(req, res) {

    var query = "SELECT * FROM users";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                users : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No users found"
            });
        }
    });

});




// Get User by Id

router.get('/getUserById/', function(req, res) {

    var userId = req.query.userId;

    var query = "SELECT * FROM users WHERE user_id = "+userId+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                user_details : result[0]
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'User not found'
            });
        }
    });

});




// Get User by Name

router.get('/getUserByName/', function(req, res) {

    var name = req.query.name;

    var query = "SELECT * FROM users WHERE name LIKE CONCAT('"+name+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                user_details : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'User not found'
            });
        }
    });

});




// Get User by Email

router.get('/getUserByEmail/', function(req, res) {

    var email = req.query.email;

    var query = "SELECT * FROM users WHERE email LIKE '"+email+"'";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                user_details : result[0],
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'User not found',
            });
        }
    });

});




// Update user profile

router.post('/updateUserProfile', function(req, res) {

    var user_id = req.body.user_id;
    var name = req.body.name;
    var email = req.body.email;
    var user_img = req.body.user_img;
    var mobile_num = req.body.mobile_num;
    var os_type = req.body.os_type;

    // Convert to String
    var quoted_name = "'"+name+"'";
    var quoted_userImg = "'"+user_img+"'";
    var quoted_mobileNum = "'"+mobile_num+"'"
    var quoted_osType = "'"+os_type+"'";

    var query = "UPDATE users SET name = "+quoted_name+" , user_img = "+quoted_userImg+" , mobile_num = "+quoted_mobileNum+" , os_type = "+quoted_osType+" WHERE user_id = "+user_id+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.affectedRows != 0) {
            var fetchDetailsQuery = "SELECT users.os_type,users.fcm_id,users.blocked,cart.cart_id FROM users LEFT JOIN cart ON users.user_id = cart.user_id WHERE users.user_id = "+user_id+"";
            connection.query(fetchDetailsQuery, function(fetchDetailsErr, fetchDetailsResult) {
                if(fetchDetailsErr) {
                    res.status(200).send({
                        error : true,
                        message : fetchDetailsErr,
                    });
                }
                else if(fetchDetailsResult.length != 0) {
                    res.status(201).send({
                        error : false,
                        status : 'Your Profile has been updated successfully',
                        user_details : {
                            user_id : user_id,
                            name : name,
                            email : email,
                            user_img : user_img,
                            mobile_num : mobile_num,
                            os_type : os_type,
                            fcm_id : fetchDetailsResult[0].fcm_id,
                            blocked : fetchDetailsResult[0].blocked,
                            cart_id : fetchDetailsResult[0].cart_id
                        }
                    });
                }
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Not updated',
            });
        }
    });

});




// Change User's Password

router.post('/changeUserPassword', function(req, res) {

    var user_id = req.body.user_id;
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;

    var pwdQuery = "SELECT password FROM users WHERE user_id = "+user_id+"";
    connection.query(pwdQuery, function(pwdErr, pwdResult) {
        if(pwdErr) {
            res.status(200).send({
                error : true,
                message : pwdErr,
            });
        }
        else if(pwdResult != null && pwdResult[0] != null) {
            var decryptedOldPassword = decrypt(pwdResult[0].password);
            if(decryptedOldPassword === old_password) {
                var encryptedNewPassword = encrypt(new_password);
                var updateQuery = "UPDATE users SET password = '"+encryptedNewPassword+"' WHERE user_id = "+user_id+"";
                connection.query(updateQuery, function(updateErr, updateResult) {
                    if(updateErr) {
                        res.status(200).send({
                            error : true,
                            message : updateErr,
                        });
                    }
                    else if(updateResult.changedRows != 0) {
                        res.status(201).send({
                            error : false,
                            status : "Updated with new password",
                        });
                    }
                })
            }
            else {
                res.status(200).send({
                    error : true,
                    message : "Incorrect Old Password",
                });
            }
        }
        else {
            res.status(200).send({
                error : true,
                message : "Incorrect details",
            });
        }
    });

});




// Forget User Password, for registered users

router.post('/forgotUserPassword', function(req, res) {

    var email = req.body.email;
    var user_type = req.body.user_type;
    var regeneratedOtp = otpGenerator();

    var emailVerifyQuery = "SELECT email,user_type,otp,status FROM email_verify WHERE email LIKE '"+email+"' AND user_type LIKE '"+user_type+"'";
    connection.query(emailVerifyQuery, function(emailVerifyErr, emailVerifyResult) {
        if(emailVerifyErr) {
            res.status(200).send({
                error : true,
                message : emailVerifyErr
            });
        }
        else if(emailVerifyResult.length == 0) {
            res.status(200).send({
                error : true,
                message : "No User Found..."
            });
        }
        else if(emailVerifyResult.length != 0) {
            if(emailVerifyResult[0].email == email && emailVerifyResult[0].user_type == user_type) {
                var updateStatusQuery = "UPDATE email_verify SET otp = "+regeneratedOtp+" , status = 0 WHERE email LIKE '"+email+"' AND user_type LIKE '"+user_type+"'";
                connection.query(updateStatusQuery, function(updateStatusErr, updateStatusResult) {
                    if(updateStatusErr) {
                        res.status(200).send({
                            error : true,
                            message : updateStatusErr
                        });
                    }
                    else if(updateStatusResult.affectedRows != 0) {
                        var html = "<p>‘Your OTP for PYG Re-Verification is : '"+regeneratedOtp+"'</p><br><br><strong>Thanks for registering with PYG</strong>";
                        Mail.sendMail(email, 'PYG Reset Password', html);
                        res.status(201).send({
                            error : false,
                            status : "OTP has been sent to registered email-id"
                        });
                    }
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    message : "User type mismatch",
                });
            }
        }
    });
});




// Reset User Password

router.post('/resetUserPassword', function(req, res) {

    var email = req.body.email;
    var new_password = req.body.new_password;

    var passwordResetStatusCheckQuery = "SELECT status FROM email_verify WHERE email LIKE '"+email+"' AND user_type LIKE 'Consumer'";
    connection.query(passwordResetStatusCheckQuery, function(passwordResetStatusCheckErr, passwordResetStatusCheckResult) {
        if(passwordResetStatusCheckErr) {
            res.status(200).send({
                error : true,
                message : passwordResetStatusCheckErr
            });
        }
        else if(passwordResetStatusCheckResult.length != 0) {
            var encryptedNewPassword = encrypt(new_password);
            if(passwordResetStatusCheckResult[0].status == 1) {
                var passwordResetQuery = "UPDATE users SET password = '"+encryptedNewPassword+"' WHERE email LIKE '"+email+"'";
                connection.query(passwordResetQuery, function(passwordResetErr, passwordResetResult) {
                    if(passwordResetErr) {
                        res.status(200).send({
                            error : true,
                            message : passwordResetErr
                        });
                    }
                    else if(passwordResetResult.affectedRows != 0) {
                        res.status(201).send({
                            error : false,
                            status : "Password altered"
                        });
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : "Problem in reset password"
                        });
                    }
                });
            }
            else if(passwordResetStatusCheckResult[0].status == 0) {
                res.status(200).send({
                    error : true,
                    message : "Complete your email verification"
                });
            }
        }
        else {
            res.status(200).send({
                error : true,
                message : "Error in fetching status"
            });
        }
    });
});




// User Login API, for registered users

router.post('/loginUser', function(req, res) {

    var email = req.body.email;
    var password = req.body.password;
    var quoted_email = "'"+email+"'";

    var search_query = "SELECT email,password FROM users WHERE email LIKE "+quoted_email+"";
    connection.query(search_query, function(searchErr, searchResult) {
        if(searchErr) {
            res.status(200).send({
                error : true,
                message : searchErr
            });
        }
        else if(searchResult.length != 0) {
            var decrypted_password = decrypt(searchResult[0].password);
            if(email == searchResult[0].email && password == decrypted_password) {
                var fetchDetails_query = "SELECT users.user_id,users.name,users.email,users.mobile_num,users.user_img,users.os_type,users.fcm_id,users.blocked,cart.cart_id,cart.merchant_id,cart.products_id FROM users LEFT JOIN cart ON users.user_id = cart.user_id WHERE users.email LIKE "+quoted_email+"";
                connection.query(fetchDetails_query, function(fetchDetailsErr, fetchDetailsResult) {
                    if(fetchDetailsErr) {
                        res.status(200).send({
                            error : true,
                            message : fetchDetailsErr
                        });
                    }
                    else {
                        if(fetchDetailsResult[0].blocked == 0) {
                            // Check for Cart
                            if(fetchDetailsResult[0].products_id != null && fetchDetailsResult[0].products_id != '' && fetchDetailsResult[0].products_id.indexOf(',') != -1) {
                                var productsInCartArr = fetchDetailsResult[0].products_id.split(',');
                                var productsCountInCart = productsInCartArr.length;
                                temp_arr = {
                                    user_id : fetchDetailsResult[0].user_id,
                                    name : fetchDetailsResult[0].name,
                                    email : fetchDetailsResult[0].email,
                                    mobile_num : fetchDetailsResult[0].mobile_num,
                                    user_img : fetchDetailsResult[0].user_img,
                                    os_type : fetchDetailsResult[0].os_type,
                                    fcm_id : fetchDetailsResult[0].fcm_id,
                                    blocked : fetchDetailsResult[0].blocked,
                                    cart_id : fetchDetailsResult[0].cart_id,
                                    merchant_in_cart : fetchDetailsResult[0].merchant_id,
                                    productsCountInCart : productsCountInCart
                                }
                                res.status(200).send({
                                    error : false,
                                    status : "You've been logged in Successfully",
                                    user_details : temp_arr
                                });
                            }
                            else if(fetchDetailsResult[0].products_id != null && fetchDetailsResult[0].products_id != '' && fetchDetailsResult[0].products_id.indexOf(',') == -1) {
                                var productsCountInCart = 1;
                                temp_arr = {
                                    user_id : fetchDetailsResult[0].user_id,
                                    name : fetchDetailsResult[0].name,
                                    email : fetchDetailsResult[0].email,
                                    mobile_num : fetchDetailsResult[0].mobile_num,
                                    user_img : fetchDetailsResult[0].user_img,
                                    os_type : fetchDetailsResult[0].os_type,
                                    fcm_id : fetchDetailsResult[0].fcm_id,
                                    blocked : fetchDetailsResult[0].blocked,
                                    cart_id : fetchDetailsResult[0].cart_id,
                                    merchant_in_cart : fetchDetailsResult[0].merchant_id,
                                    productsCountInCart : productsCountInCart
                                }
                                res.status(200).send({
                                    error : false,
                                    status : "You've been logged in Successfully",
                                    user_details : temp_arr
                                });
                            }
                            else if(fetchDetailsResult[0].products_id == null || fetchDetailsResult[0].products_id == '') {
                                var productsCountInCart = 0;
                                temp_arr = {
                                    user_id : fetchDetailsResult[0].user_id,
                                    name : fetchDetailsResult[0].name,
                                    email : fetchDetailsResult[0].email,
                                    mobile_num : fetchDetailsResult[0].mobile_num,
                                    user_img : fetchDetailsResult[0].user_img,
                                    os_type : fetchDetailsResult[0].os_type,
                                    fcm_id : fetchDetailsResult[0].fcm_id,
                                    blocked : fetchDetailsResult[0].blocked,
                                    cart_id : fetchDetailsResult[0].cart_id,
                                    productsCountInCart : productsCountInCart
                                }
                                res.status(200).send({
                                    error : false,
                                    status : "You've been logged in Successfully",
                                    user_details : temp_arr
                                });
                            }
                            
                        }
                        else if(fetchDetailsResult[0].blocked != 0) {
                            res.status(200).send({
                                error : true,
                                message : 'Blocked User'
                            });
                        }
                    }
                });
            }
            else if(email == searchResult[0].email && password != decrypted_password) {
                res.status(200).send({
                    error : true,
                    message : 'Incorrect password'
                });
            }
        }
        else {
            res.status(200).send({
                error : true,
                message : "User doesn't exists"
            });
        }
    });

});




// Creating Merchant Entry after verification

router.post('/insertMerchant', function(req, res) {

    var merchant_name = req.body.merchant_name;
    var merchant_email = req.body.merchant_email;
    var password = req.body.password;
    var merchant_contact = req.body.merchant_contact;
    var shop_name = req.body.shop_name;
    var address = req.body.address;
    var zip_code = req.body.zip_code;
    var area = req.body.area;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var merchant_img = req.body.merchant_img;
    var os_type = req.body.os_type;
    var fcm_id = req.body.fcm_id;
    var encrypted_password = encrypt(password);
    var quoted_email = "'"+merchant_email+"'";

    var query = "SELECT status FROM email_verify WHERE email = "+quoted_email+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length == 0) {
            res.status(200).send({
                error : true,
                message : 'Complete your email verification'
            });
        }
        else if(result[0].status == 1) {
            var checkMerchantEntry_query = "SELECT merchant_email FROM merchants";
            connection.query(checkMerchantEntry_query, function(checkMerchantEntryErr, checkMerchantEntryResult, existing) {
                if(checkMerchantEntryErr) {
                    res.status(200).send({
                        error : true,
                        message : checkMerchantEntryErr
                    });
                }
                else {
                    for(var i = 0; i < checkMerchantEntryResult.length; i++) {
                        if(checkMerchantEntryResult[i].merchant_email == merchant_email) {
                            existing = 1;
                            break;
                        }
                    }
                    if(existing == 1) {
                        res.status(200).send({
                            error : true,
                            message : 'Merchant already registered',
                        });
                    }
                    else {
                        var insert_query = "INSERT INTO merchants(merchant_name,merchant_email,password,merchant_contact,shop_name,address,zip_code,area,latitude,longitude,merchant_img,os_type,fcm_id) VALUES('"+merchant_name+"' , '"+merchant_email+"' , '"+encrypted_password+"' , '"+merchant_contact+"' , '"+shop_name+"' , '"+address+"' , '"+zip_code+"' , '"+area+"' , '"+latitude+"' , '"+longitude+"' , '"+merchant_img+"' , '"+os_type+"', '"+fcm_id+"')";
                        connection.query(insert_query, function(insertErr, insertResult) {
                            if(insertErr) {
                                res.status(200).send({
                                    error : true,
                                    message : insertErr
                                });
                            }
                            else if(insertResult.affectedRows != 0) {
                                var catalogue_insertQuery = "INSERT INTO merchant_catalogue(merchant_id) VALUES('"+insertResult.insertId+"')";
                                connection.query(catalogue_insertQuery, function(catalogueinsertErr, catalogueinsertResult) {
                                    if(catalogueinsertErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : catalogueinsertErr
                                        });
                                    }
                                    else {
                                        var inventory_insertQuery = "INSERT INTO merchant_inventory(merchant_id) VALUES('"+insertResult.insertId+"')";
                                        connection.query(inventory_insertQuery, function(inventoryinsertErr, inventoryinsertResult) {
                                            if(inventoryinsertErr) {
                                                res.status(200).send({
                                                    error : true,
                                                    message : inventoryinsertErr
                                                });
                                            }
                                            else {
                                                res.status(201).send({
                                                    error : false,
                                                    status : 'Inserted',
                                                    merchant_name : merchant_name,
                                                    merchant_email : merchant_email,
                                                    merchant_contact : merchant_contact,
                                                    shop_name : shop_name,
                                                    merchant_image : merchant_img,
                                                    catalogue_id : catalogueinsertResult.insertId,
                                                    inventory_id : inventoryinsertResult.insertId
                                                });
                                            }
                                        });
                                    }
                                });        
                            }
                        });
                    }
                }
            });
        }
        else if(result[0].status == 0) {
            res.status(200).send({
                error : true,
                message : 'Complete your email verification'
            });
        }
    });
 
});




// Merchant Login API for registered merchants

router.post('/loginMerchant', function(req, res) {

    var merchant_email = req.body.merchant_email;
    var password = req.body.password;
    var quoted_email = "'"+merchant_email+"'";

    var search_query = "SELECT merchant_email,password FROM merchants WHERE merchant_email LIKE "+quoted_email+"";
    connection.query(search_query, function(searchErr, searchResult) {
        if(searchErr) {
            res.status(200).send({
                error : true,
                message : searchErr
            });
        }
        else if(searchResult.length != 0) {
            var decrypted_password = decrypt(searchResult[0].password);
            if(merchant_email == searchResult[0].merchant_email && password == decrypted_password) {
                var fetchDetails_query = "SELECT merchants.merchant_email,merchants.merchant_contact,merchants.shop_name,merchants.merchant_name,merchants.address,zip_code,merchants.area,merchants.latitude,merchants.longitude,merchants.merchant_img,merchants.os_type,merchants.fcm_id,merchants.admin_approval,merchant_catalogue.catalogue_id,merchant_inventory.inventory_id FROM merchants LEFT JOIN merchant_catalogue ON merchants.merchant_id = merchant_catalogue.merchant_id LEFT JOIN merchant_inventory ON merchants.merchant_id = merchant_inventory.merchant_id WHERE merchants.merchant_email LIKE "+quoted_email+"";
                connection.query(fetchDetails_query, function(fetchDetailsErr, fetchDetailsResult) {
                    if(fetchDetailsErr) {
                        res.status(200).send({
                            error : true,
                            message : fetchDetailsErr
                        });
                    }
                    else {
                        if(fetchDetailsResult[0].admin_approval == 0) {
                            res.status(200).send({
                                error : false,
                                message : "Login Successful",
                                merchant_details : fetchDetailsResult[0]
                            });
                        }
                        else if(fetchDetailsResult[0].admin_approval != 0) {
                            res.status(200).send({
                                error : true,
                                message : "Blocked Merchant"
                            });
                        }
                    }
                });
            }
            else if(merchant_email == searchResult[0].merchant_email && password != decrypted_password) {
                res.status(200).send({
                    error : true,
                    message : 'Incorrect password'
                });
            }
        }
        else {
            res.status(200).send({
                error : true,
                message : "Merchant doesn't exists"
            });
        }
    });

});




// Adding new Categories

router.post('/addCategory', function(req, res) {

    var category_name = req.body.category_name;
    var category_img = req.body.category_img;
    var added_by = req.body.added_by;               // 0 - If added by admin
    var quotedCategory = "'"+category_name+"'";
    
    var repititionQuery = "SELECT category_name FROM categories WHERE category_name LIKE "+quotedCategory+"";
    connection.query(repititionQuery, function(repititionErr, repititionResult) {
        if(repititionErr) {
            res.status(200).send({
                error : true,
                message : repititionErr
            });
        }
        else if(repititionResult.length != 0) {
            res.status(200).send({
                error : true,
                message : 'Category already Exists'
            });
        }
        else {
            if(added_by != 0) {
                var searchMerchantQuery = "SELECT merchant_name FROM merchants WHERE merchant_id="+added_by+"";
                connection.query(searchMerchantQuery, function(searchMerchantErr, searchMerchantResult) {
                    if(searchMerchantErr) {
                        res.status(200).send({
                            error : true,
                            message : searchMerchantErr
                        });
                    }
                    else {
                        var insertQuery = "INSERT INTO categories(category_name,category_image,added_by) VALUES('"+category_name+"' , '"+category_img+"' , '"+added_by+"')";
                        connection.query(insertQuery, function(insertErr, insertResult) {
                            if(insertErr) {
                                res.status(200).send({
                                    error : true,
                                    message : insertErr
                                });
                            }
                            else if(insertResult.affectedRows != 0) {
                                res.status(201).send({
                                    error : false,
                                    status : 'Inserted',
                                    category_id : insertResult.insertId,
                                    category_name : category_name,
                                    category_img : category_img,
                                    added_by : {
                                        id : added_by,
                                        name : searchMerchantResult[0].merchant_name
                                    },
                                });
                            }
                        });
                    }
                });
            }
            else if(added_by == 0) {
                var insertQuery = "INSERT INTO categories(category_name,category_image,added_by) VALUES('"+category_name+"' , '"+category_img+"' , '"+added_by+"')";
                connection.query(insertQuery, function(insertErr, insertResult) {
                    if(insertErr) {
                        res.status(200).send({
                            error : true,
                            message : insertErr
                        });
                    }
                    else if(insertResult.affectedRows != 0) {
                        res.status(201).send({
                            error : false,
                            status : 'Inserted',
                            category_id : insertResult.insertId,
                            category_name : category_name,
                            category_img : category_img,
                            added_by : {
                                id : added_by,
                                name : "Admin"
                            },
                        });
                    }
                });
            }
        }
    });

});




// Get all categories and their details

router.get('/getAllCategories', function(req, res) {

    var query = "SELECT * FROM categories";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                categories : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Categories found'
            })
        }
    });

});




// Get Category by Id

router.get('/getCategoryById/', function(req, res) {

    var categoryId = req.query.categoryId;

    var query = "SELECT * FROM categories WHERE category_id = "+categoryId+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                category : result[0],
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Category found',
            })
        }
    });

});




// Get Category by Name

router.get('/getCategoryByName/', function(req, res) {

    var categoryName = req.query.categoryName;

    var query = "SELECT * FROM categories WHERE category_name LIKE CONCAT('"+categoryName+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                category : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Category found',
            });
        }
    });

});




// Search Categories available with merchants

router.get('/searchMerchantCategories/', function(req, res) {

    var merchantId = req.query.merchantId;
    var categoryName = req.query.categoryName;

    var urls = new Array("http://52.43.215.200:8080/pyg/user/getCategoryByName/?categoryName="+categoryName+"" , "http://52.43.215.200:8080/pyg/user/getMerchantCategories/?merchantId="+merchantId+"");
    var urlResult1;
    var urlResult2;
    var searchResult = new Array();

    urls.forEach(function(url, index) {
        if(index == 0) { 
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult1 = body.category;
                }
            });
        }
        else if(index == 1) {
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult2 = body.categories;
                }
            });
        }
    });

    setTimeout(function() {
        urlResult1.forEach(function(commonResult) {
            urlResult2.forEach(function(merchantResult) {
                if(commonResult.category_name == merchantResult.category_name) {
                    getResultFromOtherAPI(merchantResult);                                        
                }
            });
        });
        searchResult = getResultFromOtherAPI(1);
        setTimeout(function() {
            getResultFromOtherAPI(0);
            if(searchResult.length != 0) {
                res.status(200).send({
                    error : false,
                    searchResults : searchResult
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    searchResults : "No Categories Found"
                });
            }
        }, 1000);
    }, 2000);

});




// Add Products to Category

router.post('/addProduct', function(req, res) {

    var product_name = req.body.product_name;
    var product_img = req.body.product_img;
    var category_id = req.body.category_id;
    var product_description = req.body.product_description;
    var sku = req.body.sku;
    var quantification = req.body.quantification;
    var added_by = req.body.added_by;                                   // 0 - If added by admin                           

    var repititionQuery = "SELECT product_name FROM products WHERE product_name LIKE '"+product_name+"'";
    connection.query(repititionQuery, function(repititionErr, repititionResult) {
        if(repititionErr) {
            res.status(200).send({
                error : true,
                message : repititionErr
            });
        }
        else if(repititionResult.length != 0) {
            res.status(200).send({
                error : true,
                message : 'Product already Exists'
            });
        }
        else {
            var search_query = "SELECT category_name FROM categories WHERE category_id = "+category_id+"";
            connection.query(search_query, function(searchErr, searchResult) {
                if(searchErr) {
                    res.status(200).send({
                        error : true,
                        message : searchErr
                    });
                }
                else if(added_by != 0) {
                    var searchMerchantQuery = "SELECT merchant_name FROM merchants WHERE merchant_id = "+added_by+"";
                    connection.query(searchMerchantQuery, function(searchMerchantErr, searchMerchantResult){
                        if(searchMerchantErr) {
                            res.status(200).send({
                                error : true,
                                message : searchMerchantErr
                            });
                        }
                        else {
                            var insert_query = "INSERT INTO products(product_id,product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('' , "+product_name+"', '"+product_img+"' , '"+category_id+"' , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , '"+added_by+"')";
                            connection.query(insert_query, function(insertErr, insertResult) {
                                if(insertErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : insertErr
                                    });
                                }
                                else if(insertResult.affectedRows != 0) {
                                    res.status(201).send({
                                        error : false,
                                        message : 'Inserted',
                                        product_id : insertResult.insertId,
                                        product_name : product_name,
                                        product_img : product_img,
                                        category_id : category_id,
                                        category_name : searchResult[0].category_name,
                                        product_description : product_description,
                                        sku : sku,
                                        quantification : quantification,
                                        added_by : {
                                            id : added_by,
                                            name : searchMerchantResult[0].merchant_name
                                        },
                                    });
                                }
                            });
                        }
                    });
                }
                else {
                    var insert_query = "INSERT INTO products(product_id,product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('',"+product_name+"', '"+product_img+"' , '"+category_id+"' , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , '"+added_by+"')";
                    connection.query(insert_query, function(insertErr, insertResult) {
                        if(insertErr) {
                            res.status(200).send({
                                error : true,
                                message : insertErr
                            });
                        }
                        else if(insertResult.affectedRows != 0) {
                            res.status(201).send({
                                error : false,
                                message : 'Inserted',
                                product_id : insertResult.insertId,
                                product_name : product_name,
                                product_img : product_img,
                                category_id : category_id,
                                category_name : searchResult[0].category_name,
                                product_description : product_description,
                                sku : sku,
                                quantification : quantification,
                                added_by : {
                                    id : added_by,
                                    name : "Admin"
                                },
                            });
                        }
                    });
                }       
            });
        }
    });

});




// Get all products and their details

router.get('/getAllProducts', function(req, res) {

    var query = "SELECT * FROM products";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                categories : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Products found'
            })
        }
    });

});




// Get Category by Id

router.get('/getProductById/', function(req, res) {

    var productId = req.query.productId;

    var query = "SELECT * FROM products WHERE product_id = "+productId+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                category : result[0],
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Product found',
            })
        }
    });

});




// Get Category by Name

router.get('/getProductByName/', function(req, res) {

    var productName = req.query.productName;

    var query = "SELECT * FROM products WHERE product_name LIKE CONCAT('"+productName+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                product : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Product found',
            });
        }
    });
});




// Search Products available with merchants

router.get('/searchMerchantProducts/', function(req, res) {

    var merchantId = req.query.merchantId;
    var categoryId = req.query.categoryId;
    var productName = req.query.productName;

    var urls = new Array("http://52.43.215.200:8080/pyg/user/getProductByName/?productName="+productName+"" , "http://52.43.215.200:8080/pyg/user/getMerchantProducts/?merchantId="+merchantId+"&categoryId="+categoryId+"");
    var urlResult1;
    var urlResult2;
    var searchResult = new Array();

    urls.forEach(function(url, index) {
        if(index == 0) { 
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult1 = body.product;
                }
            });
        }
        else if(index == 1) {
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult2 = body.products;
                }
            });
        }
    });

    setTimeout(function() {
        urlResult1.forEach(function(commonResult) {
            urlResult2.forEach(function(merchantResult) {
                if(commonResult.product_name == merchantResult.product_name) {
                    getResultFromOtherAPI(merchantResult);
                }
            });
        });
        searchResult = getResultFromOtherAPI(1);
        setTimeout(function() {
            getResultFromOtherAPI(0);
            if(searchResult.length != 0) {
                res.status(200).send({
                    error : false,
                    searchResults : searchResult
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    searchResults : "No Categories Found"
                });
            }
        }, 1000);
    }, 2000);

});




// Locate Nearby Merchants

router.get('/getNearbyMerchants/', function(req, res) {

    var userLat = req.query.userLat;
    var userLong = req.query.userLong;
    var radius = req.query.radius;
    
    var fetchMerchantsQuery = "SELECT merchant_id,latitude,longitude FROM merchants";
    connection.query(fetchMerchantsQuery, function(fetchMerchantErr, fetchMerchantResult) {
        if(fetchMerchantErr) {
            res.status(200).send({
                error : true,
                message : fetchMerchantErr,
            });
        }
        else if(fetchMerchantResult.length != 0) {
            const data = [];
            for(var i = 0; i < fetchMerchantResult.length; i++) {
                temp_arr = [fetchMerchantResult[i].latitude, fetchMerchantResult[i].longitude, fetchMerchantResult[i].merchant_id];
                data.push(temp_arr);
            }
            const dataSet = Geo.createCompactSet(data);
            const geo = new Geo(dataSet, {sorted: true});

            var nearbyMerchants = geo.nearBy(userLat, userLong, 1000);
            if(nearbyMerchants != null) {
                for(var k = 0; k < nearbyMerchants.length; k++) {
                    var query = "SELECT merchant_id,shop_name,address,zip_code,area,latitude,longitude FROM merchants WHERE merchant_id = "+nearbyMerchants[k].i+" AND admin_approval = 1";
                    connection.query(query, function(err, result) {
                        if(err) {
                            res.status(200).send({
                                error : true,
                                message : 'Error in fetching merchant details'
                            });
                        }
                        else {
                            setMerchants(result[0]);
                        }
                    });
                }
                var nearbyMerchantDetails = setMerchants(1);
                setTimeout(function() {
                    if(nearbyMerchantDetails.length != 0) {
                        setMerchants(0);    
                        res.status(200).send({
                            error : false,
                            merchants : nearbyMerchantDetails
                        });
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : 'No merchant found',
                        });
                    }
                }, 3000);
                
            }
            else {
                res.status(200).send({
                    error : true,
                    message : 'No merchant found',
                });
            }
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Merchant found',
            });
        }
    });

});




// Get available categories with a merchant

router.get('/getMerchantCategories/', function(req, res) {

    var merchantId = req.query.merchantId;

    var inventoryQuery = "SELECT inventory_id,merchant_id,categories_id FROM merchant_inventory WHERE merchant_id = "+merchantId+"";
    connection.query(inventoryQuery, function(inventoryErr, inventoryResult) {
        if(inventoryErr) {
            res.status(200).send({
                error : true,
                message : inventoryErr
            });
        }
        else if(inventoryResult.length != 0 && inventoryResult[0].categories_id != null) {
            var categoriesRepititive = inventoryResult[0].categories_id;
            var categoriesRepititiveArr = categoriesRepititive.split(',');
            var categoriesArr = categoriesRepititiveArr.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            });
            for(var i = 0; i < categoriesArr.length; i++) {
                var categoriesQuery = "SELECT category_id,category_name,category_image FROM categories WHERE category_id = "+categoriesArr[i]+"";
                connection.query(categoriesQuery, function(categoriesErr, categoriesResult) {
                    if(categoriesErr) {
                        res.status(200).send({
                            error : true,
                            message : categoriesErr,
                        });
                    }
                    else {
                        getcategoriesByMerchant(categoriesResult[0]);
                    }
                });                
            }
            var categoriesWithMerchant = getcategoriesByMerchant(1);
            setTimeout(function() {
                if(categoriesWithMerchant.length != 0) {
                    getcategoriesByMerchant(0);    
                    res.status(200).send({
                        error : false,
                        inventory_id : inventoryResult[0].inventory_id,
                        merchant_id : inventoryResult[0].merchant_id,
                        categories : categoriesWithMerchant
                    });
                }
                else {
                    res.status(200).send({
                        error : true,
                        message : 'No categories found',
                    });
                }
            }, 1000);
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No categories found',
            });            
        }
    });
    
});




// Get Products and their details on selected category available with selected merchant

router.get('/getMerchantProducts/', function(req, res) {
	
	var merchantId = req.query.merchantId;
    var categoryId = req.query.categoryId;
	
	var query = "SELECT * FROM merchant_inventory WHERE merchant_id = "+merchantId+"";
	connection.query(query, function(err, result) {
		if(err) {
			res.status(200).send({
				error : true,
				message : err
			});
		}
		else if(result[0].products_id != null && result[0].products_id != '') {
			
			var productsArr = result[0].products_id.split(',');
			var categoriesArr = result[0].categories_id.split(',');
			var skuArr = result[0].sku.split(',');
			var availabilityArr = result[0].available_for_sale.split(',');
			var quantificationArr = result[0].quantification.split(',');
			var defaultPriceArr = result[0].default_price.split(',');
			var sellingPriceArr = result[0].selling_price.split(',');
			var pygPriceArr = result[0].pyg_price.split(',');
			var seasonalArr = result[0].seasonal.split(',');
			var availableQtyArr = result[0].available_qty.split(',');
			var lowStockArr = result[0].low_stock.split(',');
			
			var categoriesIndex = new Array();
            var productDetails = new Array();
			
			var strCategoryId = categoryId.toString();
			for(var i = 0; i < categoriesArr.length; i++) {
				if(strCategoryId == categoriesArr[i]) {
					categoriesIndex.push(i);
				}
            }
            
			categoriesIndex.forEach(function(k) {
                
                var productIdInteger = parseInt(productsArr[k],10);

				var productNameQuery = "SELECT products.product_name, products.product_image, products.category_id, products.product_description, categories.category_name FROM products LEFT JOIN categories ON products.category_id = categories.category_id WHERE products.product_id = "+productIdInteger+"";
				connection.query(productNameQuery, function(productNameErr, productNameResult) {
					if(productNameErr) {
						res.status(200).send({
							error : true,
							message : productNameErr
						});
					}
					else {
						temp_arr = {
                            category_id : productNameResult[0].category_id,
                            category_name : productNameResult[0].category_name,
							product_id : productsArr[k],
							product_name : productNameResult[0].product_name,
                            product_img : productNameResult[0].product_image,
                            product_description : productNameResult[0].product_description,
							sku : skuArr[k],
							availability : availabilityArr[k],
							quantification : quantificationArr[k],
							default_price : defaultPriceArr[k],
							selling_price : sellingPriceArr[k],
							pyg_price : pygPriceArr[k],
							seasonal : seasonalArr[k],
							available_qty : availableQtyArr[k],
							low_stock : lowStockArr[k]
                        };
                        setProductDetails(temp_arr);
                    }
                });
            });
            productDetails = setProductDetails(1);
            setTimeout(function() {
                if(productDetails.length != 0) {
                    setProductDetails(0);
                    res.status(200).send({
                        error : false,
                        merchant_id : result[0].merchant_id,
                        inventory_id : result[0].inventory_id,
                        products : productDetails
                    });
                }
                else {
                    res.status(200).send({
                        error : true,
                        message : 'No Products Found',
                    });
                }
            }, 1000);
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Products Found',
            });
        }       
    });

});




// Add Products to Cart (Revised Copy)

router.post('/addtoCartRevised', function(req, res) {

    //Required Params
    var user_id = req.body.user_id;
    var cart_id = req.body.cart_id;
    var merchant_id = req.body.merchant_id;
    var products = req.body.products;
    var qty = req.body.qty;

    var insertCartQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = '"+products+"', qty = '"+qty+"' WHERE user_id = "+user_id+" AND cart_id = "+cart_id+"";
    connection.query(insertCartQuery, function(insertCartErr, insertCartResult) {
        if(insertCartErr) {
            res.status(200).send({
                error : true,
                message : insertCartErr
            });
        }
        else if(insertCartResult.affectedRows != 0) {
            res.status(201).send({
                error : false,
                status : "Cart altered",
                products : products,
                qty : qty
            });
        }
    });

});




// Add Products to Cart

router.post('/addtoCart', function(req, res) {

    var user_id = req.body.user_id;
    var cart_id = req.body.cart_id;
    var merchant_id = req.body.merchant_id;
    var product_id = req.body.product_id;
    var qty = req.body.qty;
    var strProductId = product_id.toString();
    var strQty = qty.toString();

    // Fetch Existing cart Status
    var query = "SELECT * FROM cart WHERE cart_id = "+cart_id+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
                //check : 1
            });
        }

        // What If the existing cart is empty
        else if(result[0].products_id == null && result[0].qty == null) {

            // Only Process the posted product if it's quantity is not equal to 0
            if(strQty != '0') {
                var emptyUpdateQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = '"+strProductId+"', qty = '"+strQty+"' WHERE cart_id = "+cart_id+" AND user_id = "+user_id+"";
                connection.query(emptyUpdateQuery, function(emptyUpdateErr, emptyUpdateResult) {
                    if(emptyUpdateErr) {
                        res.status(200).send({
                            error : true,
                            message : emptyUpdateErr,
                            //check : 2
                        });
                    }
                    else if(emptyUpdateResult.changedRows != 0) {
                        res.status(201).send({
                            error : false,
                            status : 'Cart Altered',
                            products : strProductId,
                            qty : strQty,
                            //check : 3
                        });
                    }
                });
            }
        }

        // What if the existing cart is not empty
        else if(result[0].products_id != null && result[0].qty != null) {

            // What if the existing cart contains more than 1 item
            if(result[0].products_id.indexOf(',') > -1 && result[0].qty.indexOf(',') > -1) {

                // To Check if the posted item is already existing in cart
                var splitProductsInCart = result[0].products_id.split(',');
                var splitQtyInCart = result[0].qty.split(',');

                var existingIndex = splitProductsInCart.indexOf(strProductId);

                if(existingIndex != -1) {
                    modifyExistingProductInCart(existingIndex,splitProductsInCart,splitQtyInCart);
                }
                else if(existingIndex == -1) {

                    //If product is not in cart just append the posted item
                    // Before appending check existence by passing 0 to function
                    splitProductsInCart.push(strProductId);
                    splitQtyInCart.push(strQty);
                    var appendUpdateProductStr = splitProductsInCart.join(',');
                    var appendUpdateQtyStr = splitQtyInCart.join(',');

                    var appendUpdateQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = '"+appendUpdateProductStr+"', qty = '"+appendUpdateQtyStr+"' WHERE cart_id = "+cart_id+"";
                    connection.query(appendUpdateQuery, function(appendUpdateErr, appendUpdateResult) {
                        if(appendUpdateErr) {
                            res.status(200).send({
                                error : true,
                                message : appendUpdateErr,
                                //check : 4
                            });
                        }
                        else if(appendUpdateResult.changedRows != 0) {
                            res.status(201).send({
                                error : false,
                                status : 'Cart Altered',
                                products : appendUpdateProductStr,
                                qty : appendUpdateQtyStr,
                                //check : 5
                            });
                        }
                    });
                }

                // If product is already in cart control shifts here
                function modifyExistingProductInCart(index,splittedProductsArr,splittedQtyArr) {
                    
                        //Check Whether posted qty is not 0 for an existing item
                        if(strQty != '0') {
                            splittedProductsArr[index] = strProductId;
                            splittedQtyArr[index] = strQty;
                            var middleModifyProductStr = splittedProductsArr.join(',');
                            var middleModifyQtyStr = splittedQtyArr.join(',');

                            var middleModifyQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = '"+middleModifyProductStr+"', qty = '"+middleModifyQtyStr+"' WHERE cart_id = "+cart_id+"";
                            connection.query(middleModifyQuery, function(middleModifyErr, middleModifyResult) {
                                if(middleModifyErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : middleModifyErr,
                                        //check : 6,
                                    });
                                }
                                else if(middleModifyResult.changedRows != 0) {
                                    res.status(201).send({
                                        error : false,
                                        status : 'Cart Altered',
                                        products : middleModifyProductStr,
                                        qty : middleModifyQtyStr,
                                        //check : 7
                                    });
                                }
                            });
                        }

                        //What if the posted qty is 0 for an existing item
                        else if(strQty == '0') {
                            splittedProductsArr.splice(index, 1);
                            splittedQtyArr.splice(index, 1);

                            //After removing an item from Cart check the cart's length is greater than 1 to join
                            if(splittedProductsArr.length > 1) {
                                var removeUpdateProductStr = splittedProductsArr.join(',');
                                var removeUpdateQtyStr = splittedQtyArr.join(',');

                                var removedUpdateQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = '"+removeUpdateProductStr+"', qty = '"+removeUpdateQtyStr+"' WHERE cart_id = "+cart_id+"";
                                connection.query(removedUpdateQuery, function(removedUpdateErr, removedUpdateResult) {
                                    if(removedUpdateErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : removedUpdateErr,
                                            //check : 8
                                        });
                                    }
                                    else if(removedUpdateResult.changedRows != 0) {
                                        res.status(201).send({
                                            error : false,
                                            status : 'Cart Altered',
                                            products : removeUpdateProductStr,
                                            qty : removeUpdateQtyStr,
                                            //check : 9
                                        });
                                    }
                                });
                            }

                            //After removing an item from Cart check the cart's length is equal to 1
                            else if(splittedProductsArr.length == 1) {
                                var removedSingleUpdateProductStr = splittedProductsArr[0];
                                var removedSingleUpdateQtyStr = splittedQtyArr[0];

                                var removedSingleUpdateQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = '"+removedSingleUpdateProductStr+"', qty = '"+removedSingleUpdateQtyStr+"' WHERE cart_id = "+cart_id+"";
                                connection.query(removedSingleUpdateQuery, function(removedSingleUpdateErr, removedSingleUpdateResult) {
                                    if(removedSingleUpdateErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : removedSingleUpdateErr,
                                            //check : 10
                                        });
                                    }
                                    else if(removedSingleUpdateResult.changedRows != 0) {
                                        res.status(201).send({
                                            error : false,
                                            status : 'Cart Altered',
                                            products : removedSingleUpdateProductStr,
                                            qty : removedSingleUpdateQtyStr,
                                            //check : 11
                                        });
                                    }
                                });                            
                            }
                        }
                }
            }

            // What if the existing cart contains only 1 item
            else {

                //What if the posted(1) item and existing(1) item is equal
                if(strProductId == result[0].products_id) {
                    
                    //What if posted(1) qty is 0 => Delete the item from cart
                    if(strQty == '0') {
                        var singleDeleteProductStr = null;
                        var singleDeleteQtyStr = null;

                        var singleDeleteQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = NULL, qty = NULL WHERE cart_id = "+cart_id+"";
                        connection.query(singleDeleteQuery, function(singleDeleteErr, singleDeleteResult) {
                            if(singleDeleteErr) {
                                res.status(200).send({
                                    error : true,
                                    message : singleDeleteErr,
                                    //check : 12
                                });
                            }
                            else if(singleDeleteResult.changedRows != 0) {
                                res.status(201).send({
                                    error : false,
                                    status : 'Cart Altered',
                                    products : singleDeleteProductStr,
                                    qty : singleDeleteQtyStr,
                                    //check : 13
                                });
                            }
                        });
                    }

                    //What if posted(1) qty is more than 0 => Modify the exisiting qty for that item
                    else if(strQty != '0') {
                        var singleModifyProductStr = strProductId;
                        var singleModifyQtyStr = strQty;

                        var singleModifyQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = '"+singleModifyProductStr+"', qty = '"+singleModifyQtyStr+"' WHERE cart_id = "+cart_id+"";
                        connection.query(singleModifyQuery, function(singleModifyErr, singleModifyResult) {
                            if(singleModifyErr) {
                                res.status(200).send({
                                    error : true,
                                    message : singleModifyErr,
                                    //check : 14
                                });
                            }
                            else if(singleModifyResult.changedRows != 0) {
                                res.status(201).send({
                                    error : false,
                                    status : 'Cart Altered',
                                    products : singleModifyProductStr,
                                    qty : singleModifyQtyStr,
                                    //check : 15
                                });
                            }
                        });                    
                    }
                }

                //What if the posted(1) item and existing(1) item is not equal => Just Append to exisiting values
                else if(strProductId != result[0].products_id) {
                    var alteredProductArr = [result[0].products_id , strProductId];
                    var alteredQtyArr = [result[0].qty , strQty];
                    var singleUpdateProductStr = alteredProductArr.join(',');
                    var singleUpdateQtyStr = alteredQtyArr.join(',');

                    var singleUpdateQuery = "UPDATE cart SET merchant_id = "+merchant_id+", products_id = '"+singleUpdateProductStr+"', qty = '"+singleUpdateQtyStr+"' WHERE cart_id = "+cart_id+"";
                    connection.query(singleUpdateQuery, function(singleUpdateErr, singleUpdateResult) {
                        if(singleUpdateErr) {
                            res.status(200).send({
                                error : true,
                                message : singleUpdateErr,
                                //check : 16
                            });
                        }
                        else if(singleUpdateResult.changedRows != 0) {
                            res.status(201).send({
                                error : false,
                                status : 'Cart Altered',
                                products : singleUpdateProductStr,
                                qty : singleUpdateQtyStr,
                                //check : 17
                            });
                        }
                    });
                }
            }
        }
    });

    
});//eof add cart api




// View Cart
router.get('/viewCart/', function(req, res) {
    setTimeout(() => {
        var merchantId = req.query.merchantId;
        var userId = req.query.userId;
        var cartId = req.query.cartId;

        var funcRes = new Set();  
        var viewDetails = new Set();

        
        var viewCartQuery = "SELECT products_id,qty FROM cart WHERE user_id = "+userId+" AND cart_id = "+cartId+"";
        connection.query(viewCartQuery, function(viewCartErr, viewCartResult) {
            console.log("cart result product id "+viewCartResult[0].products_id);
            if(viewCartErr) {
                res.status(200).send({
                    error : true,
                    message : viewCartErr
                });
            }
            else if(viewCartResult[0].products_id != null && viewCartResult[0].products_id != '') {
    
                // If cart contains multiple items
                if(viewCartResult[0].products_id.indexOf(',') > -1 && viewCartResult[0].qty.indexOf(',') > -1) {
                    var splitCartProducts = viewCartResult[0].products_id.split(',');
                    var splitCartQty = viewCartResult[0].qty.split(',');
    
                    var viewCartDetails = new Array();
    
                    //Search cart items in merchant inventory in a loop manner
                    splitCartProducts.forEach(function(itemOnCart,indexOnCart) {
                        var productIdInt = parseInt(itemOnCart, 10);
                        var searchMerchantInventoryQuery = "SELECT merchant_inventory.inventory_id, merchant_inventory.merchant_id, merchant_inventory.products_id, merchant_inventory.selling_price, products.product_name, products.product_image, products.quantification FROM merchant_inventory LEFT JOIN products ON products.product_id = "+productIdInt+" WHERE merchant_inventory.merchant_id = "+merchantId+"";
                        connection.query(searchMerchantInventoryQuery, function(searchMerchantInventoryErr, searchMerchantInventoryResult) {                        
                            if(searchMerchantInventoryErr) {
                                res.status(200).send({
                                    error : true,
                                    message : searchMerchantInventoryErr
                                });
                            }
                            else {
                                // Search the product in inventory
                                var inventoryProductsArr = searchMerchantInventoryResult[0].products_id.split(',');
                                var inventorySellingPriceArr = searchMerchantInventoryResult[0].selling_price.split(',');
                                var indexOfProduct = inventoryProductsArr.indexOf(itemOnCart);
    
                                //transfer required values to a temp array.
                                temp_arr = {
                                    product_id : itemOnCart,
                                    product_name : searchMerchantInventoryResult[0].product_name,
                                    product_image : searchMerchantInventoryResult[0].product_image,
                                    quantification : searchMerchantInventoryResult[0].quantification,
                                    selling_price : inventorySellingPriceArr[indexOfProduct],
                                    qty : splitCartQty[indexOnCart]
                                }
                                funcRes = setCartDetails(temp_arr,viewDetails);
                                // console.log(Array.from(funcRes));
                            }
                        });
                    });
                    // viewCartDetails = setCartDetails(1);
                    setTimeout(function() {
                        if(funcRes.length != 0) {
                            // console.log("view Cart "+viewCartDetails);
                            // setCartDetails(0);
                            res.status(200).send({
                                error : false,
                                user_id : userId,
                                cart_id : cartId,
                                merchant_id : merchantId,
                                cart : Array.from(funcRes)
                            });
                        }
                    },5000);
                }
                else {
                    
                    //If cart contains only 1 item
    
                    var singleproductIdInt = parseInt(viewCartResult[0].products_id, 10);
                    var singleProductSearchQuery = "SELECT merchant_inventory.inventory_id, merchant_inventory.merchant_id, merchant_inventory.products_id, merchant_inventory.selling_price, products.product_name, products.product_image, products.quantification FROM merchant_inventory LEFT JOIN products ON products.product_id = "+singleproductIdInt+" WHERE merchant_inventory.merchant_id = "+merchantId+"";
                    
                    connection.query(singleProductSearchQuery, function(singleProductSearchErr, singleProductSearchResult) {
                        if(singleProductSearchErr) {
                            res.status(200).send({
                                error : true,
                                message : singleProductSearchErr
                            });
                        }
                        else {
                            // Search the product in inventory
                            var singleInventoryProductsArr = singleProductSearchResult[0].products_id.split(',');
                            var singleInventorySellingPriceArr = singleProductSearchResult[0].selling_price.split(',');
                            var singleIndexOfProduct = singleInventoryProductsArr.indexOf(viewCartResult[0].products_id);
    
                            temp_arr = {
                                product_id : viewCartResult[0].products_id,
                                product_name : singleProductSearchResult[0].product_name,
                                product_image : singleProductSearchResult[0].product_image,
                                quantification : singleProductSearchResult[0].quantification,
                                selling_price : singleInventorySellingPriceArr[singleIndexOfProduct],
                                qty : viewCartResult[0].qty
                            }
    
                            //transfer into response
                            setTimeout(function() {
                                res.status(200).send({
                                    error : false,
                                    user_id : userId,
                                    cart_id : cartId,
                                    merchant_id : merchantId,
                                    cart : [temp_arr]
                                });
                            }, 1000);
                        }
                    });
                }            
            }
            else if(viewCartResult[0].products_id == null || viewCartResult[0].products_id == '') {
                res.status(200).send({
                    error : true,
                    message : 'Cart is Empty',
                });
            }
        });    
    }, 1000);
});




// Products availablity in cart
router.get('/productInCart/', function(req, res) {

    var productId = req.query.productId;
    var cartId = req.query.cartId;
    var userId = req.query.userId;
    var merchantId = req.query.merchantId;

    var cartSearchQuery = "SELECT products.product_id, products.product_name, products.product_description, products.product_image, products.quantification, cart.products_id, cart.qty FROM products LEFT JOIN cart ON cart.cart_id = "+cartId+" AND cart.user_id = "+userId+" WHERE products.product_id = "+productId+"";
    connection.query(cartSearchQuery, function(cartSearchErr, cartSearchResult) {
        if(cartSearchErr) {
            res.status(200).send({
                error : true,
                message : cartSearchErr,
            });
        }
        else {
            var inventoryQuery = "SELECT merchant_id,products_id,selling_price FROM merchant_inventory WHERE merchant_id = "+merchantId+"";
            connection.query(inventoryQuery, function(inventoryErr, inventoryResult) {
                if(inventoryErr) {
                    res.status(200).send({
                        error : true,
                        message : inventoryErr
                    });
                }
                // If multiple products in cart and it's available in Cart
                else if(cartSearchResult[0].products_id != null && cartSearchResult[0].products_id != '' && cartSearchResult[0].products_id.indexOf(',') > -1 && cartSearchResult[0].qty.indexOf(',') > -1) {
                    var splitCartArr = cartSearchResult[0].products_id.split(',');
                    var splitCartQty = cartSearchResult[0].qty.split(',');
                    var productIndexInCart = splitCartArr.indexOf(productId);
                    if(productIndexInCart != -1) {
                        var qtyInCart = splitCartQty[productIndexInCart];

                        var splitInventoryProductsArr = inventoryResult[0].products_id.split(',');
                        var splitInventoryPriceArr = inventoryResult[0].selling_price.split(',');
                        var productIndexInInventory = splitInventoryProductsArr.indexOf(productId);
                        var sellingPrice = splitInventoryPriceArr[productIndexInInventory];

                        temp_arr = {
                            product_id : productId,
                            product_name : cartSearchResult[0].product_name,
                            product_image : cartSearchResult[0].product_image,
                            product_description : cartSearchResult[0].product_description,
                            quantification : cartSearchResult[0].quantification,
                            selling_price : sellingPrice,
                            qty_in_cart : qtyInCart

                        }

                        res.status(200).send({
                            error : false,
                            user_id : userId,
                            cart_id : cartId,
                            merchant_id : merchantId,
                            product : temp_arr
                        });
                    }
                    else if(productIndexInCart == -1) {

                        var splitInventoryProductsArr = inventoryResult[0].products_id.split(',');
                        var splitInventoryPriceArr = inventoryResult[0].selling_price.split(',');
                        var productIndexInInventory = splitInventoryProductsArr.indexOf(productId);
                        var sellingPrice = splitInventoryPriceArr[productIndexInInventory];

                        temp_arr = {
                            product_id : productId,
                            product_name : cartSearchResult[0].product_name,
                            product_image : cartSearchResult[0].product_image,
                            product_description : cartSearchResult[0].product_description,
                            quantification : cartSearchResult[0].quantification,
                            selling_price : sellingPrice,
                            qty_in_cart : 0
                        }

                        res.status(200).send({
                            error : false,
                            user_id : userId,
                            cart_id : cartId,
                            merchant_id : merchantId,
                            product : temp_arr        
                        });
                    }
                }

                // If only 1 product in cart and it's available in cart
                else if(cartSearchResult[0].products_id != null && cartSearchResult[0].products_id != '' && cartSearchResult[0].products_id.indexOf(',') == -1 && cartSearchResult[0].qty.indexOf(',') == -1) {
                    var singleproductInCart = cartSearchResult[0].products_id;
                    var singleQtyInCart = cartSearchResult[0].qty;

                    if(singleproductInCart == productId) {
                        var singleSplitInventoryProductsArr = inventoryResult[0].products_id.split(',');
                        var singleSplitInventoryPriceArr = inventoryResult[0].selling_price.split(',');
                        var singleProductIndexInInventory = singleSplitInventoryProductsArr.indexOf(productId);
                        var singleSellingPrice = singleSplitInventoryPriceArr[singleProductIndexInInventory];

                        temp_arr = {
                            product_id : singleproductInCart,
                            product_name : cartSearchResult[0].product_name,
                            product_image : cartSearchResult[0].product_image,
                            product_description : cartSearchResult[0].product_description,
                            quantification : cartSearchResult[0].quantification,
                            selling_price : singleSellingPrice,
                            qty_in_cart : singleQtyInCart
                        }

                        res.status(200).send({
                            error : false,
                            user_id : userId,
                            cart_id : cartId,
                            merchant_id : merchantId,
                            product : temp_arr
                        });
                    }
                    else if(singleproductInCart != productId) {
                        var splitInventoryProductsArr = inventoryResult[0].products_id.split(',');
                        var splitInventoryPriceArr = inventoryResult[0].selling_price.split(',');
                        var productIndexInInventory = splitInventoryProductsArr.indexOf(productId);
                        var sellingPrice = splitInventoryPriceArr[productIndexInInventory];

                        temp_arr = {
                            product_id : productId,
                            product_name : cartSearchResult[0].product_name,
                            product_image : cartSearchResult[0].product_image,
                            product_description : cartSearchResult[0].product_description,
                            quantification : cartSearchResult[0].quantification,
                            selling_price : sellingPrice,
                            qty_in_cart : 0
                        }

                        res.status(200).send({
                            error : false,
                            user_id : userId,
                            cart_id : cartId,
                            merchant_id : merchantId,
                            product : temp_arr        
                        });
                    }
                }
                else if(cartSearchResult[0].products_id != null || cartSearchResult[0].products_id != '') {
                        var splitInventoryProductsArr = inventoryResult[0].products_id.split(',');
                        var splitInventoryPriceArr = inventoryResult[0].selling_price.split(',');
                        var productIndexInInventory = splitInventoryProductsArr.indexOf(productId);
                        var sellingPrice = splitInventoryPriceArr[productIndexInInventory];

                        temp_arr = {
                            product_id : productId,
                            product_name : cartSearchResult[0].product_name,
                            product_image : cartSearchResult[0].product_image,
                            product_description : cartSearchResult[0].product_description,
                            quantification : cartSearchResult[0].quantification,
                            selling_price : sellingPrice,
                            qty_in_cart : 0
                        }

                        res.status(200).send({
                            error : false,
                            user_id : userId,
                            cart_id : cartId,
                            merchant_id : merchantId,
                            product : temp_arr        
                        });
                }
            });
        }
    });

});




// Clear Cart
router.post('/clearCart', function(req, res) {
    
    var user_id = req.body.user_id;
    var cart_id = req.body.cart_id;

    var clearCartQuery = "UPDATE cart SET merchant_id = null, products_id = null, qty = null WHERE user_id = "+user_id+" AND cart_id = "+cart_id+"";
    connection.query(clearCartQuery, function(clearCartErr, clearCartResult) {
        if(clearCartErr) {
            res.status(200).send({
                error : true,
                message : clearCartErr
            });
        }
        else if(clearCartResult.affectedRows != 0) {
            res.status(201).send({
                error : false,
                status : "Cart Cleared",
            });
        }
    });

});




// Get Order through Order Id
router.get('/getOrderById/', function(req, res) {

    var orderId = req.query.orderId;
    var orderQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status WHERE orders.order_id = "+orderId+"";

    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult != null && orderResult.length != 0) {

            //Fetch Product Details
            
            //If ordered products is more than 1
            if(orderResult[0].products_id.indexOf(',') != -1) {

                var orderedProductArr = orderResult[0].products_id.split(',');
                var orderedQtyArr = orderResult[0].qty.split(',');
                var orderedProductsPriceArr = orderResult[0].price.split(',');
                var orderedProductsTotalPriceArr = orderResult[0].total_price.split(',');

                var products = new Array();

                orderedProductArr.forEach(function(item, index) {
                    var product_id = parseInt(item, 10);
                    var productDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+product_id+"";
                    connection.query(productDetailsQuery, function(productDetailsErr, productDetailsResult) {
                        if(productDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : productDetailsErr
                            });
                        }
                        else {
                            temp_arr = {
                                product_id : product_id,
                                product_name : productDetailsResult[0].product_name,
                                product_description : productDetailsResult[0].product_description,
                                sku : productDetailsResult[0].sku,
                                quantification : productDetailsResult[0].quantification,
                                ordered_qty : orderedQtyArr[index],
                                price : orderedProductsPriceArr[index],
                                total_price : orderedProductsTotalPriceArr[index]
                            };
                            orderedProducts(temp_arr);
                        }
                    }); 
                });
                var otherInfoQuery = "SELECT merchants.shop_name,feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                    connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                    if(otherInfoErr) {
                        res.status(200).send({
                            error : true,
                            message : otherInfoErr,
                        });
                    }
                    else {
                        products = orderedProducts(1);
                        setTimeout(function() {
                            orderedProducts(0);
                            res.status(200).send({
                                error : false,
                                order : {
                                    order_id : orderResult[0].order_id,
                                    user_id : orderResult[0].user_id,
                                    merchant_id : orderResult[0].merchant_id,
                                    shop_name : otherInfoResult[0].shop_name,
                                    products : products,
                                    taxes : orderResult[0].taxes,
                                    bill_amount : orderResult[0].bill_amount,
                                    tracking_id : orderResult[0].tracking_id,
                                    order_time : orderResult[0].order_time,
                                    collecting_time : orderResult[0].collecting_time,
                                    payment_mode : orderResult[0].payment_mode,
                                    payment_status : orderResult[0].payment_status,
                                    transaction_id : orderResult[0].transaction_id,
                                    merchant_approved : orderResult[0].merchant_approved,
                                    order_status : orderResult[0].order_status,
                                    order_status_name : orderResult[0].order_status_name,
                                    rating : otherInfoResult[0].rating,
                                    review : otherInfoResult[0].review
                                }
                            });
                        }, 2000);
                    }
                });
            }

            // If order contains only 1 item
            else if(orderResult[0].products_id.indexOf(',') == -1) {
                var singleProduct_id = parseInt(orderResult[0].products_id);
                var singleProductDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+singleProduct_id+"";
                    connection.query(singleProductDetailsQuery, function(singleproductDetailsErr, singleproductDetailsResult) {
                        if(singleproductDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : singleproductDetailsErr
                            });
                        }
                        else {
                            temp_arr = [{
                                product_id : singleProduct_id,
                                product_name : singleproductDetailsResult[0].product_name,
                                product_description : singleproductDetailsResult[0].product_description,
                                sku : singleproductDetailsResult[0].sku,
                                quantification : singleproductDetailsResult[0].quantification,
                                ordered_qty : orderResult[0].qty,
                                price : orderResult[0].price,
                                total_price : orderResult[0].total_price
                            }];

                            var otherInfoQuery = "SELECT merchants.shop_name,feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                            connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                                if(otherInfoErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : otherInfoErr
                                    });
                                }
                                else {
                                    res.status(200).send({
                                        error : false,
                                        order : {
                                            order_id : orderResult[0].order_id,
                                            user_id : orderResult[0].user_id,
                                            merchant_id : orderResult[0].merchant_id,
                                            shop_name : otherInfoResult[0].shop_name,
                                            products : temp_arr,
                                            taxes : orderResult[0].taxes,
                                            bill_amount : orderResult[0].bill_amount,
                                            tracking_id : orderResult[0].tracking_id,
                                            order_time : orderResult[0].order_time,
                                            collecting_time : orderResult[0].collecting_time,
                                            payment_mode : orderResult[0].payment_mode,
                                            payment_status : orderResult[0].payment_status,
                                            transaction_id : orderResult[0].transaction_id,
                                            merchant_approved : orderResult[0].merchant_approved,
                                            order_status : orderResult[0].order_status,
                                            order_status_name : orderResult[0].order_status_name,
                                            rating : otherInfoResult[0].rating,
                                            review : otherInfoResult[0].review
                                        }
                                    });
                                }
                            });
                        }
                    }); 

            } 
        }
        else if(orderResult == null || orderResult.length == 0) {
            res.status(200).send({
                error : false,
                message : 'No Orders found'
            });
        }
    });

});




// Get Order through Tracking Id
router.get('/getOrderByTrackingId/', function(req, res) {

    var trackingId = req.query.trackingId;
    var orderQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status WHERE orders.tracking_id = 'PYG#ID"+trackingId+"'";

    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult != null && orderResult.length != 0) {
            //Fetch Product Details
            
            //If ordered products is more than 1
            if(orderResult[0].products_id.indexOf(',') != -1) {

                var orderedProductArr = orderResult[0].products_id.split(',');
                var orderedQtyArr = orderResult[0].qty.split(',');
                var orderedProductsPriceArr = orderResult[0].price.split(',');
                var orderedProductsTotalPriceArr = orderResult[0].total_price.split(',');

                var products = new Array();

                orderedProductArr.forEach(function(item, index) {
                    var product_id = parseInt(item, 10);
                    var productDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+product_id+"";
                    connection.query(productDetailsQuery, function(productDetailsErr, productDetailsResult) {
                        if(productDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : productDetailsErr
                            });
                        }
                        else {
                            temp_arr = {
                                product_id : product_id,
                                product_name : productDetailsResult[0].product_name,
                                product_description : productDetailsResult[0].product_description,
                                sku : productDetailsResult[0].sku,
                                quantification : productDetailsResult[0].quantification,
                                ordered_qty : orderedQtyArr[index],
                                price : orderedProductsPriceArr[index],
                                total_price : orderedProductsTotalPriceArr[index]
                            };
                            orderedProducts(temp_arr);
                        }
                    }); 
                });
                var otherInfoQuery = "SELECT merchants.shop_name, feedback.rating, feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                    if(otherInfoErr) {
                        res.status(200).send({
                            error : true,
                            message : otherInfoErr,
                        });
                    }
                    else {
                        products = orderedProducts(1);
                        setTimeout(function() {
                            orderedProducts(0);
                            res.status(200).send({
                                error : false,
                                order : {
                                    order_id : orderResult[0].order_id,
                                    user_id : orderResult[0].user_id,
                                    merchant_id : orderResult[0].merchant_id,
                                    shop_name : otherInfoResult[0].shop_name,
                                    products : products,
                                    taxes : orderResult[0].taxes,
                                    bill_amount : orderResult[0].bill_amount,
                                    tracking_id : orderResult[0].tracking_id,
                                    order_time : orderResult[0].order_time,
                                    collecting_time : orderResult[0].collecting_time,
                                    payment_mode : orderResult[0].payment_mode,
                                    payment_status : orderResult[0].payment_status,
                                    transaction_id : orderResult[0].transaction_id,
                                    merchant_approved : orderResult[0].merchant_approved,
                                    order_status : orderResult[0].order_status,
                                    order_status_name : orderResult[0].order_status_name,
                                    rating : otherInfoResult[0].rating,
                                    review : otherInfoResult[0].review
                                }
                            });
                        }, 2000);
                    }
                });
            }

            // If order contains only 1 item
            else if(orderResult[0].products_id.indexOf(',') == -1) {
                var singleProduct_id = parseInt(orderResult[0].products_id);
                var singleProductDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+singleProduct_id+"";
                    connection.query(singleProductDetailsQuery, function(singleproductDetailsErr, singleproductDetailsResult) {
                        if(singleproductDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : singleproductDetailsErr
                            });
                        }
                        else {
                            temp_arr = [{
                                product_id : singleProduct_id,
                                product_name : singleproductDetailsResult[0].product_name,
                                product_description : singleproductDetailsResult[0].product_description,
                                sku : singleproductDetailsResult[0].sku,
                                quantification : singleproductDetailsResult[0].quantification,
                                ordered_qty : orderResult[0].qty,
                                price : orderResult[0].price,
                                total_price : orderResult[0].total_price
                            }];

                            var otherInfoQuery = "SELECT merchants.shop_name, feedback.rating, feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                            connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                                if(otherInfoErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : otherInfoErr,
                                    });
                                }
                                else {
                                    res.status(200).send({
                                        error : false,
                                        order : {
                                            order_id : orderResult[0].order_id,
                                            user_id : orderResult[0].user_id,
                                            merchant_id : orderResult[0].merchant_id,
                                            shop_name : otherInfoResult[0].shop_name,
                                            products : temp_arr,
                                            taxes : orderResult[0].taxes,
                                            bill_amount : orderResult[0].bill_amount,
                                            tracking_id : orderResult[0].tracking_id,
                                            order_time : orderResult[0].order_time,
                                            collecting_time : orderResult[0].collecting_time,   
                                            payment_mode : orderResult[0].payment_mode,
                                            payment_status : orderResult[0].payment_status,
                                            transaction_id : orderResult[0].transaction_id,
                                            merchant_approved : orderResult[0].merchant_approved,
                                            order_status : orderResult[0].order_status,
                                            order_status_name : orderResult[0].order_status_name,
                                            rating : otherInfoResult[0].rating,
                                            review : otherInfoResult[0].review
                                        }
                                    });
                                }
                            });
                        }
                    }); 
            } 
        }
        else if(orderResult == null || orderResult.length == 0) {
            res.status(200).send({
                error : false,
                message : 'No Orders found'
            });
        }
    });
    
});




// Get Orders Placed by User
router.get('/getOrdersByUser/', function(req, res) {

    var userId = req.query.userId;

    var ordersQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status WHERE orders.user_id = "+userId+" ORDER BY orders.order_time DESC";
    connection.query(ordersQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult != null && orderResult.length != 0) {

            var overallResult = new Array();
            
            //Split Orders and process every oreder in an array
            orderResult.forEach(function(order, orderIndex) {

                // Check for products ordered in each entry

                // If products ordered is more than 1
                if(order.products_id.indexOf(',') != -1) {
                    var splitOrderedProductsArr = order.products_id.split(',');
                    var splitOrderedQtyArr = order.qty.split(',');
                    var splitOrderedProductsPriceArr = order.price.split(',');
                    var splitOrderedProductsTotalPriceArr = order.total_price.split(',');

                    var productsInOrder = new Array();

                    splitOrderedProductsArr.forEach(function(product, productIndex) {
                        var productId = parseInt(product, 10);
                        var productQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+productId+"";
                        connection.query(productQuery, function(productErr, productResult) {
                            if(productErr) {
                                res.status(200).send({
                                    error : true,
                                    message : productErr
                                });
                            }
                            else {
                                temp_arr = {
                                    product_id : productId,
                                    product_name : productResult[0].product_name,
                                    product_image : productResult[0].product_image,
                                    product_description : productResult[0].product_description,
                                    sku : productResult[0].sku,
                                    quantification : productResult[0].quantification,
                                    ordered_qty : splitOrderedQtyArr[productIndex],
                                    price : splitOrderedProductsPriceArr[productIndex],
                                    total_price : splitOrderedProductsTotalPriceArr[productIndex]
                                }
                                productsInOrder[productIndex] = temp_arr;
                            }
                        });
                    });
                    var otherInfoQuery = "SELECT merchants.shop_name,feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+order.order_id+" WHERE merchants.merchant_id = "+order.merchant_id+"";
                    connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                        if(otherInfoErr) {
                            res.status(200).send({
                                error : true,
                                message : otherInfoErr,
                            });
                        }
                        else {
                            setTimeout(function() {
                                temp_order = {
                                    order_id : order.order_id,
                                    user_id : order.user_id,
                                    merchant_id : order.merchant_id,
                                    shop_name : otherInfoResult[0].shop_name,
                                    products : productsInOrder,
                                    taxes : order.taxes,
                                    bill_amount : order.bill_amount,
                                    tracking_id : order.tracking_id,
                                    order_time : order.order_time,
                                    collecting_time : order.collecting_time, 
                                    payment_mode : order.payment_mode,
                                    payment_status : order.payment_status,
                                    transaction_id : order.transaction_id,
                                    merchant_approved : order.merchant_approved,
                                    order_status : order.order_status,
                                    order_status_name : order.order_status_name,
                                    rating : otherInfoResult[0].rating,
                                    review : otherInfoResult[0].review
                                }
                                overallResult[orderIndex] = temp_order;
                            }, 3000);
                        }
                    });
                }

                // If order contains only one product
                else if(order.products_id.indexOf(',') == -1) {
                    var singleProductId = parseInt(order.products_id, 10);
                    var singleProductQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+singleProductId+"";
                    connection.query(singleProductQuery, function(singleProductErr, singleProductResult) {
                        if(singleProductErr) {
                            res.status(200).send({
                                error : true,
                                message : singleProductErr
                            });
                        }
                        else {
                            temp_arr = [{
                                product_id : singleProductId,
                                product_name : singleProductResult[0].product_name,
                                product_image : singleProductResult[0].product_image,
                                product_description : singleProductResult[0].product_description,
                                sku : singleProductResult[0].sku,
                                quantification : singleProductResult[0].quantification,
                                ordered_qty : order.qty,
                                price : order.price,
                                total_price : order.total_price
                            }];

                            var otherInfoQuery = "SELECT merchants.shop_name,feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+order.order_id+" WHERE merchants.merchant_id = "+order.merchant_id+"";
                            connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                                if(otherInfoErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : otherInfoErr,
                                    });
                                }
                                else {
                                    temp_order = {
                                        order_id : order.order_id,
                                        user_id : order.user_id,
                                        merchant_id : order.merchant_id,
                                        shop_name : otherInfoResult[0].shop_name,
                                        products : temp_arr,
                                        taxes : order.taxes,
                                        bill_amount : order.bill_amount,
                                        tracking_id : order.tracking_id,
                                        order_time : order.order_time,
                                        collecting_time : order.collecting_time, 
                                        payment_mode : order.payment_mode,
                                        payment_status : order.payment_status,
                                        transaction_id : order.transaction_id,
                                        merchant_approved : order.merchant_approved,
                                        order_status : order.order_status,
                                        order_status_name : order.order_status_name,
                                        rating : otherInfoResult[0].rating,
                                        review : otherInfoResult[0].review
                                    }
                                    overallResult[orderIndex] = temp_order;
                                }
                            });
                        }
                
                    });
                }
            });
            setTimeout(function() {
                res.status(200).send({
                    error : false,
                    orders : overallResult
                });
            }, 6000);
        }
        else if(orderResult == null || orderResult.length == 0) {
            res.status(200).send({
                error : true,
                message : 'No Orders found'
            });
        }
    });
    
});





// Place Order
router.post('/placeOrder', function(req, res) {

    var user_id = req.body.user_id;
    var user_email = req.body.user_email;
    var merchant_id = req.body.merchant_id;
    var cart_id = req.body.cart_id;
    var products = req.body.products;
    var qty = req.body.qty;
    var price = req.body.price;
    var total_price = req.body.total_price;
    var taxes = req.body.taxes;
    var bill_amount = req.body.bill_amount;
    var tracking_id = "PYG#ID"+randomstring.generate({length : 8, charset : 'numeric'})+"";
    var payment_mode = req.body.payment_mode;
    
    var merchantEmailQuery = "SELECT merchant_email FROM merchants WHERE merchant_id = "+merchant_id+"";
    connection.query(merchantEmailQuery, function(merchantEmailErr, merchantEmailResult) {
        if(!merchantEmailErr) {
            setMerchantEmail(merchantEmailResult[0].merchant_email);
        }
    });
    
    var cartCheckQuery = "SELECT products_id,qty FROM cart WHERE cart_id = "+cart_id+"";
    connection.query(cartCheckQuery, function(cartCheckErr, cartCheckResult) {
        if(cartCheckErr) {
            res.status(200).send({
                error : true,
                message : cartCheckErr,
                //check : 1,
            });
        }
        else if(cartCheckResult[0].products_id != null && cartCheckResult[0].products_id != '') {
            if(cartCheckResult[0].products_id == products && cartCheckResult[0].qty == qty) {
                var insertOrderQuery = "INSERT INTO orders(user_id,merchant_id,products_id,qty,price,total_price,taxes,bill_amount,tracking_id,order_time,payment_mode) VALUES("+user_id+" , "+merchant_id+" , '"+products+"' , '"+qty+"' , '"+price+"' , '"+total_price+"' , "+taxes+" , "+bill_amount+" , '"+tracking_id+"' , NOW() , '"+payment_mode+"')";
                connection.query(insertOrderQuery, function(insertOrderErr, insertOrderResult) {
                    if(insertOrderErr) {
                        res.status(200).send({
                            error : true,
                            message : insertOrderErr,
                            //check : 2
                        });
                    }
                    else if(insertOrderResult.affectedRows != 0) {
                        var clearCartQuery = "UPDATE cart SET merchant_id = NULL, products_id = NULL, qty = NULL WHERE user_id = "+user_id+" AND cart_id = "+cart_id+"";
                        connection.query(clearCartQuery, function(clearCartErr, clearCartResult) {
                            if(clearCartErr) {
                                res.status(200).send({
                                    error : true,
                                    message : clearCartErr,
                                    //check : 3
                                });
                            }
                            else if(clearCartResult.changedRows != 0) {

                                // Send notification to that merchant about this order

                                var notificationQuery = "SELECT merchants.*, users.name FROM merchants LEFT JOIN users ON users.user_id = "+user_id+" WHERE merchants.merchant_id = "+merchant_id+"";
                                connection.query(notificationQuery, function(notificationErr, notificationResult) {
                                    if(notificationErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : notificationErr
                                        });
                                    }
                                    else if(notificationResult.length != 0) {

                                        // Check device type and send notifications
                                        if(notificationResult[0].os_type.search('droid') != -1) {
                                            Notify.merchantNotificationAndroid(notificationResult[0].fcm_id, "Received an Order", ""+notificationResult[0].name+" has placed an order in your store", {order_id : insertOrderResult.insertId});
                                        }
                                        
                                         else if(notificationResult[0].os_type.search('IOS') != -1) {
                                            Notify.merchantNotificationIOS(notificationResult[0].fcm_id, "Received an Order", ""+notificationResult[0].name+" has placed an order in your store", {order_id : insertOrderResult.insertId});
                                         }

                                         setTimeout(function() { 
                                            res.status(201).send({
                                                error : false,
                                                status : 'Waiting for Merchant Approval',
                                                order_id : insertOrderResult.insertId,
                                                tracking_id : tracking_id,
                                                user_id : user_id,
                                                merchant_id : merchant_id,
                                                products : products,
                                                qty : qty,
                                                price : price,
                                                total_price : total_price,
                                                taxes : taxes,
                                                bill_amount : bill_amount,
                                                payment_mode : payment_mode,
                                                //check : 4
                                            });
                                        }, 5000);
                                    }
                                    else {
                                        setTimeout(function() { 
                                            res.status(201).send({
                                                error : false,
                                                status : 'Waiting for Merchant Approval',
                                                order_id : insertOrderResult.insertId,
                                                tracking_id : tracking_id,
                                                user_id : user_id,
                                                merchant_id : merchant_id,
                                                products : products,
                                                qty : qty,
                                                price : price,
                                                total_price : total_price,
                                                taxes : taxes,
                                                bill_amount : bill_amount,
                                                payment_mode : payment_mode,
                                                //check : 4
                                            });
                                        }, 5000);
                                    }
                                });
                            }
                        });                       
                    }
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    message : 'Ordered Products and Cart Mismatch',
                    //check : 5
                });
            }
        }
    });

});




// Request for Return

router.post('/orderReturnRequest', function(req, res) {

    var user_id = req.body.user_id;
    var order_id = req.body.order_id;
    var tracking_id = req.body.tracking_id;

    var returnRequestQuery = "UPDATE orders SET order_status = 7 WHERE order_id = "+order_id+" AND user_id = "+user_id+" AND tracking_id = '"+tracking_id+"'";
    connection.query(returnRequestQuery, function(returnRequestErr, returnRequestResult) {
        if(returnRequestErr) {
            res.status(200).send({
                error : true,
                message : returnRequestErr
            });
        }
        else if(returnRequestResult.affectedRows != 0) {
            res.status(201).send({
                error : false,
                status : 'Requested for return',
                order_id : order_id,
                tracking_id : tracking_id
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Check inputs properly...'
            });
        }
    });

});    




// Cancel an Order placed by User

router.post('/cancelOrder', function(req, res) {

    var user_id = req.body.user_id;
    var order_id = req.body.order_id;
    var tracking_id = req.body.tracking_id;
    var user_email = req.body.user_email;
    var mailFlag = 0;
    var merchant_email;

    // Get merchant Email through order
    var merchantEmailQuery = "SELECT orders.merchant_id,merchants.merchant_email FROM orders LEFT JOIN merchants ON orders.merchant_id = merchants.merchant_id WHERE orders.order_id = "+order_id+"";
    connection.query(merchantEmailQuery, function(merchantEmailErr, merchantEmailResult) {
        if(!merchantEmailErr) {
            setMerchantEmail(merchantEmailResult[0].merchant_email);
        }
    });

    var cancelOrderQuery = "UPDATE orders SET order_status = 5 WHERE order_id = "+order_id+" AND user_id = "+user_id+" AND tracking_id = '"+tracking_id+"'";
    connection.query(cancelOrderQuery, function(cancelOrderErr, cancelOrderResult) {
        if(cancelOrderErr) {
            res.status(200).send({
                error : true,
                message : cancelOrderErr
            });
        }
        else if(cancelOrderResult.affectedRows != 0) {

            //Send Mail to user regarding this order placement...

            var fetchOrderDetailsUrl = "http://52.43.215.200:8080/pyg/user/getOrderById/?orderId="+order_id+"";
            request(fetchOrderDetailsUrl, {json : true}, function(err,result,body) {
                if(err) {
                    mailFlag = 0;
                }
                else if(!err && result.statusCode != 404) {
                    var products = new Array();
                    body.order.products.forEach(function(item) {
                        temp_arr = [
                            item.product_name ,
                            item.quantification,
                            item.ordered_qty,
                            item.price,
                            item.total_price
                        ];
                        products.push(temp_arr + " ; ");
                    });

                    var html = "<p><strong>Order Details</strong></p><br><br><p>Shop Name : "+body.order.shop_name+"<br><br><strong>Products(Name, Quantification, Ordered Quantity, Price, Total Price)</strong>: "+products.toString()+"<br><br><strong>Cost</strong><br><br>Bill Amount : "+body.order.bill_amount+"<br><br>Tracking ID : "+body.order.tracking_id+"<br><br>Order Time : "+body.order.order_time+"<br><br>Collecting TIme : "+body.order.collecting_time+"<br><br>Payment Mode : "+body.order.payment_mode+"<br><br>Order Status : "+body.order.order_status_name+"</p><br><br><strong>Thanks for shopping with PYG</strong>";
                    Mail.sendMail(user_email, "PYG Order Cancellation", html);

                    //Send to merchant also
                    merchant_email = setMerchantEmail(1);
                    var merchantHtml = "<p><strong>Order Details</strong></p><br><br><p>User Email: "+user_email+"<br><br><strong>Products(Name, Quantification, Ordered Quantity, Price, Total Price)</strong>: "+products.toString()+"<br><br><strong>Cost</strong><br><br>Bill Amount : "+body.order.bill_amount+"<br><br>Tracking ID : "+body.order.tracking_id+"<br><br>Order Time : "+body.order.order_time+"<br><br>Collecting TIme : "+body.order.collecting_time+"<br><br>Payment Mode : "+body.order.payment_mode+"<br><br>Order Status : "+body.order.order_status_name+"</p><br><br><strong>Thanks for shopping with PYG</strong>";
                    Mail.sendMail(merchant_email, "PYG Order Cancellation", merchantHtml);
                    mailFlag = 1;
                }
            });

            setTimeout(function() {
                setMerchantEmail(0);
                res.status(201).send({
                    error : false,
                    status : 'Order Cancelled',
                    order_id : order_id,
                    tracking_id : tracking_id
                });
            }, 10000);
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Check inputs properly...'
            });
        }
    });

});




// Merchant reviewing Order

router.post('/reviewOrder', function(req, res) {

    var merchant_id = req.body.merchant_id;
    var order_id = req.body.order_id;
    var user_id = req.body.user_id;
    var approved = req.body.approved;
    var order_status_id = req.body.order_status_id;
    var collecting_time = req.body.collecting_time;
    
    // If merchant not approved the order
    if(approved == 0) {

        //Update the Status as declined
        var updateDeclineQuery = "UPDATE orders SET order_status_id = "+order_status_id+" WHERE order_id = "+order_id+" AND merchant_id = "+merchant_id+"";
        connection.query(updateDeclineQuery, function(updateDeclineErr, updateDeclineResult) {
            if(updateDeclineErr) {
                res.status(200).send({
                    error : true,
                    message : updateDeclineErr,
                    //check : 1
                });
            }
            else if(updateDeclineResult.changedRows != 0) {
                res.status(201).send({
                    error : true,
                    status : 'Order Declined by Merchant',
                    order_id : order_id,
                    user_id : user_id,
                    merchant_id : merchant_id,
                    //check : 2
                });
            }
        });
    }

    // If merchant Approved
    else if(approved == 1) {

        // Update the status collecting time and approval status
        var updateStatusQuery = "UPDATE orders SET order_status = "+order_status_id+", merchant_approved = 1 WHERE order_id = "+order_id+" AND merchant_id = "+merchant_id+"";
        connection.query(updateStatusQuery, function(updateStatusErr, updateStatusResult) {
            if(updateStatusErr) {
                res.status(200).send({
                    error : true,
                    message : updateStatusErr,
                    //check : 3
                });
            }

            //If merchant approved his inventory should be modified here get the ordered products and ordered qty
            else if(updateStatusResult.changedRows != 0) {
                var orderedDetailsQuery = "SELECT products_id,qty FROM orders WHERE order_id = "+order_id+"";
                connection.query(orderedDetailsQuery, function(orderedDetailsErr, orderedDetailsResult) {
                    if(orderedDetailsErr) {
                        res.status(200).send({
                            error : true,
                            message : orderedDetailsErr,
                            //check : 4
                        });
                    }

                    //If ordered items are more than 1

                        else if((orderedDetailsResult[0].products_id.indexOf(',')) > -1 && (orderedDetailsResult[0].qty.indexOf(',') > -1)) {
                            var getOrderedProductsArr = orderedDetailsResult[0].products_id.split(','); //Split Ordered Products into Array
                            var getOrderedQtyArr = orderedDetailsResult[0].qty.split(',');  //Split Ordered Qty into Array

                            // Get Products and available_qty from inventory
                            var inventoryDetailsQuery = "SELECT inventory_id,products_id,available_qty FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
                            connection.query(inventoryDetailsQuery, function(inventoryDetailsErr, inventoryDetailsResult) {
                                if(inventoryDetailsErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : inventoryDetailsErr,
                                        //check : 5
                                    });
                                }

                                //Deduction Process
                                else {
                                    var getInventoryProductsArr = inventoryDetailsResult[0].products_id.split(','); //Split Inventory products into Array 
                                    var getInventoryQtyArr = inventoryDetailsResult[0].available_qty.split(',');    //Split Inventory available_qty into Array
                                    getOrderedProductsArr.forEach(function(item,index) { // For each ordered product deduct the ordered quantity from available quantity of each ordered product in inventory
                                        var inventoryProductIndex = getInventoryProductsArr.indexOf(item);  // Find index of each ordered item in inventory products
                                        var inventoryAvailableQty = parseInt(getInventoryQtyArr[inventoryProductIndex], 10);    //After finding product convert the corresponding quantity to integer
                                        var orderedQty = parseInt(getOrderedQtyArr[index], 10); //Convert the ordered product quantity to integer based on it's index
                                        inventoryAvailableQty = inventoryAvailableQty-orderedQty;   //Deduction from available_qty
                                        getInventoryQtyArr[inventoryProductIndex] = inventoryAvailableQty;    //Assign the modified qty to that place
                                    });
                                    var inventoryProductsStr = getInventoryProductsArr.join(',');   
                                    var inventoryQtyStr = getInventoryQtyArr.join(',');
                                    
                                    var updatedInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+inventoryProductsStr+"', available_qty = '"+inventoryQtyStr+"' WHERE merchant_id = "+merchant_id+"";
                                    connection.query(updatedInventoryQuery, function(updatedInventoryErr, updatedInventoryResult) {
                                        if(updatedInventoryErr) {
                                            res.status(200).send({
                                                error : true,
                                                message : updatedInventoryErr,
                                                //check : 6
                                            });
                                        }
                                        else if(updatedInventoryResult.changedRows != 0) {
                                            res.status(201).send({
                                                error : false,
                                                status : 'Inventory Modified',
                                                merchant_id : merchant_id,
                                                inventory_id : inventoryDetailsResult[0].inventory_id,
                                                products : inventoryProductsStr,
                                                available_qty : inventoryQtyStr,
                                                //check : 7
                                            });
                                        }
                                    });
                                }
                            });       
                        }

                        //If ordered item is only 1
                        else if((orderedDetailsResult[0].products_id.indexOf(',') == -1) && (orderedDetailsResult[0].qty.indexOf(',') == -1)) {
                            var singleProductInventoryDetailsQuery = "SELECT inventory_id,products_id,available_qty FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
                            connection.query(singleProductInventoryDetailsQuery, function(singleProductInventoryErr, singleProductInventoryResult) {
                                if(singleProductInventoryErr) {
                                    res.status(200).send({
                                        error : false,
                                        message : singleProductInventoryErr,
                                        //check : 8
                                    });
                                }

                                //Deduction Process
                                else {
                                    var getInventoryProducts_Arr = singleProductInventoryResult[0].products_id.split(',');  //Split Inventory Products into array in inventory
                                    var getInventoryQty_Arr = singleProductInventoryResult[0].available_qty.split(','); //Split available quantity into array in inventory
                                    var orderedProductInventoryIndex = getInventoryProducts_Arr.indexOf(orderedDetailsResult[0].products_id);   //Get index of the ordered product in inventory
                                    var orderedQty_AvailabilityInInventory = parseInt(getInventoryQty_Arr[orderedProductInventoryIndex], 10);   //Convert the corresponding qty in inventory to integer 
                                    var orderedQtyInt = parseInt(orderedDetailsResult[0].qty);  //Convert the ordered qty to integer
                                    orderedQty_AvailabilityInInventory = orderedQty_AvailabilityInInventory-orderedQtyInt;  //Deduction
                                    getInventoryQty_Arr[orderedProductInventoryIndex] = orderedQty_AvailabilityInInventory; //Replace the modified qty to that place

                                    var singleInventoryProductsStr = getInventoryProducts_Arr.join(',');   
                                    var singleInventoryQtyStr = getInventoryQty_Arr.join(',');
                                    
                                    var singleUpdatedInventoryQuery = "UPDATE merchant_inventory SET products_id = '"+singleInventoryProductsStr+"', available_qty = '"+singleInventoryQtyStr+"' WHERE merchant_id = "+merchant_id+"";
                                    connection.query(singleUpdatedInventoryQuery, function(singleUpdatedInventoryErr, singleUpdatedInventoryResult) {
                                        if(singleUpdatedInventoryErr) {
                                            res.status(200).send({
                                                error : true,
                                                message : singleUpdatedInventoryErr,
                                                //check : 9
                                            });
                                        }
                                        else if(singleUpdatedInventoryResult.changedRows != 0) {
                                            res.status(201).send({
                                                error : false,
                                                status : 'Inventory Modified',
                                                merchant_id : merchant_id,
                                                inventory_id : singleProductInventoryResult[0].inventory_id,
                                                products : singleInventoryProductsStr,
                                                available_qty : singleInventoryQtyStr,
                                                //check : 10
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    
                });
            }
        });
    }

});




// Feedback Module

router.post('/userFeedback', function(req, res) {

    var user_id = req.body.user_id;
    var order_id = req.body.order_id;
    var merchant_id = req.body.merchant_id;
    var rating = req.body.rating;
    var review = req.body.review;

    var insertFeedbackQuery = "INSERT INTO feedback(order_id,user_id,merchant_id,rating,review) VALUES("+order_id+" , "+user_id+" , "+merchant_id+" , "+rating+" , '"+review+"')";
    connection.query(insertFeedbackQuery, function(insertFeedbackErr, insertFeedbackResult) {
        if(insertFeedbackErr) {
            res.status(200).send({
                error : true,
                message : insertFeedbackErr,
            });
        }
        else if(insertFeedbackResult.affectedRows != 0) {
            res.status(201).send({
                error : false,
                status : 'Thanks for your valuable feedback',
                user_id : user_id,
                order_id : order_id,
                merchant_id : merchant_id,
                rating : rating,
                review : review
            });
        }
    });

});




// Update last login

router.post('/lastLogin', function(req, res) {

    var user_id = req.body.user_id;
    var lastlogin = req.body.lastlogin;

    var updateLastLoginQuery = "UPDATE users SET lastlogin = '"+lastlogin+"' WHERE user_id = "+user_id+"";
    connection.query(updateLastLoginQuery, function(updateLastLoginErr, updateLastLoginResult) {
        if(updateLastLoginErr) {
            res.status(200).send({
                error : true,
                message : updateLastLoginErr
            });
        }
        else if(updateLastLoginResult.affectedRows != 0) {
            res.status(201).send({
                error : false,
                status : 'Last login updated',
                lastLogin : lastlogin
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Problem in updating last login'
            });
        }
    });

});




// Change FCM ID on dynamic generation

router.post('/updateFCM', function(req, res) {

    var user_id = req.body.user_id;
    var os_type = req.body.os_type;
    var fcm_id = req.body.fcm_id;

    var fcmUpdateQuery = "UPDATE users SET fcm_id = '"+fcm_id+"' WHERE user_id = "+user_id+" AND os_type LIKE '"+os_type+"'";
    connection.query(fcmUpdateQuery, function(fcmUpdateErr, fcmUpdateResult) {
        if(fcmUpdateErr) {
            res.status(200).send({
                error : true,
                message : fcmUpdateErr
            });
        }
        else if(fcmUpdateResult.affectedRows != 0) {
            res.status(201).send({
                error : false,
                status : "FCM ID Updated",
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "Problem in updating FCM ID"
            })
        }
    });

});




// Registering routes to our API's
// all routes will be prefixed with /pyg
app.use('/pyg/user', router);

// Disable Cache
app.use(function(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
});

app.set('etag', false);

// Start the server
app.listen(port);




// Set Nearby Merchants (Used in API named 'getNearbyMerchants')

var nearbyMerchantDetails = [];
function setMerchants(merchantDetails) {
    
    if(merchantDetails == 1) {
        return nearbyMerchantDetails;
    }
    else if(merchantDetails == 0) {
        nearbyMerchantDetails = [];
    }
    else {
        nearbyMerchantDetails.push(merchantDetails);
    }

}




// Get Categories selling by merchants in their shop by passing Merchant Id(Used in API named 'getMerchantCategories')

var categoriesWithMerchant = [];
function getcategoriesByMerchant(categories) {

    if(categories == 1) {
        return categoriesWithMerchant;
    }
    else if(categories == 0) {
        categoriesWithMerchant = [];
    }
    else {
        categoriesWithMerchant.push(categories);
    }

}




// Set Product Details passed from query function of 'getMerchantProducts' API

var productDetails = [];
function setProductDetails(products) {
    if(products == 1) {
        return productDetails;
    }
    else if(products == 0) {
        productDetails = [];
    }
    else {
        productDetails.push(products);
    }
}




// View the items available in Cart
var viewCartDetails = [];
function setCartDetails(cart,viewDetails) {    
    if(cart == 1) {
        return viewCartDetails;
    }
    else if(cart == 0) {
        viewCartDetails = [];
    }
    else {
            viewDetails.add(cart);
            return viewDetails;
    }

}




// Get product details for ordered items
var orderedProductDetails = [];
function orderedProducts(product) {

    if(product == 1) {
        return orderedProductDetails;
    }
    else if(product == 0) {
        orderedProductDetails = [];
    }
    else {
        orderedProductDetails.push(product);
    }

}




// Get results from other URLs and pass to required array
var otherAPIResults = [];
function getResultFromOtherAPI(content) {

    if(content == 1) {
        return otherAPIResults;
    }
    else if(content == 0) {
        otherAPIResults = [];
    }
    else {
        otherAPIResults.push(content);
    }

}




// Set Email Address from placeOrder
var merchant_email;
function setMerchantEmail(email) {
    
    if(email == 1) {
        return merchant_email;
    }
    else if(email == 0) {
        merchant_email = "";
    }
    else {
        merchant_email = email;
    }
}




// Encryption of passwords

function encrypt(data) {
    const crypted = cryptr.encrypt(data);
    return crypted;
}




// Decryption of passwords

function decrypt(data) {
    var decrypted = cryptr.decrypt(data);
    return decrypted;
}




// Generating OTP for Mail Verification

function otpGenerator() {
    var otp = Math.floor(1000 + Math.random() * 9000);
    return otp;
}