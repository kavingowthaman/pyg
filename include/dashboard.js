// PYG v1.0

// Required Modules and Files
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('secretkey');
const Geo = require('geo-nearby');
const randomstring = require('randomstring');
const Excel = require('xlsx');
const request = require('request');
const aws4 = require('aws4');
const asn1 = require('asn1');
const cors = require('cors');
const objectAssign = require('object-assign');
const Mail = require('./SendMail.js');
const Notify = require('./Notifications.js');
const User = require('../index.js');
const Merchant = require('./merchant.js');
const DbConnect = require('./DbConnect.js');
const connection = new DbConnect();

module.exports = {

dashboard : function() {
    
//URL Encoding
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

//Access Control Mechanisms (Dashboard Access)
app.use(cors());
    
//Routing API's
var port = process.env.PORT || 8800;
var router = express.Router();
    
    
    
    
//API's in PYG MERCHANT as follows

// Sample API

router.post('/loginAdmin', function(req, res) {

    var admin_email = req.body.admin_email;
    var admin_password = req.body.admin_password;

    var adminLoginQuery = "SELECT admin_email,password FROM admin WHERE admin_email LIKE '"+admin_email+"'";
    connection.query(adminLoginQuery, function(adminLoginErr, adminLoginResult) {
        if(adminLoginErr) {
            res.status(200).send({
                error : true,
                message : adminLoginErr
            });
        }
        else if(adminLoginResult.length != 0) {
            if((adminLoginResult[0].admin_email == admin_email) && (adminLoginResult[0].password == admin_password)) {
                res.status(200).send({
                    error : false,
                    status : "Login Successful"
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    message : "Login Failed"
                });
            }
        }
        else {
            res.status(200).send({
                error : true,
                message : "Incorrect User ID"
            });
        }
    });

});




// Count of users

router.get('/getCounts', function(req, res) {

    var userCount = 0;
    var merchantCount = 0;
    var orderCount = 0;

    var userQuery = "SELECT COUNT(DISTINCT user_id) as userCount FROM users";
    connection.query(userQuery, function(userErr, userResult) {
        if(userErr) {
            res.status(200).send({
                error : true,
                message : userErr
            });
        }
        else if(userResult) {
            userCount = userResult[0].userCount;
        }
    });

    var merchantQuery = "SELECT COUNT(DISTINCT merchant_id) as merchantCount FROM merchants";
    connection.query(merchantQuery, function(merchantErr, merchantResult) {
        if(merchantErr) {
            res.status(200).send({
                error : true,
                message : merchantErr
            });
        }
        else if(merchantResult) {
            merchantCount = merchantResult[0].merchantCount;
        }
    });

    var orderQuery = "SELECT COUNT(DISTINCT order_id) as orderCount FROM orders";
    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult) {
            orderCount = orderResult[0].orderCount;
        }
    });

    var completedQuery = "SELECT COUNT(DISTINCT order_id) as completedorder, CAST(SUM(bill_amount) AS DECIMAL(15,4)) as CompletedOrderAmmount FROM orders where order_status = '4'";
    connection.query(completedQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult) {
            completedCount = orderResult[0].completedorder;
            CompletedOrderAmmount = orderResult[0].CompletedOrderAmmount;
        }
    });

    var pendingQuery = "SELECT COUNT(DISTINCT order_id) as pendingorder, CAST(SUM(bill_amount) AS DECIMAL(15,4)) as pendingOrderAmmount FROM orders where order_status IN (1,2,3,9)";
    connection.query(pendingQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult) {
            pendingCount = orderResult[0].pendingorder;
            pendingOrderAmmount = orderResult[0].pendingOrderAmmount;
        }
    });

    var cancelledQuery = "SELECT COUNT(DISTINCT order_id) as cancelledorder, CAST(SUM(bill_amount) AS DECIMAL(15,4)) as cancelledOrderAmmount FROM orders where order_status IN (5,6,7,8)";
    connection.query(cancelledQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult) {
            cancelledCount = orderResult[0].cancelledorder;
            cancelledOrderAmmount = orderResult[0].cancelledOrderAmmount;
        }
    });



    setTimeout(function() {
        res.status(200).send({
            error : false,
            userCount : userCount,
            merchantCount : merchantCount,
            orderCount : orderCount,
            completedOrderCount : completedCount,
            CompletedOrderAmmount : CompletedOrderAmmount,
            pendingOrderCount :  pendingCount,
            pendingOrderAmmount : pendingOrderAmmount,
            cancelledOrderCount : cancelledCount,
            cancelledOrderAmmount : cancelledOrderAmmount
        });
    }, 2000);

});


//get merchant order details Revised
router.get('/getMerchantOrderDetailsRevised', function(req, res){
    var merchant_id = req.query.merchant_id;

    var allOrders = [];

    var OrdersQuery = "SELECT * FROM orders WHERE merchant_id="+merchant_id+"";
    connection.query(OrdersQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult != null && orderResult.length != 0) {
            
            for(var i=0;i<orderResult.length;i++){
                    if(orderResult[i].order_status == 1 || orderResult[i].order_status == 2 || orderResult[i].order_status == 3 || orderResult[i].order_status == 9) {
                        orderResult[i].order_status = "Pending";
                    } else if (orderResult[i].order_status == 4) {
                        orderResult[i].order_status = "Completed";
                    } else if (orderResult[i].order_status == 5 || orderResult[i].order_status == 6 || orderResult[i].order_status == 7 || orderResult[i].order_status == 8) {
                        orderResult[i].order_status = "Cancelled";
                    }


                    allOrders.push(orderResult[i]);
                    //calling async function
                    productDetails();
                    //async function 
                    async function productDetails() {
                        var product = [];
                        //fetchProductInfo takes 3 parameters
                        //Productid, Allorder-array, index value(i) 
                        var result = await fetchProductInfo(orderResult[i].products_id,allOrders,i)                        
                    }
            }
        
            setTimeout(function() {
                //3 Arrays to differentiate between completed, pending and cancelled orders
                let completedArr = [];
                let pendingArr   = [];   
                let cancelledArr = [];

                for(let i=0;i<allOrders.length;i++) {
                    if(allOrders[i].order_status == "Completed") {
                        completedArr.push(allOrders[i]);
                    } else if(allOrders[i].order_status == "Pending") {
                        pendingArr.push(allOrders[i]);
                    } else if(allOrders[i].order_status == "Cancelled") {
                        cancelledArr.push(allOrders[i]);
                    }
                }

                res.status(200).send({
                    error       : false,
                    allOrders   : allOrders,
                    completed   : completedArr,
                    pending     : pendingArr,
                    cancelled   : cancelledArr
                });
            }, 2000);
        }
        else if(orderResult == null || orderResult.length == 0) {
            res.status(200).send({
                error : true,
                message : 'No Orders found'
            });
        }
            
    });

});

// Display all users

router.get('/getAllUsers', function(req, res) {

    var query = "SELECT * FROM users";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                users : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Users found"
            });
        }
    });

});




// Get User by Id

router.get('/getUserById/', function(req, res) {

    var userId = req.query.userId;

    var query = "SELECT * FROM users WHERE user_id = "+userId+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                user_details : result[0]
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'User not found'
            });
        }
    });

});




// Get User by Name

router.get('/getUserByName/', function(req, res) {

    var name = req.query.name;

    var query = "SELECT * FROM users WHERE name LIKE CONCAT('"+name+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                user_details : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'User not found'
            });
        }
    });

});




// Get User by Email

router.get('/getUserByEmail/', function(req, res) {

    var email = req.query.email;

    var query = "SELECT * FROM users WHERE email LIKE CONCAT('"+email+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                user_details : result[0],
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'User not found',
            });
        }
    });

});


// Display all merchants

router.get('/getAllMerchants', function(req, res) {
    var query = "SELECT * FROM merchants";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result) {
            res.status(200).send({
                error : false,
                merchants : result
            });
        }
    });
});




// To get Merchant with their Id

router.get('/getMerchantById/', function(req, res) {

    var merchantId = req.query.merchantId;

    var query = "SELECT * FROM merchants WHERE merchant_id = "+merchantId+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                merchant : result[0]
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Merchant not found'
            });
        }
    });

});




// To get Merchant by their Names

router.get('/getMerchantByName/', function(req, res) {

    var merchantName = req.query.merchantName;

    var merchantQuery = "SELECT * FROM merchants WHERE merchant_name LIKE CONCAT('"+merchantName+"','%')";
    connection.query(merchantQuery, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                merchant : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Merchant not found'
            });
        }
    });

});




// To get Merchants with their Shop Name

router.get('/getMerchantByShop/', function(req, res) {

    var shopName = req.query.shopName;

    var merchantQuery = "SELECT * FROM merchants WHERE shop_name LIKE CONCAT('"+shopName+"','%')";
    connection.query(merchantQuery, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                merchant : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Merchant not found'
            });
        }
    });

});




// To get Merchant with their Email Id

router.get('/getMerchantByEmail/', function(req, res) {

    var merchantEmail = req.query.merchantEmail;

    var merchantQuery = "SELECT * FROM merchants WHERE merchant_email LIKE CONCAT('"+merchantEmail+"','%')";
    connection.query(merchantQuery, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                merchant : result
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'Merchant not found'
            });
        }
    });

});


// Display all Categories

router.get('/getAllCategories', function(req, res) {

    var categoriesQuery = "SELECT * FROM categories";
    connection.query(categoriesQuery, function(categoriesErr, categoriesResult) {
        if(categoriesErr) {
            res.status(200).send({
                error : true,
                message : categoriesErr
            });
        }
        else if(categoriesResult) {
            res.status(200).send({
                error : false,
                categories : categoriesResult
            });
        }
    });

});




// Get category by id

router.get('/getCategoryById/', function(req, res) {

    //Required Params
    var categoryId = req.query.categoryId;

    var categoryDetailQuery = "SELECT * FROM categories WHERE category_id = "+categoryId+"";
    connection.query(categoryDetailQuery, function(categoryDetailsErr, categoryDetailResult) {
        if(categoryDetailsErr) {
            res.status(200).send({
                error : true,
                message : categoryDetailsErr
            });
        }
        else if(categoryDetailResult.length != 0) {
            res.status(200).send({
                error : false,
                category : categoryDetailResult[0]
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "Problem in fetching Category details"
            });
        }
    });

});




// Get Category by Name

router.get('/getCategoryByName/', function(req, res) {

    var categoryName = req.query.categoryName;

    var query = "SELECT * FROM categories WHERE category_name LIKE CONCAT('"+categoryName+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                category : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Category found',
            });
        }
    });

});




// Search Categories available with merchants

router.get('/searchMerchantCategories/', function(req, res) {

    var merchantId = req.query.merchantId;
    var categoryName = req.query.categoryName;

    var urls = new Array("http://52.43.215.200:8800/pyg/CMS/getCategoryByName/?categoryName="+categoryName+"" , "http://52.43.215.200:8800/pyg/CMS/getMerchantCategories/?merchantId="+merchantId+"");
    var urlResult1;
    var urlResult2;
    var searchResult = new Array();

    urls.forEach(function(url, index) {
        if(index == 0) { 
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult1 = body.category;
                }
            });
        }
        else if(index == 1) {
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult2 = body.categories;
                }
            });
        }
    });

    setTimeout(function() {
        urlResult1.forEach(function(commonResult) {
            urlResult2.forEach(function(merchantResult) {
                if(commonResult.category_name == merchantResult.category_name) {
                    getResultFromOtherAPI(merchantResult);                                        
                }
            });
        });
        searchResult = getResultFromOtherAPI(1);
        setTimeout(function() {
            getResultFromOtherAPI(0);
            if(searchResult.length != 0) {
                res.status(200).send({
                    error : false,
                    searchResults : searchResult
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    searchResults : "No Categories Found"
                });
            }
        }, 1000);
    }, 2000);

});




// Display all Products

router.get('/getAllProducts', function(req, res) {

    var productsQuery = "SELECT * FROM products";
    connection.query(productsQuery, function(productsErr, productsResult) {
        if(productsErr) {
            res.status(200).send({
                error : true,
                message : productsErr
            });
        }
        else if(productsResult) {
            res.status(200).send({
                error : false,
                products : productsResult
            });
        }
    });

});




// Get Category by Id

router.get('/getProductById/', function(req, res) {

    var productId = req.query.productId;

    var query = "SELECT * FROM products WHERE product_id = "+productId+"";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                product : result[0],
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Product found',
            })
        }
    });

});




// Get Category by Name

router.get('/getProductByName/', function(req, res) {

    var productName = req.query.productName;

    var query = "SELECT * FROM products WHERE product_name LIKE CONCAT('"+productName+"','%')";
    connection.query(query, function(err, result) {
        if(err) {
            res.status(200).send({
                error : true,
                message : err,
            });
        }
        else if(result.length != 0) {
            res.status(200).send({
                error : false,
                product : result,
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : 'No Product found',
            });
        }
    });
});




// Search Products available with merchants

router.get('/searchMerchantProducts/', function(req, res) {

    var merchantId = req.query.merchantId;
    var categoryId = req.query.categoryId;
    var productName = req.query.productName;

    var urls = new Array("http://52.43.215.200:8800/pyg/CMS/getProductByName/?productName="+productName+"" , "http://52.43.215.200:8800/pyg/CMS/getMerchantProducts/?merchantId="+merchantId+"&categoryId="+categoryId+"");
    var urlResult1;
    var urlResult2;
    var searchResult = new Array();

    urls.forEach(function(url, index) {
        if(index == 0) { 
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult1 = body.product;
                }
            });
        }
        else if(index == 1) {
            request(url, {json : true}, function(err, result, body) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : "Problem in fetching result from "+url+"",
                    });
                }
                else if(!err && result.statusCode != 404) {
                    urlResult2 = body.products;
                }
            });
        }
    });

    setTimeout(function() {
        urlResult1.forEach(function(commonResult) {
            urlResult2.forEach(function(merchantResult) {
                if(commonResult.product_name == merchantResult.product_name) {
                    getResultFromOtherAPI(merchantResult);
                }
            });
        });
        searchResult = getResultFromOtherAPI(1);
        setTimeout(function() {
            getResultFromOtherAPI(0);
            if(searchResult.length != 0) {
                res.status(200).send({
                    error : false,
                    searchResults : searchResult
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    searchResults : "No Categories Found"
                });
            }
        }, 1000);
    }, 2000);

});




// Get all products based on category id

router.get('/getProductsByCategoryId/', function(req, res) {

    // Required Params
    var categoryId = req.query.categoryId;

    var productQuery = "SELECT * FROM products WHERE category_id = "+categoryId+"";
    connection.query(productQuery, function(productErr, productResult) {
        if(productErr) {
            res.status(200).send({
                error : true,
                message : productErr
            });
        }
        else if(productResult.length != 0) {
            res.status(200).send({
                error : false,
                products : productResult
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Products"
            });
        }
    });

});


// Get all products count of individual category

router.get('/getProductsCount', function(req, res) {

    var productQuery = "SELECT categories.category_id AS categoryId, category_name AS categoryName, category_image as categoryImage, COUNT(products.product_id) productCount FROM products RIGHT JOIN categories ON products.category_id = categories.category_id GROUP BY categories.category_id HAVING COUNT(*)>=0";

    connection.query(productQuery, function(productErr, productResult) {
        if(productErr) {
            res.status(200).send({
                error : true,
                message : productErr
            });
        }
        else if(productResult.length!=0) {
            res.status(200).send({
                error : false,
                categories : productResult
            });
        }
    });
});


// Get all products based on category name

router.get('/getProductsByCategoryName/', function(req, res) {

    // Required Params
    var categoryName = req.query.categoryName;

    var categoryQuery = "SELECT DISTINCT category_id FROM categories WHERE category_name LIKE CONCAT('"+categoryName+"','%') LIMIT 1";
    connection.query(categoryQuery, function(categoryErr, categoryResult) {
        if(categoryErr) {
            res.status(200).send({
                error : true,
                message : categoryErr
            });
        }
        else if(categoryResult.length != 0) {
            
            //Pick products
            var productQuery = "SELECT * FROM products WHERE category_id = "+categoryResult[0].category_id+"";
            connection.query(productQuery, function(productErr, productResult) {
                setTimeout(function() {
                    if(productErr) {
                        res.status(200).send({
                            error : true,
                            message : productErr
                        });
                    }
                    else if(productResult.length != 0) {
                        res.status(200).send({
                            error : false,
                            products : productResult
                        });
                    }
                    else {
                        res.status(200).send({
                            error : true,
                            message : "No Product found"
                        });
                    }
                }, 1000);
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Categories found"
            });
        }
    });

});


// Display all orders

router.get('/getAllOrders', function(req, res) {

    var ordersQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status";
    connection.query(ordersQuery, function(ordersErr, ordersResult) {
        if(ordersErr) {
            res.status(200).send({
                error : true,
                message : ordersErr
            });
        }
        else if(ordersResult) {
            res.status(200).send({
                error : false,
                orders : ordersResult
            });
        }
    });

});




// Get Order through Order Id
router.get('/getOrderById/', function(req, res) {

    var orderId = req.query.orderId;
    var orderQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status WHERE orders.order_id = "+orderId+"";

    connection.query(orderQuery, function(orderErr, orderResult) {
        if(orderErr) {
            res.status(200).send({
                error : true,
                message : orderErr
            });
        }
        else if(orderResult != null && orderResult.length != 0) {

            //Fetch Product Details
            
            //If ordered products is more than 1
            if(orderResult[0].products_id.indexOf(',') != -1) {

                var orderedProductArr = orderResult[0].products_id.split(',');
                var orderedQtyArr = orderResult[0].qty.split(',');
                var orderedProductsPriceArr = orderResult[0].price.split(',');
                var orderedProductsTotalPriceArr = orderResult[0].total_price.split(',');

                var products = new Array();

                orderedProductArr.forEach(function(item, index) {
                    var product_id = parseInt(item, 10);
                    var productDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+product_id+"";
                    connection.query(productDetailsQuery, function(productDetailsErr, productDetailsResult) {
                        if(productDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : productDetailsErr
                            });
                        }
                        else {
                            temp_arr = {
                                product_id : product_id,
                                product_name : productDetailsResult[0].product_name,
                                product_description : productDetailsResult[0].product_description,
                                sku : productDetailsResult[0].sku,
                                quantification : productDetailsResult[0].quantification,
                                ordered_qty : orderedQtyArr[index],
                                price : orderedProductsPriceArr[index],
                                total_price : orderedProductsTotalPriceArr[index]
                            };
                            module.exports.orderedProducts(temp_arr);
                        }
                    }); 
                });
                var otherInfoQuery = "SELECT merchants.shop_name, merchants.address, feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                    connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                    if(otherInfoErr) {
                        res.status(200).send({
                            error : true,
                            message : otherInfoErr,
                        });
                    }
                    else {
                        products = module.exports.orderedProducts(1);
                        setTimeout(function() {
                            module.exports.orderedProducts(0);
                            res.status(200).send({
                                error : false,
                                order : {
                                    order_id : orderResult[0].order_id,
                                    user_id : orderResult[0].user_id,
                                    merchant_id : orderResult[0].merchant_id,
                                    shop_name : otherInfoResult[0].shop_name,
                                    address : otherInfoResult[0].address,
                                    products : products,
                                    taxes : orderResult[0].taxes,
                                    bill_amount : orderResult[0].bill_amount,
                                    tracking_id : orderResult[0].tracking_id,
                                    order_time : orderResult[0].order_time,
                                    collecting_time : orderResult[0].collecting_time,
                                    payment_mode : orderResult[0].payment_mode,
                                    payment_status : orderResult[0].payment_status,
                                    transaction_id : orderResult[0].transaction_id,
                                    merchant_approved : orderResult[0].merchant_approved,
                                    order_status : orderResult[0].order_status,
                                    order_status_name : orderResult[0].order_status_name,
                                    rating : otherInfoResult[0].rating,
                                    review : otherInfoResult[0].review
                                }
                            });
                        }, 2000);
                    }
                });
            }

            // If order contains only 1 item
            else if(orderResult[0].products_id.indexOf(',') == -1) {
                var singleProduct_id = parseInt(orderResult[0].products_id);
                var singleProductDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+singleProduct_id+"";
                    connection.query(singleProductDetailsQuery, function(singleproductDetailsErr, singleproductDetailsResult) {
                        if(singleproductDetailsErr) {
                            res.status(200).send({
                                error : true,
                                message : singleproductDetailsErr
                            });
                        }
                        else {
                            temp_arr = [{
                                product_id : singleProduct_id,
                                product_name : singleproductDetailsResult[0].product_name,
                                product_description : singleproductDetailsResult[0].product_description,
                                sku : singleproductDetailsResult[0].sku,
                                quantification : singleproductDetailsResult[0].quantification,
                                ordered_qty : orderResult[0].qty,
                                price : orderResult[0].price,
                                total_price : orderResult[0].total_price
                            }];

                            var otherInfoQuery = "SELECT merchants.shop_name, merchants.address, feedback.rating,feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                            connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                                if(otherInfoErr) {
                                    res.status(200).send({
                                        error : true,
                                        message : otherInfoErr
                                    });
                                }
                                else {
                                    res.status(200).send({
                                        error : false,
                                        order : {
                                            order_id : orderResult[0].order_id,
                                            user_id : orderResult[0].user_id,
                                            merchant_id : orderResult[0].merchant_id,
                                            shop_name : otherInfoResult[0].shop_name,
                                            address : otherInfoResult[0].address,
                                            products : temp_arr,
                                            taxes : orderResult[0].taxes,
                                            bill_amount : orderResult[0].bill_amount,
                                            tracking_id : orderResult[0].tracking_id,
                                            order_time : orderResult[0].order_time,
                                            collecting_time : orderResult[0].collecting_time,
                                            payment_mode : orderResult[0].payment_mode,
                                            payment_status : orderResult[0].payment_status,
                                            transaction_id : orderResult[0].transaction_id,
                                            merchant_approved : orderResult[0].merchant_approved,
                                            order_status : orderResult[0].order_status,
                                            order_status_name : orderResult[0].order_status_name,
                                            rating : otherInfoResult[0].rating,
                                            review : otherInfoResult[0].review
                                        }
                                    });
                                }
                            });
                        }
                    }); 

            } 
        }
        else if(orderResult == null || orderResult.length == 0) {
            res.status(200).send({
                error : false,
                message : 'No Orders found'
            });
        }
    });

});




// Get Order through Tracking Id
router.get('/getOrderByTrackingId/', function(req, res) {

    var trackingId = req.query.trackingId;
    var orderQuery = "SELECT orders.*, order_status.order_status_name FROM orders LEFT JOIN order_status ON order_status.order_status_id = orders.order_status WHERE orders.tracking_id = 'PYG#ID"+trackingId+"'";

    connection.query(orderQuery, function(orderErr, orderResult) {
        setTimeout(function() {
            if(orderErr) {
                res.status(200).send({
                    error : true,
                    message : orderErr
                });
            }
            else if(orderResult != null && orderResult.length != 0) {
                //Fetch Product Details
                
                //If ordered products is more than 1
                if(orderResult[0].products_id.indexOf(',') != -1) {

                    var orderedProductArr = orderResult[0].products_id.split(',');
                    var orderedQtyArr = orderResult[0].qty.split(',');
                    var orderedProductsPriceArr = orderResult[0].price.split(',');
                    var orderedProductsTotalPriceArr = orderResult[0].total_price.split(',');

                    var products = new Array();

                    orderedProductArr.forEach(function(item, index) {
                        var product_id = parseInt(item, 10);
                        var productDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+product_id+"";
                        connection.query(productDetailsQuery, function(productDetailsErr, productDetailsResult) {
                            if(productDetailsErr) {
                                res.status(200).send({
                                    error : true,
                                    message : productDetailsErr
                                });
                            }
                            else {
                                temp_arr = {
                                    product_id : product_id,
                                    product_name : productDetailsResult[0].product_name,
                                    product_description : productDetailsResult[0].product_description,
                                    sku : productDetailsResult[0].sku,
                                    quantification : productDetailsResult[0].quantification,
                                    ordered_qty : orderedQtyArr[index],
                                    price : orderedProductsPriceArr[index],
                                    total_price : orderedProductsTotalPriceArr[index]
                                };
                                module.exports.orderedProducts(temp_arr);
                            }
                        }); 
                    });
                    var otherInfoQuery = "SELECT merchants.shop_name, feedback.rating, feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                    connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                        if(otherInfoErr) {
                            res.status(200).send({
                                error : true,
                                message : otherInfoErr,
                            });
                        }
                        else {
                            products = module.exports.orderedProducts(1);
                            setTimeout(function() {
                                module.exports.orderedProducts(0);
                                res.status(200).send({
                                    error : false,
                                    order : {
                                        order_id : orderResult[0].order_id,
                                        user_id : orderResult[0].user_id,
                                        merchant_id : orderResult[0].merchant_id,
                                        shop_name : otherInfoResult[0].shop_name,
                                        products : products,
                                        taxes : orderResult[0].taxes,
                                        bill_amount : orderResult[0].bill_amount,
                                        tracking_id : orderResult[0].tracking_id,
                                        order_time : orderResult[0].order_time,
                                        collecting_time : orderResult[0].collecting_time,
                                        payment_mode : orderResult[0].payment_mode,
                                        payment_status : orderResult[0].payment_status,
                                        transaction_id : orderResult[0].transaction_id,
                                        merchant_approved : orderResult[0].merchant_approved,
                                        order_status : orderResult[0].order_status,
                                        order_status_name : orderResult[0].order_status_name,
                                        rating : otherInfoResult[0].rating,
                                        review : otherInfoResult[0].review
                                    }
                                });
                            }, 2000);
                        }
                    });
                }

                // If order contains only 1 item
                else if(orderResult[0].products_id.indexOf(',') == -1) {
                    var singleProduct_id = parseInt(orderResult[0].products_id);
                    var singleProductDetailsQuery = "SELECT product_name,product_image,product_description,sku,quantification FROM products WHERE product_id = "+singleProduct_id+"";
                        connection.query(singleProductDetailsQuery, function(singleproductDetailsErr, singleproductDetailsResult) {
                            if(singleproductDetailsErr) {
                                res.status(200).send({
                                    error : true,
                                    message : singleproductDetailsErr
                                });
                            }
                            else {
                                temp_arr = [{
                                    product_id : singleProduct_id,
                                    product_name : singleproductDetailsResult[0].product_name,
                                    product_description : singleproductDetailsResult[0].product_description,
                                    sku : singleproductDetailsResult[0].sku,
                                    quantification : singleproductDetailsResult[0].quantification,
                                    ordered_qty : orderResult[0].qty,
                                    price : orderResult[0].price,
                                    total_price : orderResult[0].total_price
                                }];

                                var otherInfoQuery = "SELECT merchants.shop_name, feedback.rating, feedback.review FROM merchants LEFT JOIN feedback ON feedback.order_id = "+orderResult[0].order_id+" WHERE merchants.merchant_id = "+orderResult[0].merchant_id+"";
                                connection.query(otherInfoQuery, function(otherInfoErr, otherInfoResult) {
                                    if(otherInfoErr) {
                                        res.status(200).send({
                                            error : true,
                                            message : otherInfoErr,
                                        });
                                    }
                                    else {
                                        res.status(200).send({
                                            error : false,
                                            order : {
                                                order_id : orderResult[0].order_id,
                                                user_id : orderResult[0].user_id,
                                                merchant_id : orderResult[0].merchant_id,
                                                shop_name : otherInfoResult[0].shop_name,
                                                products : temp_arr,
                                                taxes : orderResult[0].taxes,
                                                bill_amount : orderResult[0].bill_amount,
                                                tracking_id : orderResult[0].tracking_id,
                                                order_time : orderResult[0].order_time,
                                                collecting_time : orderResult[0].collecting_time,   
                                                payment_mode : orderResult[0].payment_mode,
                                                payment_status : orderResult[0].payment_status,
                                                transaction_id : orderResult[0].transaction_id,
                                                merchant_approved : orderResult[0].merchant_approved,
                                                order_status : orderResult[0].order_status,
                                                order_status_name : orderResult[0].order_status_name,
                                                rating : otherInfoResult[0].rating,
                                                review : otherInfoResult[0].review
                                            }
                                        });
                                    }
                                });
                            }
                        }); 
                } 
            }
            else if(orderResult == null || orderResult.length == 0) {
                res.status(200).send({
                    error : false,
                    message : 'No Orders found'
                });
            }
        }, 2000);
    });
    
});




// Get Orders By User

router.get('/getOrdersByUser/', function(req, res) {

    //Required Params
    var userId = req.query.userId;

    var userOrderQuery = "SELECT * FROM orders WHERE user_id = "+userId+"";
    connection.query(userOrderQuery, function(userOrderErr, userOrderResult) {
        if(userOrderErr) {
            res.status(200).send({
                error : true,
                message : userOrderErr
            });
        }
        else if(userOrderResult.length != 0) {
            res.status(200).send({
                error : false,
                orders : userOrderResult
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });

});




// Get orders by Merchants

router.get('/getOrdersByMerchant/', function(req, res) {

    //Required Params
    var merchantId = req.query.merchantId;

    var merchantOrderQuery = "SELECT * FROM orders WHERE merchant_id = "+merchantId+"";
    connection.query(merchantOrderQuery, function(merchantOrderErr, merchantOrderResult) {
        if(merchantOrderErr) {
            res.status(200).send({
                error : true,
                message : merchantOrderErr
            });
        }
        else if(merchantOrderResult.length != 0) {
            res.status(200).send({
                error : false,
                orders : merchantOrderResult
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Orders found"
            });
        }
    });

});




// Get Unapproved Merchants

router.get('/getUnapprovedMerchants', function(req, res) {

    var merchantQuery = "SELECT * FROM merchants WHERE admin_approval = 0";
    connection.query(merchantQuery, function(merchantErr, merchantResult) {
        if(merchantErr) {
            res.status(200).send({
                error : true,
                message : merchantErr
            });
        }
        else if(merchantResult.length != 0) {
            res.status(200).send({
                error : false,
                merchants : merchantResult
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Merchants found"
            });
        }
    });

});


// Get Rejected Merchants

router.get('/getRejectedMerchants', function(req, res) {

    var rejectedMerchantQuery = "SELECT * FROM merchants WHERE admin_approval = 2";
    connection.query(rejectedMerchantQuery, function(rejectedMerchantErr, rejectedMerchantResult) {
        if(rejectedMerchantErr) {
            res.status(200).send({
                error : true,
                message : rejectedMerchantErr
            });
        }
        else if(rejectedMerchantResult.length != 0) {
            res.status(200).send({
                error : false,
                merchants : rejectedMerchantResult
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "No Rejected Merchants found"
            });
        }
    });

});




// Approve a merchant by admin

router.post('/approveMerchant', function(req, res) {

    //Required Params
    var merchant_id = req.body.merchant_id;

    var approveMerchantQuery = "UPDATE merchants SET admin_approval = 1 WHERE merchant_id = "+merchant_id+"";
    connection.query(approveMerchantQuery, function(approveMerchantErr, approveMerchantResult) {
        if(approveMerchantErr) {
            res.status(200).send({
                error : true,
                message : approveMerchantErr
            });
        }
        else if(approveMerchantResult.affectedRows != 0) {
            res.status(200).send({
                error : false,
                message : "Approved"        
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "Problem in approving this merchant"
            });
        }
    });

});

// Reject a merchant by admin
router.post('/rejectMerchant', function(req, res) {

    //Required Params
    var merchant_id = req.body.merchant_id;

    var rejectMerchantQuery = "UPDATE merchants SET admin_approval = 2 WHERE merchant_id = "+merchant_id+"";
    connection.query(rejectMerchantQuery, function(rejectMerchantErr, rejectMerchantResult) {
        if(rejectMerchantErr) {
            res.status(200).send({
                error : true,
                message : rejectMerchantErr
            });
        }
        else if(rejectMerchantResult.affectedRows != 0) {
            res.status(200).send({
                error : false,
                message : "Rejected"        
            });
        }
        else {
            res.status(200).send({
                error : true,
                message : "Problem in rejecting this merchant"
            });
        }
    });

});



// Add a product to master PYG Inventory

router.post('/addProductToMasterInventory', function(req, res) {

    //Required Params
    var product_name = req.body.product_name;
    var product_img = req.body.product_img;
    var category_id = req.body.category_id;
    var product_description = req.body.product_description;
    var sku = req.body.sku;
    var quantification = req.body.quantification;
//    var merchant_id = req.body.merchant_id;                                   // 0 - If added by admin                           

    var repititionQuery = "SELECT product_name FROM products WHERE product_name LIKE '"+product_name+"'";
    connection.query(repititionQuery, function(repititionErr, repititionResult) {
        if(repititionErr) {
            res.status(200).send({
                error : true,
                message : repititionErr
            });
        }
        else if(repititionResult.length != 0) {
            res.status(200).send({
                error : true,
                message : 'Product already Exists'
            });
        }
        else {
            var search_query = "SELECT category_name FROM categories WHERE category_id = "+category_id+"";
            connection.query(search_query, function(searchErr, searchResult) {
                if(searchErr) {
                    res.status(200).send({
                        error : true,
                        message : searchErr
                    });
                }
                else {
                    var insert_query = "INSERT INTO products(product_name,product_image,category_id,product_description,sku,quantification,added_by) VALUES('"+product_name+"', '"+product_img+"' , "+category_id+" , '"+product_description+"' , '"+sku+"' , '"+quantification+"' , 0)";
                    connection.query(insert_query, function(insertErr, insertResult) {
                        if(insertErr) {
                            res.status(200).send({
                                error : true,
                                message : insertErr
                            });
                        }
                        else if(insertResult.affectedRows != 0) {
                            res.status(201).send({
                                error : false,
                                message : 'Inserted',
                                product_id : insertResult.insertId,
                                product_name : product_name,
                                product_img : product_img,
                                category_id : category_id,
                                category_name : searchResult[0].category_name,
                                product_description : product_description,
                                sku : sku,
                                quantification : quantification,
                                added_by : {
                                    id : 0,
                                    name : "Admin"
                                },
                            });
                        }
                    });
                }       
            });
        }
    });

});



// Update any existing product in master inventory

router.post('/updateProductInMasterInventory', function(req, res) {

    // Required Params
    var product_id = req.body.product_id;
    var product_name = req.body.product_name;
    var product_img = req.body.product_img;
    var category_id = req.body.category_id;
    var product_description = req.body.product_description;
    var sku = req.body.sku;
    var quantification = req.body.quantification;

    //Select that product
    var selectProductQuery = "SELECT * FROM products WHERE product_id = "+product_id+"";
    connection.query(selectProductQuery, function(selectProductErr, selectProductResult) {
        if(selectProductErr) {
            res.status(200).send({
                error : true,
                message : selectProductErr
            });
        }
        else if(selectProductResult.length == 0) {
            res.status(200).send({
                error : true,
                message : "Product doesn't exists"
            });
        }
        else {
            var updateProductQuery = "UPDATE products SET product_name = '"+product_name+"', product_image = '"+product_img+"', category_id = "+category_id+", product_description = "+product_description+", sku = '"+sku+"', quantification = '"+quantification+"' WHERE product_id = "+product_id+"";
            connection.query(updateProductQuery, function(updateProductErr, updateProductResult) {
                if(updateProductErr) {
                    res.status(200).send({
                        error : true,
                        message : updateProductErr
                    });
                }
                else if(updateProductResult.affectedRows != 0) {
                    res.status(201).send({
                        error : false,
                        status : "Updated",
                        product_id : product_id,
                        product_name : product_name,
                        product_image : product_img,
                        product_description : product_description,
                        sku : sku,
                        quantification : quantification
                    });
                }
            });
        }
    });

});




// Remove a product from Master Inventory

router.post('/removeProductFromMasterInventory', function(req, res) {

    // Required Params
    var product_id = req.body.product_id;

    var productSearchQuery = "SELECT * FROM products WHERE product_id = "+product_id+"";
    connection.query(productSearchQuery, function(productSearchErr, productSearchResult) {
        if(productSearchErr) {
            res.status(200).send({
                error : true,
                message : productSearchErr
            });
        }
        else if(productSearchResult.length == 0) {
            res.status(200).send({
                error : true,
                message : "Product doesn't exists"
            });
        }
        else if(productSearchResult.length != 0) {
            var deleteProductQuery = "DELETE FROM products WHERE product_id = "+product_id+"";
            connection.query(deleteProductQuery, function(deleteProductErr, deleteProductResult) {
                if(deleteProductErr) {
                    res.status(200).send({
                        error : true,
                        message : deleteProductErr
                    });
                }
                else if(deleteProductResult.affectedRows != 0){
                    res.status(201).send({
                        error : false,
                        status : "Product deleted from Master Inventory"
                    });
                }
            });
        }
    });

});




// Add Category to Master Inventory

router.post('/addCategoryToMasterInventory', function(req, res) {

    var category_name = req.body.category_name;
    var category_img = req.body.category_img;
    var quotedCategory = "'"+category_name+"'";
    
    var repititionQuery = "SELECT category_name FROM categories WHERE category_name LIKE "+quotedCategory+"";
    connection.query(repititionQuery, function(repititionErr, repititionResult) {
        if(repititionErr) {
            res.status(200).send({
                error : true,
                message : repititionErr
            });
        }
        else if(repititionResult.length != 0) {
            res.status(200).send({
                error : true,
                message : 'Category already Exists'
            });
        }
        else {
                var insertQuery = "INSERT INTO categories(category_name,category_image,added_by) VALUES('"+category_name+"' , '"+category_img+"' , 0)";
                connection.query(insertQuery, function(insertErr, insertResult) {
                    if(insertErr) {
                        res.status(200).send({
                            error : true,
                            message : insertErr
                        });
                    }
                    else if(insertResult.affectedRows != 0) {
                        res.status(201).send({
                            error : false,
                            status : 'Inserted',
                            category_id : insertResult.insertId,
                            category_name : category_name,
                            category_img : category_img,
                            added_by : {
                                id : 0,
                                name : "Admin"
                            },
                        });
                    }
                });
        }
    });

});


// Update a Category In Master Inventory

router.post('/updateCategoryInMasterInventory', function(req, res) {

    // Required Params
    var category_id = req.body.category_id;
    var category_name = req.body.category_name;
    var category_img = req.body.category_img;

    var selectCategoryQuery = "SELECT * FROM categories WHERE category_id = "+category_id+"";
    connection.query(selectCategoryQuery, function(selectCategoryErr, selectCategoryResult) {
        if(selectCategoryErr) {
            res.status(200).send({
                error : true,
                message : selectCategoryErr
            });
        }
        else if(selectCategoryResult.length == 0) {
            res.status(200).send({
                error : true,
                message : "Category doesn't exists"
            });
        }
        else if(selectCategoryResult.length != 0) {

            var updateCategoryQuery = "UPDATE categories SET category_name = '"+category_name+"', category_image = '"+category_img+"' WHERE category_id = "+category_id+"";
            connection.query(updateCategoryQuery, function(updateCategoryErr, updateCategoryResult) {
                if(updateCategoryErr) {
                    res.status(200).send({
                        error : true,
                        message : updateCategoryErr
                    });
                }
                else if(updateCategoryResult.affectedRows != 0) {
                    res.status(201).send({
                        error : false,
                        status : "Updated",
                        category_id : category_id,
                        category_name : category_name,
                        category_image : category_img
                    });
                }
            });
        }
    });

});


// Delete a Category From Master Inventory

router.delete('/deleteCategoryFromMasterInventory', function(req, res) {

    //Required Params
    var category_id = req.body.category_id;
    var category_name = req.body.category_name;

    // Search existing category
    var categorySearchQuery = "SELECT * FROM categories WHERE category_id = "+category_id+" AND category_name LIKE '"+category_name+"'";
    connection.query(categorySearchQuery, function(categorySearchErr, categorySearchResult) {
        if(categorySearchErr) {
            res.status(200).send({
                error : true,
                message : categorySearchErr
            });
        }
        else if(categorySearchResult.length == 0) {
            res.status(200).send({
                error : true,
                message : "Category doesn't exists"
            });
        }
        else if(categorySearchResult.length != 0) {

            // Delete Category from master inventory
            var deleteCategoryQuery = "DELETE FROM categories WHERE category_id = "+category_id+" AND category_name LIKE '"+category_name+"'";
            connection.query(deleteCategoryQuery, function(deleteCategoryErr, deleteCategoryResult) {
                if(deleteCategoryErr) {
                    res.status(200).send({
                        error : true,
                        message : deleteCategoryErr
                    });
                }
                else if(deleteCategoryResult.affectedRows != 0) {
                    res.status(200).send({
                        error : false,
                        status : "Category deleted from Master Inventory"
                    });
                }
            });
        }
    });

});


/* Add product with excel sheet

 Uploaded excel sheets should have following parameters are mandatory
 - product_name
 - product_description(handle)
 - sku
 - category_name (Category Name should already entered to categories table)
 - quantification(Kg, gms, litre, unit, etc,.)

*/
// API to upload products in bulk
router.post('/bulkProductsUpload', function(req, res) {

    //Required Params
    var fileName = req.body.fileName;
    var unUploadedEntries = new Array();
    var uploadedEntries = new Array();

    var download = require('download-file')
 
    var url = "https://streetsmartb2.s3.amazonaws.com/chauffers/export_items-data-06-10-2018.csv";
 
    var options = {
        directory: "./bulkUploadFiles/",
        fileName: "export_items-data-06-10-2018.csv"
    }
 
    download(url, options, function(err){
        if (err) throw err
        console.log("downloaded");
    }) 

    var uploadedFile = Excel.readFile("./bulkUploadFiles/"+fileName);
    if(!uploadedFile) {
        res.status(200).send({
            error : true,
            status : "Problem in updating sheets"
        });
    }
    else {
        var sheetsInUploadedFile = uploadedFile.SheetNames;
        var excelData = Excel.utils.sheet_to_json(uploadedFile.Sheets[sheetsInUploadedFile[0]]);

        excelData.forEach(function(item) {
            
            // Check whether the product already exists in database
            var repititionQuery = "SELECT * FROM products WHERE product_name LIKE '"+item.Name+"' OR sku LIKE "+item.SKU+"";
            connection.query(repititionQuery, function(repititionErr, repititionResult) {

                // If query leads to error
                if(repititionErr) {
                    res.status(200).send({
                        error : true,
                        message : repititionErr
                    });
                }

                // If some match found then don't insert that product
                else if(repititionResult.length != 0) {
                    item.Reason = "Product already exists";
                    unUploadedEntries.push(item);
                }

                // If no matches found then insert that product
                else if(repititionResult.length == 0) {

                    // Check for categories
                    if(!item.Category) {
                        item.Reason = "Category entry doesn't exists";
                        unUploadedEntries.push(item);
                    }
                    else {

                        // Select category_id based on Category Name
                        var categorySelectionQuery = "SELECT category_id FROM categories WHERE category_name LIKE '"+item.Category+"'";
                        connection.query(categorySelectionQuery, function(categorySelectionErr, categorySelectionResult) {

                            // Query Error
                            if(categorySelectionErr) {
                                res.status(200).send({
                                    error : true,
                                    message : categorySelectionErr
                                });
                            }

                            // What if category not exists
                            else if(categorySelectionResult.length == 0) {
                                item.Reason = "Category doesn't exists"
                                unUploadedEntries.push(item);       
                            }

                            // What if category exists
                            else if(categorySelectionResult.length != 0) {
                                
                                // Make product entry into inventory if all entries are exists

                                // Check required inputs seperately
                                if((!item.Handle) || (!item.SKU) || (!item.Name) || (!item.Category) || (!item.Quantification)) {
                                    item.Reason = "Some required parameters were missing";
                                    unUploadedEntries.push(item);
                                }
                                else {
                                    var inventoryUpdateQuery = "INSERT INTO products(product_name,category_id,product_description,sku,quantification,added_by) VALUES('"+item.Name+"', "+categorySelectionResult[0].category_id+", '"+item.Handle+"', '"+item.SKU+"', '"+item.Quantification+"', 0)";
                                    connection.query(inventoryUpdateQuery, function(inventoryUpdateErr, inventoryUpdateResult) {
                                        if(inventoryUpdateErr) {
                                            res.status(201).send({
                                                error : false,
                                                message : inventoryUpdateErr
                                            });
                                        }
                                        else if(inventoryUpdateResult.insertId != 0) {
                                            item.Status = "Inserted into inventory";
                                            uploadedEntries.push(item);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
        });

        // Set a timeout then check for the entries in uploaded and unUploaded entries
        setTimeout(function() {
            if(uploadedEntries.length != 0 || unUploadedEntries.length != 0) {
                res.status(200).send({
                    error : false,
                    totalEntries : excelData.length,
                    uploadedEntries : {
                        count : uploadedEntries.length,
                        entries : uploadedEntries
                    },
                    unUploadedEntries : {
                        count : unUploadedEntries.length,
                        entries : unUploadedEntries
                    }
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    message : "Problem in uploading Entries"
                });
            }
        }, 10000);
    }

});

router.post('/bulkInventoryUpload', function(req, res) {

    //Required Params
    var fileName = req.body.fileName;
    var unUploadedEntries = new Array();
    var uploadedEntries = new Array();
    var productnotinexcel = new Array();
    var product_name = new Array();
    var merchant_id = req.body.merchant_id;
    var download = require('download-file')
    var url = fileName;
    var newfileName = fileName.replace("https://streetsmartb2.s3.amazonaws.com/chauffers/","");
    var options = {
        directory: "./bulkUploadFiles/",
        fileName: "test.xlsx"
    }
 
    download(url, options, function(err){
        if (err) throw err
        console.log("downloaded");
    }) 

    var uploadedFile = Excel.readFile("./bulkUploadFiles/"+newfileName);
    if(!uploadedFile) {
        res.status(200).send({
            error : true,
            status : "Problem in updating sheets"
        });
    }
    else {
        var sheetsInUploadedFile = uploadedFile.SheetNames;
        var excelData = Excel.utils.sheet_to_json(uploadedFile.Sheets[sheetsInUploadedFile[0]]);
        // variables for inventory updation
        var productlist;
        var categorylist;
        var skulist;
        var availabilitylist;
        var quantification;
        var defaultprice;
        var sellingprice;
        var pygprice;
        var seasonal;
        var instock;
        var lowstock;

        excelData.forEach(function(item) {
            // Check whether the product already exists in database
            var repititionQuery = "SELECT * FROM products WHERE product_name LIKE '"+item.Product+"' OR sku LIKE "+item.SKU+"";
            connection.query(repititionQuery, function(repititionErr, repititionResult) {
                // If query leads to error
                if(repititionErr) {
                    res.status(200).send({
                        error : true,
                        message : repititionErr
                    });
                }

                // If some match doesn't found then don't insert that product
                else if(repititionResult.length == 0) {
                    item.Reason = "Product doesn't exists";
                    unUploadedEntries.push(item);
                }

                // If matches found then insert that product
                else if(repititionResult.length != 0) {

                    // Check for categories
                    if(!item.Category) {
                        item.Reason = "Category entry doesn't exists";
                        unUploadedEntries.push(item);
                    }
                    else {
                       
                        // Select category_id based on Category Name
                        var categorySelectionQuery = "SELECT product_id,category_id FROM products WHERE product_name LIKE '"+item.Product+"'";
                        connection.query(categorySelectionQuery, function(categorySelectionErr, categorySelectionResult) {
                            // Query Error
                            if(categorySelectionErr) {
                                res.status(200).send({
                                    error : true,
                                    message : categorySelectionErr
                                });
                            }

                            // What if category not exists
                            else if(categorySelectionResult.length == 0) {
                                item.Reason = "Category doesn't exists"
                                unUploadedEntries.push(item);       
                            }

                            // What if category exists
                            else if(categorySelectionResult.length != 0) {
                                // Make product entry into inventory if all entries are exists

                                // Check required inputs seperately
                                if((!item.Availability) || (!item.SKU) || (!item.Product) || (!item.Category) || (!item.Quantification) || (!item.Default_price) || (!item.Cost) || (!item.Price) || (!item.Seasonal) || (!item.In_stock) || (!item.Low_stock)) {
                                    item.Reason = "Some required parameters were missing";
                                    unUploadedEntries.push(item);
                                }
                                else {
                                    var inventoryQuery = "SELECT * FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
                                    connection.query(inventoryQuery, function(inventoryErr, inventoryResult) {
                                        
                                        if(inventoryErr) {
                                            res.status(201).send({
                                                error : true,
                                                message : inventoryErr
                                            });
                                        }
                                        else if(inventoryResult.length != 0) {
                                            // To find whether the product is in inventory or not

                                            // // If products in inventory is null
                                                if (productlist == null || categorylist == null || skulist == null || availabilitylist == null || quantification == null || defaultprice == null || sellingprice == null || pygprice == null || seasonal == null || instock == null || lowstock == null) {
                                                    productlist = categorySelectionResult[0].product_id;
                                                    categorylist = categorySelectionResult[0].category_id;
                                                    skulist = item.SKU;
                                                    availabilitylist = item.Availability;
                                                    quantification = item.Quantification;
                                                    defaultprice = item.Default_price;
                                                    sellingprice = item.Cost;
                                                    pygprice = item.Price;
                                                    seasonal = item.Seasonal;
                                                    instock = item.In_stock;
                                                    lowstock = item.Low_stock;
                                                } else {
                                                    productlist = productlist + "," + categorySelectionResult[0].product_id;
                                                    categorylist = categorylist + "," + categorySelectionResult[0].category_id;
                                                    skulist = skulist + "," + item.SKU;
                                                    availabilitylist = availabilitylist + "," + item.Availability;
                                                    quantification = quantification + "," + item.Quantification;
                                                    defaultprice = defaultprice + "," + item.Default_price;
                                                    sellingprice = sellingprice + "," + item.Cost;
                                                    pygprice = pygprice + "," + item.Price;
                                                    seasonal = seasonal + "," + item.Seasonal;
                                                    instock = instock + "," + item.In_stock;
                                                    lowstock = lowstock + "," + item.Low_stock;
                                                }
                                        }
                                        else {
                                            res.status(201).send({
                                                error : true,
                                                message : "Inventory not found"
                                            });
                                        }
                                    });
                                }   
                            
                            }
                        });
                    }
                }
            });
            setTimeout(function() {
                var productlistarr = productlist.split(',');
                var orderedproductnotinexcel = new Array();
                var productarr = new Array();
                var merchantOrderQuery = "SELECT * FROM orders WHERE merchant_id = "+merchant_id+" and order_status IN(1, 2, 3)";
                connection.query(merchantOrderQuery, function(merchantOrderErr, merchantOrderResult) {
                    if(!merchantOrderErr && merchantOrderResult.length != 0) {
                        for(var i = 0; i<merchantOrderResult.length; i++) {
                            productarr = merchantOrderResult[i].products_id.split(',');
                            for(var j = 0; j<productarr.length; j++) {
                                for(var k = 0; k<productlistarr.length; k++) {
                                    if(productarr[j] == productlistarr[k]) {
                                        break;
                                    } else {
                                        orderedproductnotinexcel.push(productarr[j])
                                    }
                                }
                            }
                        }
                    }
                    productnotinexcel = orderedproductnotinexcel.filter(function(elem, index, self) {
                        return index === self.indexOf(elem);
                    });
                    if(productnotinexcel.length == 0) {
                        var inventoryUpdateQuery = "UPDATE merchant_inventory SET products_id ='"+productlist+"', categories_id = '"+categorylist+"', sku = '"+skulist+"', available_for_sale = '"+availabilitylist+"', quantification = '"+quantification+"', default_price = '"+defaultprice+"', selling_price = '"+sellingprice+"', pyg_price = '"+pygprice+"', seasonal = '"+seasonal+"', available_qty = '"+instock+"' , low_stock = '"+lowstock+"' , last_updated = CURRENT_TIMESTAMP WHERE merchant_id = "+merchant_id+"";
                        connection.query(inventoryUpdateQuery, function(inventoryUpdateErr, inventoryUpdateResult) {
                            if(inventoryUpdateErr) {
                                res.status(201).send({
                                    error : true,
                                    message : inventoryUpdateErr
                                });
                            }
                            else if(inventoryUpdateResult.affectedRows != 0) {
                                item.Status = "Updated into inventory";
                                uploadedEntries.push(item);
                            }
                        });
                    }
                });
            }, 2000);
        });
        // Set a timeout then check for the entries in uploaded and unUploaded entries
        setTimeout(() => {
            for(var i = 0; i<productnotinexcel.length; i++) {
                var query = "SELECT * FROM products WHERE product_id = "+productnotinexcel[i]+"";
                connection.query(query, function(err, result) {
                    product_name.push(result[0].product_name);
                });
            }
        }, 3000);
            
        setTimeout(function() {
            if(productnotinexcel.length != 0) {
                res.status(200).send({
                    error : true,
                    pending_order_products : product_name
                    // message : "Problem in uploading Entries"
                });
            } else if(uploadedEntries.length != 0 || unUploadedEntries.length != 0) {
                res.status(200).send({
                    error : false,
                    totalEntries : excelData.length,
                    uploadedEntries : {
                        count : uploadedEntries.length,
                        entries : uploadedEntries
                    },
                    unUploadedEntries : {
                        count : unUploadedEntries.length,
                        entries : unUploadedEntries
                    }
                });
            }
            else {
                res.status(200).send({
                    error : true,
                    message : "Problem in uploading Entries"
                });
            }
        }, 4000);
    }

});


router.post('/excelByMerchantId', function(req, res) {
    
    var merchant_id = req.body.merchant_id;
    var prodnamearray = new Array();
    var catenamearray = new Array();
    var merchant_name = new Array();

    var inventoryQuery = "SELECT * FROM merchant_inventory WHERE merchant_id = "+merchant_id+"";
    connection.query(inventoryQuery, function(inventoryErr, inventoryResult) {
        
        if(inventoryErr) {
            res.status(201).send({
                error : true,
                message : inventoryErr
            });
        } else if(inventoryResult.length!=0 && inventoryResult[0].products_id != null && inventoryResult[0].products_id != '') {
			
			var productsArr = inventoryResult[0].products_id.split(',');
			var categoriesArr = inventoryResult[0].categories_id.split(',');
			var skuArr = inventoryResult[0].sku.split(',');
			var availabilityArr = inventoryResult[0].available_for_sale.split(',');
			var quantificationArr = inventoryResult[0].quantification.split(',');
			var defaultPriceArr = inventoryResult[0].default_price.split(',');
			var sellingPriceArr = inventoryResult[0].selling_price.split(',');
			var pygPriceArr = inventoryResult[0].pyg_price.split(',');
			var seasonalArr = inventoryResult[0].seasonal.split(',');
			var availableQtyArr = inventoryResult[0].available_qty.split(',');
            var lowStockArr = inventoryResult[0].low_stock.split(',');
            
            var xl = require('excel4node');
            var wb = new xl.Workbook();
            var ws = wb.addWorksheet('Sheet 1');

            var style = wb.createStyle({
                font: {
                color: '#FF0800',
                size: 12,
                },
                border: { // §18.8.4 border (Border)
                left: {
                    style: 'medium', //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                    color: 'black' // HTML style hex value
                },
                right: {
                    style: 'medium',
                    color: 'black'
                },
                top: {
                    style: 'medium',
                    color: 'black'
                },
                bottom: {
                    style: 'medium',
                    color: 'black'
                } 
                },
                alignment: { // §18.8.1
                horizontal: 'center',
                vertical: 'center'
                }
            });
            var query = "SELECT shop_name, merchant_name FROM merchants WHERE merchant_id = "+merchant_id+"";
            connection.query(query, function(err, result) {
                if(err) {
                    res.status(200).send({
                        error : true,
                        message : err
                    });
                }
                else if(result.length != 0) {
                    merchant_name = ['Shop Name: '+result[0].shop_name +' & '+'Merchant Name: '+result[0].merchant_name]
                }
            });
            setTimeout(() => {
                ws.cell(1,1,1,11,true).string(merchant_name).style(style);
            var columnHeader = ['Product','SKU','Availability','Category','Quantification','Default_price','Cost','Price','Seasonal','In_stock','Low_stock'];

            //For printing excel column header
            for(var i=0;i<columnHeader.length;i++)
            {
                ws.cell(2,i+1).string(columnHeader[i]).style(style);
            }

            for(var i=0; i<productsArr.length; i++) {
                var productQuery = "SELECT products.product_name,categories.category_name FROM products,categories WHERE products.product_id = "+productsArr[i]+" AND categories.category_id = "+categoriesArr[i]+"";
                connection.query(productQuery, function(productErr, productResult) {
                    if(productErr) {
                        res.status(201).send({
                            error : true,
                            message : productErr
                        });
                    } else if(productResult.length != 0) {
                        prodnamearray.push(productResult[0].product_name);
                        catenamearray.push(productResult[0].category_name);
                    }
                });
            }
            }, 800);
            
            setTimeout(() => {
                //For printing excel row data
                for(var i=0;i<productsArr.length;i++)
                {
                    ws.cell(i+3,1).string(prodnamearray[i]).style(style);
                    ws.cell(i+3,2).string(skuArr[i]).style(style);
                    ws.cell(i+3,3).string(availabilityArr[i]).style(style);
                    ws.cell(i+3,4).string(catenamearray[i]).style(style);
                    ws.cell(i+3,5).string(quantificationArr[i]).style(style);
                    ws.cell(i+3,6).string(defaultPriceArr[i]).style(style);
                    ws.cell(i+3,7).string(sellingPriceArr[i]).style(style);
                    ws.cell(i+3,8).string(pygPriceArr[i]).style(style);
                    ws.cell(i+3,9).string(seasonalArr[i]).style(style);
                    ws.cell(i+3,10).string(availableQtyArr[i]).style(style);
                    ws.cell(i+3,11).string(lowStockArr[i]).style(style);
                }

                 //generate excel file
                wb.write('./include/bulkUploadFiles/merchant'+merchant_id+'.xlsx');
            }, 1000);

            setTimeout(() => {
                res.status(200).send({
                    error : false,
                    link : 'http://35.163.82.63/pyg/include/bulkUploadFiles/merchant'+merchant_id+'.xlsx'
                });
            }, 1000);

        } else {
            res.status(200).send({
                error : true,
                message : "inventory empty"
            });
        } 
    });
});


router.get('/getMerchantRating/', function(req, res) {

    // Required Params
    var merchantId = req.query.merchantId;

    // Select feedbacks for that particular merchant
    var selectFeedbacksQuery = "SELECT feedback.*, merchants.shop_name FROM feedback LEFT JOIN merchants ON merchants.merchant_id = feedback.merchant_id WHERE feedback.merchant_id = "+merchantId+"";
    connection.query(selectFeedbacksQuery, function(selectFeedbacksErr, selectFeedbacksResult) {
        if(selectFeedbacksErr) {
            res.status(200).send({
                error : true,
                message : selectFeedbacksErr
            });
        }
        else if(selectFeedbacksResult.length != 0) {

            // Overall Feedback Count
            var overAllFeedbackCount = selectFeedbacksResult.length;
            overAllRating = 0;
            overAllRatingsTotal = 0;

            // Calculate the sum of overall ratings
            selectFeedbacksResult.forEach(function(item) {
                overAllRatingsTotal += item.rating;
            });

            // Calculate overAll ratings
            overAllRating = overAllRatingsTotal/overAllFeedbackCount;

            // Response
            res.status(200).send({
                error : false,
                merchant_id : merchantId,
                shop_name : selectFeedbacksResult[0].shop_name,
                overAllRating : overAllRating
            });
        }
        else if(selectFeedbacksResult.length == 0) {
            res.status(200).send({
                error : false,
                merchant_id : merchantId,
                shop_name : selectFeedbacksResult[0].shop_name,
                overAllRating : 0.0
            });
        }
    });

});


//function to fetch product details
function fetchProductInfo(productids,allOrders,i) {
    return new Promise(resolve => {
        let product = new Set();
        if(productids.indexOf(',') == -1) {
            var productQuery = "SELECT product_id,product_name,product_image FROM products WHERE product_id="+productids+"";
            connection.query(productQuery, function(productErr, productResult) {
                if(productErr) {
                    resolve(singleProductErr);
                } else {
                    product.add(productResult);
                    allOrders[i].products_id = Array.from(product);
                    resolve(allOrders);
                }
            });//eof query.connection    
        } else {
            var productsArr = productids.split(',');
            for(let i=0;i<productsArr.length;i++) {
                var productQuery = "SELECT product_id,product_name,product_image FROM products WHERE product_id="+productsArr[i]+"";
                connection.query(productQuery, function(productErr, productResult) {
                    if(productErr) {
                        resolve(singleProductErr);
                    } else {
                        product.add(productResult);
                     }
                    })//eof query connection    
            }
            setTimeout(() => {
                allOrders[i].products_id = Array.from(product);
                resolve(allOrders);
            }, 1000);
        }        
    });//eof promise
}




// Registering routes to our API's
// all routes will be prefixed with /pyg/merchant
app.use('/pyg/CMS', router);

// Start the server
app.listen(port);

},




// Get product details for ordered items
orderedProductDetails : [],
orderedProducts : function(product) {

    if(product == 1) {
        return module.exports.orderedProductDetails;
    }
    else if(product == 0) {
        module.exports.orderedProductDetails = [];
    }
    else {
        module.exports.orderedProductDetails.push(product);
    }

},

}
