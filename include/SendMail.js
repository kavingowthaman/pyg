const nodemailer = require('nodemailer');

module.exports = {

    sendMail : function(to_email, subject, body) {

        this.sent = 0; //Change the mode to 0

        nodemailer.createTestAccount((err, account) => {
            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: 'smtp.sendgrid.net',
                port: 465,
                secure: true    , // true for 465, false for other ports
                auth: {
                    user: 'apikey', // SendGrid API Username
                    pass: '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' // SendGrid API Key
                }
            });
        
            // setup email data with unicode symbols
            let mailOptions = {
                from: '"PYG Shopping" <pygshopping@gmail.com>', // sender address
                to: to_email, // list of receivers
                subject: subject, // Subject line
                html: body, // plain text body
            };
        
            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    this.mailSentCheck(-1);
                }
                this.mailSentCheck(1);
            });
        });
    },

    sent : 0,
    mailSentCheck : function(flag) {
        if(flag == 1) {
            this.sent = 1;
        }
        else if(flag == -1) {
            this.sent = 0;
        }
        else if(flag == 0) {
            return this.sent;
        }
    }

}